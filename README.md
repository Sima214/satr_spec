# SATR Spec

The same idea as SSCE+SATR, but focused on modularity and stability more then speed.

## Overview

Each folder holds a module.
Each modules has a `deps.txt` file, with a list of inter module dependencies.
Can be imported as a meson subproject.

## Always incomplete module list and descriptions

- Trace: low level print to stderr.
- Concat: C string concatenation.
- Native String: On Windows converts utf8 strings to utf16 strings for use in apis.
- Logger
