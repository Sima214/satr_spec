#ifndef SPEC_TRACE
#define SPEC_TRACE
/**
 * @file
 * @brief puts style macros to be used at early program startup
 * and low level code. Compile time disabled at non-debug builds.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRACE
    #ifndef NDEBUG
        /**
         * Low level trace function.
         * Simply prints out \p msg onto the console.
         * Should contain only ascii characters
         */
        #define TRACE(msg)                                                                     \
            fputs(msg, stderr);                                                                \
            fputc('\n', stderr)
    #else
        /**
         * NOP on release builds.
         */
        #define TRACE(msg)
    #endif
#endif

#ifndef TRACF
    #ifndef NDEBUG
        /**
         * Low level trace function with formatting support.
         * Should contain only ascii characters.
         */
        #define TRACF(...)                                                                     \
            fprintf(stderr, __VA_ARGS__);                                                      \
            fputc('\n', stderr)
    #else
        /**
         * NOP on release builds.
         */
        #define TRACF(...)
    #endif
#endif

#ifndef FAIL
    #ifndef NDEBUG
        /**
         * Low level trace function.
         * Simply prints out \p msg onto the console.
         * Should contain only ascii characters
         */
        #define FAIL(msg)                                                                      \
            fputs(msg, stderr);                                                                \
            fputc('\n', stderr);                                                               \
            abort()
    #else
        /**
         * NOP on release builds.
         */
        #define FAIL(msg) abort()
    #endif
#endif

#ifndef FAILF
    #ifndef NDEBUG
        /**
         * Low level trace function with formatting support.
         * Should contain only ascii characters.
         */
        #define FAILF(...)                                                                     \
            fprintf(stderr, __VA_ARGS__);                                                      \
            fputc('\n', stderr);                                                               \
            abort()
    #else
        /**
         * NOP on release builds.
         */
        #define FAILF(...) abort()
    #endif
#endif

#ifndef ERRNO_CALL_FAIL
    /**
     * Assert the result of a function returning an ERRNO code.
     */
    #define ERRNO_CALL_FAIL(func, ...)                                                         \
        {                                                                                      \
            int error = func(__VA_ARGS__);                                                     \
            if (error) {                                                                       \
                FAILF("Call at %s failed in %s:%d with %s.", #func, __FILE__, __LINE__,        \
                      strerror(error));                                                        \
            }                                                                                  \
        }
#endif

#endif /*SPEC_TRACE*/
