#ifndef SPEC_TRACE_HPP
#define SPEC_TRACE_HPP
/**
 * @file
 * @brief print style templates to be used at early program startup
 * and low level code. Compile time disabled at non-debug builds.
 */

#include <iostream>
#include <string>
#include <cstdlib>

#include <macros.h>

namespace spec {

#ifndef NDEBUG

    template<typename T> inline void trace(T msg) {
        std::cerr << msg << std::endl;
    }

    #if __cplusplus >= 201703L
        template<typename... Args> inline void trace(Args&&... args) {
            (std::cerr << ... << args) << std::endl;
        }
    #else
        template<typename... Args> inline void trace([[maybe_unused]] Args&&... args) {}
    #endif

#else

    template<typename T> inline void trace([[maybe_unused]] T msg) {}
    template<typename... Args> inline void trace([[maybe_unused]] Args&&... args) {}

#endif

template<typename T> inline MARK_NORETURN void fatal(T msg) {
    trace<T>(msg);
    std::abort();
}

template<typename... Args> inline MARK_NORETURN void fatal(Args&&... args) {
    trace(args...);
    std::abort();
}

}  // namespace spec

#endif /*SPEC_TRACE_HPP*/
