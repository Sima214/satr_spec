#ifndef SPEC_LOGGER_HPP
#define SPEC_LOGGER_HPP
/**
 * @file
 * @brief Logger with the ability to output to console and/or a file.
 * TODO: Deprecate singleton API.
 */
#include <Utils.hpp>

#include <cstdlib>
#include <sstream>
#include <string>

#include <macros.h>
C_DECLS_START
#include <logger.h>
C_DECLS_END

namespace spec {

class Logger {
   public:
    enum class Level {
        All = LOGGER_ALL,
        Verbose = LOGGER_VERBOSE,
        Debug = LOGGER_DEBUG,
        Info = LOGGER_INFO,
        Warn = LOGGER_WARN,
        Error = LOGGER_ERROR,
        Fatal = LOGGER_FATAL,
        Off = LOGGER_OFF
    };

   private:
    Logger() {
        configure(true, Level::All);
    }

    ~Logger() {
        logger_uninitialize();
    }

   public:
    static Logger& get_instance();

    /**
     * Configures Logger's outputs.
     */
    static bool configure(bool console, Level console_level, const char* filename,
                          Level file_level) {
        logger_uninitialize();
        return logger_initialize((LogLevel) console_level, console, (LogLevel) file_level,
                                 filename) == 0;
    }
    static bool configure(bool console, Level console_level, std::string& filename,
                          Level file_level) {
        return configure(console, console_level, filename.data(), file_level);
    }
    static bool configure(bool console, Level console_level) {
        return configure(console, console_level, (char*) nullptr, Level::Off);
    }

    /**
     * Generic logger.
     */
    static void log(Level l, std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) l, str);
    }
    static void log(Level l, std::stringbuf& msg) {
        std::string str = msg.str();
        log(l, str);
    }
    static void log(Level l, std::stringstream& msg) {
        std::string str = msg.str();
        log(l, str);
    }
    static void logv(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Verbose, str);
    }
    static void logv(std::stringbuf& msg) {
        std::string str = msg.str();
        logv(str);
    }
    static void logv(std::stringstream& msg) {
        std::string str = msg.str();
        logv(str);
    }
    static void logd(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Debug, str);
    }
    static void logd(std::stringbuf& msg) {
        std::string str = msg.str();
        logd(str);
    }
    static void logd(std::stringstream& msg) {
        std::string str = msg.str();
        logd(str);
    }
    static void logi(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Info, str);
    }
    static void logi(std::stringbuf& msg) {
        std::string str = msg.str();
        logi(str);
    }
    static void logi(std::stringstream& msg) {
        std::string str = msg.str();
        logi(str);
    }
    static void logw(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Warn, str);
    }
    static void logw(std::stringbuf& msg) {
        std::string str = msg.str();
        logw(str);
    }
    static void logw(std::stringstream& msg) {
        std::string str = msg.str();
        logw(str);
    }
    static void loge(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Error, str);
    }
    static void loge(std::stringbuf& msg) {
        std::string str = msg.str();
        loge(str);
    }
    static void loge(std::stringstream& msg) {
        std::string str = msg.str();
        loge(str);
    }
    [[noreturn]] static void logf(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Fatal, str);
        std::abort();
    }
    [[noreturn]] static void logf(std::stringbuf& msg) {
        std::string str = msg.str();
        logf(str);
    }
    [[noreturn]] static void logf(std::stringstream& msg) {
        std::string str = msg.str();
        logf(str);
    }
    static void logf_rec(std::string& msg) {
        const char* str = msg.data();
        logger_puts((LogLevel) Level::Fatal, str);
    }
    static void logf_rec(std::stringbuf& msg) {
        std::string str = msg.str();
        logf_rec(str);
    }
    static void logf_rec(std::stringstream& msg) {
        std::string str = msg.str();
        logf_rec(str);
    }

#if __cplusplus >= 201703L
    template<typename... Args> static void log(Level l, Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        log(l, str);
    }
    template<typename... Args> static void logv(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logv(str);
    }
    template<typename... Args> static void logd(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logd(str);
    }
    template<typename... Args> static void logi(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logi(str);
    }
    template<typename... Args> static void logw(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logw(str);
    }
    template<typename... Args> static void loge(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        loge(str);
    }
    template<typename... Args> static MARK_NORETURN void logf(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logf(str);
    }
    template<typename... Args> static void logf_rec(Args&&... args) {
        std::stringstream str;
        (str << ... << args);
        logf_rec(str);
    }
#endif
};

}  // namespace spec

static spec::Logger& logger = spec::Logger::get_instance();

#endif /*SPEC_LOGGER_HPP*/
