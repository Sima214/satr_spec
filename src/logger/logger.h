#ifndef SPEC_LOGGER
#define SPEC_LOGGER
/**
 * @file
 * @brief Logger with the ability to output to console and/or a file.
 */

#include <macros.h>
#include <stdlib.h>

/**
 * Logging levels.
 * From less important(verbose) to very important(fatal).
 * The enums ALL and OFF must only be used for initialization.
 */
typedef enum {
    /**
     * A filter which catches all messages.
     */
    LOGGER_ALL,
    /**
     * For not important messages.
     */
    LOGGER_VERBOSE,
    /**
     * For not important messages to the end user.
     */
    LOGGER_DEBUG,
    /**
     * Generic messages.
     */
    LOGGER_INFO,
    /**
     * Show a warning.
     */
    LOGGER_WARN,
    /**
     * Show an error message.
     * Normal program execution might still be possible.
     */
    LOGGER_ERROR,
    /**
     * Show a fatal error message.
     * Continuing normal program execution is not possible.
     */
    LOGGER_FATAL,
    /**
     * A filter which disables all messages.
     */
    LOGGER_OFF
} LogLevel;

/**
 * Changes (global) logger settings.
 * \p level is a filter for
 * If \p console_write is non-zero, then the output will be printed to stdout/console.
 * If \p output_filename is non-null, then the output will also be written to a file.
 *
 * Returns non-zero if initialization failed.
 */
int logger_initialize(LogLevel console_level, int console_write, LogLevel file_level,
                      const char* output_filename);

/**
 * Closes file handles and resets logger global state.
 *
 * Returns non-zero on failure.
 */
int logger_uninitialize();

/**
 * Internal function. Used by C++ Logger.
 *
 * Writes \p msg to console and/or file
 * with time/thread information and newline character.
 */
void logger_puts(LogLevel l, const char* msg);

/**
 * printf style logger.
 */
void logger_log(LogLevel l, const char* fmt, ...) MARK_PRINTF(2, 3);

/**
 * Logs a verbose message.
 * Takes the same parameters as printf.
 */
#define logger_logv(...) logger_log(LOGGER_VERBOSE, __VA_ARGS__)

/**
 * Logs a debug message.
 * Takes the same parameters as printf.
 */
#define logger_logd(...) logger_log(LOGGER_DEBUG, __VA_ARGS__)

/**
 * Logs a generic/info message.
 * Takes the same parameters as printf.
 */
#define logger_logi(...) logger_log(LOGGER_INFO, __VA_ARGS__)

/**
 * Logs a warning message.
 * Takes the same parameters as printf.
 */
#define logger_logw(...) logger_log(LOGGER_WARN, __VA_ARGS__)

/**
 * Logs an error message.
 * Takes the same parameters as printf.
 */
#define logger_loge(...) logger_log(LOGGER_ERROR, __VA_ARGS__)

/**
 * Logs a fatal message and aborts execution.
 * Takes the same parameters as printf.
 */
#define logger_logf(...)                                                                       \
    logger_log(LOGGER_FATAL, __VA_ARGS__);                                                     \
    abort();                                                                                   \
    MARK_UNREACHABLE()

/**
 * Logs a recovarable fatal message.
 * Takes the same parameters as printf.
 */
#define logger_logf_rec(...) logger_log(LOGGER_FATAL, __VA_ARGS__)

#endif /*SPEC_LOGGER*/
