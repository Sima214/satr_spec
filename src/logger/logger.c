#include "logger.h"

#include <concat.h>
#include <macros.h>
#include <natstr.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <trace.h>

#if IS_POSIX
    #include <unistd.h>
#elif IS_WINDOWS
    #include <versionhelpers.h>
    #include <windows.h>
#endif

// Color codes.
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

static const char* LEVEL2COLOR[] = {WHT, WHT, BLU, GRN, CYN, YEL, RED, RESET};
static const char* LEVEL2STRING[] = {"ALL",  "TRACE", "DEBUG", "INFO",
                                     "WARN", "ERROR", "FATAL", "OFF"};

// Tunables.
#define LOGFILE_BUFLEN 64U
#define MESSAGE_BUFLEN 2048U
#define THREAD_BUFLEN 32U
#define TIME_BUFLEN 16U
#define THREAD_ERROR "???"

static LogLevel console_level = LOGGER_OFF;
static int console = 0;
static int console_color = 0;
static LogLevel file_level = LOGGER_OFF;
static FILE* file = NULL;

static pthread_mutex_t logger_state_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * Filters based on current console LogLevel.
 */
static int check_loglevel_console(LogLevel l) {
    return l >= console_level;
}

/**
 * Filters based on current file LogLevel.
 */
static int check_loglevel_file(LogLevel l) {
    return l >= file_level;
}

#if IS_POSIX

/**
 * Returns true if console supports ascii colors.
 */
static int console_supports_ascii_color() {
    return isatty(STDOUT_FILENO);
}

/**
 * Writes to stdout.
 */
static void write_console(const char* msg, const char* color) {
    if (console_color) {
        fputs(color, stdout);
    }
    fputs(msg, stdout);
    if (console_color) {
        fputs(RESET, stdout);
    }
    fputc('\n', stdout);
}

#elif IS_WINDOWS

/**
 * Returns true if console supports ascii colors.
 */
static int console_supports_ascii_color() {
    return IsWindows10OrGreater();
}

static const wchar_t NEW_LINE[] = L"\r\n";
static const int NEW_LINE_LEN = (sizeof(NEW_LINE) / sizeof(wchar_t)) - 1;
static int write_console_handle_invalid = 0;

static void write_console_fallback(const char* msg) {
    // Console output is either getting redirected to a file,
    // or we are a GUI executable launched from another GUI.
    write_console_handle_invalid = 1;
    puts(msg);
}

/**
 * Writes to native console.
 */
static void write_console(const char* msg, const char* color) {
    if (write_console_handle_invalid) {
        // Always take fallback path after the first failed call.
        write_console_fallback(msg);
        return;
    }
    // Get console handle...
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    // and check if it's valid.
    if (console == NULL || console == INVALID_HANDLE_VALUE) {
        // console handle is unusable.
        if (AttachConsole(ATTACH_PARENT_PROCESS)) {
            // First, try to attach to a command line.
            // This is needed for executables marked as GUIs.
            console = GetStdHandle(STD_OUTPUT_HANDLE);
            if (console == NULL || console == INVALID_HANDLE_VALUE) {
                // Still unusable. Give up.
                write_console_fallback(msg);
                return;
            }
        }
        else {
            // There is no command line.
            write_console_fallback(msg);
            return;
        }
    }
    // Write to native console.
    int success = 0;
    DWORD written = 0;
    if (console_color) {
        success = WriteConsoleA(console, color, strlen(color), &written, NULL);
        if (!success) {
            TRACE("WriteConsole Color Fail!");
            write_console_fallback(msg);
            return;
        }
    }
    const wchar_t* native = natstr_utf8_to_native(msg);
    size_t native_len = wcslen(native);
    success = WriteConsole(console, native, native_len, &written, NULL);
    natstr_free(native);
    if (!success) {
        TRACE("WriteConsole Fail!");
        write_console_fallback(msg);
        return;
    }
    if (console_color) {
        success = WriteConsoleA(console, RESET, sizeof(RESET), &written, NULL);
        if (!success) {
            TRACE("WriteConsole Color Reset Fail!");
            write_console_fallback(msg);
            return;
        }
    }
    success = WriteConsole(console, NEW_LINE, NEW_LINE_LEN, &written, NULL);
    if (!success) {
        TRACE("WriteConsole New Line Fail!");
        write_console_fallback(msg);
        return;
    }
}

#else

int console_supports_ascii_color();

void write_console(const char* msg, const char* color);

#endif

static size_t create_time(char* str, size_t n) {
    time_t curtime;
    struct tm curtime_table;
    time(&curtime);
    if (ssce_localtime(curtime, curtime_table) != 0) {
        *str = '\0';
        return 0;
    }
    size_t size = snprintf(str, n, "%02i:%02i:%02i", curtime_table.tm_hour,
                           curtime_table.tm_min, curtime_table.tm_sec);
    return math_min(size, n);
}

int logger_initialize(LogLevel cl, int cw, LogLevel fl, const char* of) {
    pthread_mutex_lock(&logger_state_lock);
    int not_ok = 0;
    // Console
    console_level = cl;
    console = cw;
    if (cw) {
        console_color = console_supports_ascii_color();
    }
    else {
        console_level = LOGGER_OFF;
    }
    // File
    file_level = fl;
    if (of != NULL) {
        file = fopen(of, "a");
        if (file == NULL) {
            not_ok = 1;
            file_level = LOGGER_OFF;
        }
    }
    else {
        file_level = LOGGER_OFF;
    }
    pthread_mutex_unlock(&logger_state_lock);
    return not_ok;
}

int logger_uninitialize() {
    pthread_mutex_lock(&logger_state_lock);
    int not_ok = 0;
    console_level = LOGGER_OFF;
    console = 0;
    console_color = 0;
    file_level = LOGGER_OFF;
    if (file != NULL) {
        if (fclose(file) == EOF) {
            not_ok = 1;
        }
        file = NULL;
    }
    pthread_mutex_unlock(&logger_state_lock);
    return not_ok;
}

void logger_puts(LogLevel l, const char* msg) {
    pthread_mutex_lock(&logger_state_lock);
    int log_console = console && check_loglevel_console(l);
    int log_file = file != NULL && check_loglevel_file(l);
    if (!log_console && !log_file) {
        // No output, early exit.
        pthread_mutex_unlock(&logger_state_lock);
        return;
    }
    // Allocate stack space.
    char bthread[THREAD_BUFLEN];
    char btime[TIME_BUFLEN];
    // Get thread name.
    if (pthread_getname_np(pthread_self(), bthread, THREAD_BUFLEN) != 0) {
        // Use default id if an error occurs.
        memcpy(bthread, THREAD_ERROR, sizeof(THREAD_ERROR) / sizeof(char));
    }
    // Fill time string.
    create_time(btime, TIME_BUFLEN);
    // Final string.
    char* final = concat(7, btime, " [", LEVEL2STRING[l], "|", bthread, "] ", msg);
    if (final == NULL) {
        pthread_mutex_unlock(&logger_state_lock);
        return;
    }
    // Send to output.
    if (log_console) {
        write_console(final, LEVEL2COLOR[l]);
    }
    if (log_file) {
        fputs(final, file);
        fputc('\n', file);
        fflush(file);
    }
    pthread_mutex_unlock(&logger_state_lock);
    free(final);
}

void logger_log(LogLevel l, const char* fmt, ...) {
    // Allocate stack space.
    char bmsg[MESSAGE_BUFLEN];
    // Format message.
    va_list vargs;
    va_start(vargs, fmt);
    int len = vsnprintf(bmsg, MESSAGE_BUFLEN, fmt, vargs);
    va_end(vargs);
    if (len >= 0) {
        // Output.
        logger_puts(l, bmsg);
    }
}
