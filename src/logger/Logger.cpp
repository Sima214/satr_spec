#include "Logger.hpp"

namespace spec {

Logger& Logger::get_instance() {
    static Logger instance;
    return instance;
}

}
