#ifndef SPEC_NTDLL
#define SPEC_NTDLL
/**
 * @file
 * @brief Internal interface to the ntdll. Windows only!
 */

#include <windows.h>
#include <ntstatus.h>
#include <winternl.h>

/**
 * Function declarations.
 */
NTSTATUS WINAPI NtDelayExecution(BOOLEAN, const LARGE_INTEGER*);
NTSTATUS WINAPI NtQueryTimerResolution(PULONG, PULONG, PULONG);
NTSTATUS WINAPI NtSetTimerResolution(ULONG, BOOLEAN, PULONG);

#endif /*SPEC_NTDLL*/
