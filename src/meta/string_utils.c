#include "string_utils.h"

#include <ctype.h>

bool is_string_blank_nullterm(const char* str) {
    const char* p = str;
    while (*p) {
        char c = *p++;
        if (!isspace(c)) {
            return false;
        }
    }
    return true;
}

bool is_string_blank_lenbound(const char* str, size_t len) {
    for (size_t i = 0; i < len; i++) {
        char c = str[i];
        if (!isspace(c)) {
            return false;
        }
    }
    return true;
}
