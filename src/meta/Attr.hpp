#include <macros.h>

#ifndef packed_struct
    /**
     * Forces the compiler to not add padding to structures.
     */
    #define packed_struct struct MARK_PACKED
#endif

#ifndef DEFINE_AUTO_PROPERTY
    /**
     * Automatically define setters and getters of Option/Factory classes.
     */
    #define DEFINE_AUTO_PROPERTY(name)                                                         \
        auto name() const {                                                                    \
            return _##name;                                                                    \
        }                                                                                      \
        auto name(decltype(_##name) name) {                                                    \
            _##name = name;                                                                    \
            return *this;                                                                      \
        }
#endif

#ifndef DEFINE_AUTO_VALIDATED_PROPERTY
    /**
     * Automatically define setters, getters and has_* of Option/Factory classes.
     */
    #define DEFINE_AUTO_VALIDATED_PROPERTY(name, invalid_value)                                \
        auto name() const {                                                                    \
            return _##name;                                                                    \
        }                                                                                      \
        bool has_##name() const {                                                              \
            return _##name != invalid_value;                                                   \
        }                                                                                      \
        auto name(decltype(_##name) name) {                                                    \
            _##name = name;                                                                    \
            return *this;                                                                      \
        }
#endif
