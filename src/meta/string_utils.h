#ifndef SPEC_STRING_UTILS
#define SPEC_STRING_UTILS
/**
 * @file
 * @brief
 */

#include <stdbool.h>
#include <stddef.h>

bool is_string_blank_nullterm(const char* str);
/**
 * NOTE: Doesn't take into account null terminators.
 */
bool is_string_blank_lenbound(const char* str, size_t len);

#endif /*SPEC_STRING_UTILS*/
