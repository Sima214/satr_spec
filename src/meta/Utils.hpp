#ifndef SPEC_UTILS_HPP
#define SPEC_UTILS_HPP
/**
 * @file
 * @brief Common C++ code.
 */

#include <cstdlib>

namespace spec {

/**
 * @brief Inheritable class with disabled copy constructors.
 */
class INonCopyable {
   public:
    INonCopyable& operator=(const INonCopyable&) = delete;
    INonCopyable(const INonCopyable&) = delete;

    INonCopyable& operator=(INonCopyable&&) = default;
    INonCopyable(INonCopyable&&) = default;

    INonCopyable() = default;
    ~INonCopyable() = default;
};

template<typename T> class FreeDeleter {
   public:
    void operator()(T* p) {
        std::free(p);
    }
};

}  // namespace spec

#endif /*SPEC_UTILS_HPP*/
