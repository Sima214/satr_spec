#ifndef SPEC_STRINGUTILS_HPP
#define SPEC_STRINGUTILS_HPP
/**
 * @file
 * @brief
 */

#include <string>
#include <string_view>

#include <macros.h>
C_DECLS_START
#include <string_utils.h>
C_DECLS_END

namespace spec {

inline bool is_string_blank(const char* str) {
    return is_string_blank_nullterm(str);
}
inline bool is_string_blank(const char* str, size_t len) {
    return is_string_blank_lenbound(str, len);
}
inline bool is_string_blank(const std::string& str) {
    return is_string_blank_lenbound(str.c_str(), str.size());
}
inline bool is_string_blank(std::string_view str) {
    return is_string_blank_lenbound(str.data(), str.size());
}

}  // namespace spec

#endif /*SPEC_STRINGUTILS_HPP*/