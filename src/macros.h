/**
 * @file
 * @brief Usefull macros I collected over years.
 */

#include <string.h>

/**
 * C/C++ syntax shortcuts.
 */
#ifndef C_DECLS_START
  #ifdef __cplusplus
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START extern "C" {
  #else
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START
  #endif
#endif
#ifndef C_DECLS_END
  #ifdef __cplusplus
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END }
  #else
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END
  #endif
#endif

/**
 * System detection.
 */
#ifndef IS_MACOS
  #define IS_MACOS ((__APPLE__) && (__MACH__))
#endif
#ifndef IS_LINUX
  #define IS_LINUX __linux__
#endif
#ifndef IS_WINDOWS
  #define IS_WINDOWS _WIN32
#endif
#ifndef IS_POSIX
  /**
   * True if compiling for a posix compliant platform.
   */
  #define IS_POSIX ((__unix__) || (IS_MACOS))
#endif
#ifndef IS_ARM
  /**
   * True if compiling for an ARM processor.
   */
  #define IS_ARM ((__arm__) || (__TARGET_ARCH_ARM))
#endif

/**
 * Utilities for creating error messages.
 */
#ifndef STRINGIFY
  #define STRINGIFY_DETAIL(x) #x
  /**
   * Given the token x, make it be "x".
   */
  #define STRINGIFY(x) STRINGIFY_DETAIL(x)
#endif

#ifndef FILE_LINE_STR
  /**
   * Creates a string literal with current source code location.
   * Useful for messages of fatal errors.
   */
  #define FILE_LINE_STR __FILE__ ":" STRINGIFY(__LINE__)
#endif

/**
 * GCC/Clang compiler attributes.
 */
#ifndef MARK_PRINTF
  /**
   * Marks a function which takes a format string like printf.
   */
  #define MARK_PRINTF(fmt_index, start_index) __attribute__((__format__(printf, fmt_index, start_index)))
#endif
#ifndef MARK_UNUSED
  /**
   * Suppresses 'not used' type warning messages.
   */
  #define MARK_UNUSED __attribute__((__unused__))
#endif
#ifndef MARK_PACKED
  /**
   * Forces the compiler to not add padding to structures.
   */
  #define MARK_PACKED __attribute__((__packed__))
#endif
#ifndef MARK_CONST
  /**
   * Marks the functions as 'const' which helps the compiler.
   * 'const' functions' result depend only on the value of their parameters,
   * aka they do not change the 'global' state.
   * Note that dereferencing a pointer is considered changing the 'global' state.
   */
  #define MARK_CONST __attribute__((__const__))
#endif
#ifndef MARK_PURE
  /**
   * Marks the functions as 'pure' which helps the compiler.
   * 'pure' functions' result depends only on the value of their parameters,
   * but are more relaxed about global state and pointers.
   */
  #define MARK_PURE __attribute__((__pure__))
#endif
#ifndef MARK_HOT
  /**
   * Marks a function as 'hot'.
   */
  #define MARK_HOT __attribute__((__hot__))
#endif
#ifndef MARK_COLD
  /**
   * Marks a function as 'cold'.
   */
  #define MARK_COLD __attribute__((__cold__))
#endif
#ifndef MARK_UNREACHABLE
  #define MARK_UNREACHABLE __builtin_unreachable();
#endif
#ifndef MARK_NORETURN
  #define MARK_NORETURN __attribute__((__noreturn__))
#endif
#ifndef COLD_BRANCH
  /**
   * Marks a branch as unlikely.
   * Use only when compiler is beeing stupid.
   */
  #define COLD_BRANCH(cond) __builtin_expect(cond, 0)
#endif
#ifndef HOT_BRANCH
  /**
   * Marks a branch as likely.
   * Use only when compiler is beeing stupid.
   */
  #define HOT_BRANCH(cond) __builtin_expect(cond, 1)
#endif
#ifndef MARK_MALLOC_ALIGNED
  /**
   * Marks that a function allocates memory and returns a pointer to said memory.
   */
  #define MARK_MALLOC_ALIGNED(alignment, ...) __attribute__((__malloc__, alloc_align(alignment), alloc_size(__VA_ARGS__)))
#endif
#ifndef MARK_MALLOC
  /**
   * Marks that a function allocates memory and returns a pointer to said memory.
   */
  #define MARK_MALLOC(...) __attribute__((__malloc__, alloc_size(__VA_ARGS__)))
#endif
#ifndef MARK_OBJ_ALLOC
  /**
   * Marks that a function that allocates memory for an opaque data type and returns a pointer to the new object.
   */
  #define MARK_OBJ_ALLOC __attribute__((__malloc__, warn_unused_result))
#endif
#ifndef MARK_NONNULL_ARGS
  /**
   * Marks arguments of a function as non-null.
   * This generates compile time warnings.
   * Indexing starts at 1.
   */
  #define MARK_NONNULL_ARGS(...) __attribute__((__nonnull__(__VA_ARGS__)))
#endif
#ifndef MARK_ALIAS
  /**
   * Links a symbol to `target`.
   */
  #define MARK_ALIAS(target) __attribute__((__alias__(#target)))
#endif
#ifndef FORCE_INLINE
  #ifndef NDEBUG
    /**
     * No-op in builds with debug symbols.
     */
    #define FORCE_INLINE
  #else
    /**
     * Forces a function to be always be inlined in optimized builds.
     */
    #define FORCE_INLINE __attribute__((__always_inline__))
  #endif
#endif

/**
 * Bitfields.
 */
#ifndef MASK_CREATE
  /**
   * Generates a mask with the bit-th bit turned on.
   */
  #define MASK_CREATE(bit) (0x1ll << (bit))
#endif
#ifndef MASK_TEST
  /**
   * Tests if all the bits of the mask are turned on in the input.
   */
  #define MASK_TEST(o, m) (!!(o & m))
#endif
#ifndef MASK_SET
  /**
   * Sets or unsets bits based on the mask according to the boolean
   * value v. The value of the first parameter is overwritten.
   */
  #define MASK_SET(o, m, v) o = ((~m) & o) | (v ? m : 0x0)
#endif
#ifndef MASK_TOOGLE
  /**
   * Toogles all the bits of the first parameter based on the mask.
   */
  #define MASK_TOOGLE(o, m) o ^= m
#endif
#ifndef EXTRACT_BYTE
  /**
   * Gets the \p i th byte of value.
   */
  #define EXTRACT_BYTE(v, i) (v >> (i * __CHAR_BIT__)) & 0xff;
#endif

/**
 * Advanced linker usage.
 */
#ifndef EXPORT_API
  #if defined(_WIN32)
    #ifndef SSCE_EXPORTS
      /**
       * Export symbol to linker.
       * Only affects shared builds.
       */
      #define EXPORT_API __declspec(dllimport)
    #else
      /**
       * Export symbol to linker.
       * Only affects shared builds.
       */
      #define EXPORT_API __declspec(dllexport)
    #endif
  #else
    /**
     * Export symbol to linker.
     * Only affects shared builds.
     */
    #define EXPORT_API __attribute__((__visibility__("default")))
  #endif
#endif
#ifndef EXPORT_API_RUNTIME
  #if defined(LINK_STATIC)
    /**
     * Defines a function which gets resolved at runtime.
     */
    #define EXPORT_API_RUNTIME
  #elif defined(LINK_ELF)
    /**
     * Defines a function which gets resolved at runtime.
     */
    #define EXPORT_API_RUNTIME(resolver) __attribute__((__ifunc__(#resolver)))
  #elif defined(LINK_MACHO)
    /**
     * Defines a function which gets resolved at runtime.
     */
    #define EXPORT_API_RUNTIME(name)                          \
      void* name ## _macho_resolver(void) __asm__("_" #name); \
      void* name ## _macho_resolver(void) {                   \
        __asm__(".symbol_resolver _" #name);                  \
        return resolve_ ## name();                            \
      }
  #elif defined(LINK_PE)
    /**
     * Defines a function which gets resolved at runtime.
     */
    #define EXPORT_API_RUNTIME
  #endif
#endif
#ifndef TARGET_EXT
  #if defined(NDEBUG) && defined(__clang__)
    /**
     * Mark this function as 'hand coded' for target instruction extension 'ext'.
     * Instructs the compiler to generate code for target extension while
     * also minimizing compiler optimizations.
     */
    #define TARGET_EXT(ext) __attribute__((__target__(#ext), minsize))
  #elif defined(NDEBUG) && defined(__GNUC__)
    /**
     * Mark this function as 'hand coded' for target instruction extension 'ext'.
     * Instructs the compiler to generate code for target extension while
     * also minimizing compiler optimizations.
     */
    #define TARGET_EXT(ext) __attribute__((__target__(#ext), optimize("no-tree-vectorize")))
  #elif !defined(NDEBUG)
    /**
     * Mark this function as 'hand coded' for target instruction extension 'ext'.
     * Instructs the compiler to generate code for target extension while
     * also minimizing compiler optimizations.
     */
    #define TARGET_EXT(ext) __attribute__((__target__(#ext)))
  #else
    #warning Unknown compiler: generated code might not be optimal!
    /**
     * Mark this function as 'hand coded' for target instruction extension 'ext'.
     * Instructs the compiler to generate code for target extension while
     * also minimizing compiler optimizations.
     */
    #define TARGET_EXT(ext)
  #endif
#endif

/**
 * Operating system inter-compatibility
 */
#ifndef ssce_mkdir
  #if IS_POSIX
    #include <sys/stat.h>
    /**
     * Creates a directory with default privileges.
     * 
     * @param dir The directory to create.
     * @return Returns zero on success.
     */
    #define ssce_mkdir(dir)       \
      ({                          \
        mode_t mask = umask(0);   \
        umask(mask);              \
        mkdir(dir, 0777 & ~mask); \
      })
  #elif defined(_WIN32)
    #include <direct.h>
    /**
     * Creates a directory with default privileges.
     * 
     * @param dir The directory to create.
     * @return Returns zero on success.
     */
    #define ssce_mkdir(dir) _mkdir(dir)
  #endif
#endif
#ifndef ssce_localtime
  #if IS_POSIX
    /**
     * Converts time into a time table in a thread safe way.
     * Returns zero on success.
     */
    #define ssce_localtime(time, table) (localtime_r(&time, &table) == NULL)
  #elif defined(_WIN32)
    /**
     * Converts time into a time table in a thread safe way.
     * Returns zero on success.
     */
    #define ssce_localtime(time, table) localtime_s(&table, &time)
  #else
    #error Unsupported OS
  #endif
#endif

/**
 * Stdlib extensions.
 */
#ifndef strequal
  /**
   * Tests if two strings are equal.
   */
  #define strequal(s1, s2) (strcmp(s1, s2) == 0)
#endif
#ifndef one_time_static_init
  /**
   * Execute some code only once.
   */
  #define one_time_static_init(variable_name, code)                                    \
    {                                                                                  \
      static int state_##variable_name = 0;                                            \
      static pthread_mutex_t lock_##variable_name = PTHREAD_MUTEX_INITIALIZER;         \
      pthread_mutex_lock(&lock_##variable_name);                                       \
      if (!state_##variable_name) {                                                    \
        code;                                                                          \
        state_##variable_name = 1;                                                     \
      };                                                                               \
      pthread_mutex_unlock(&lock_##variable_name);                                     \
    }
#endif

/**
 * Math extensions.
 */
#ifndef math_max
  /**
   * Returns the maximum of the two inputs.
   */
  #define math_max(a, b)      \
    ({                        \
      __typeof__(a) _a = (a); \
      __typeof__(b) _b = (b); \
      _a > _b ? _a : _b;      \
    })
#endif
#ifndef math_min
  /**
   * Returns the minimum of the two inputs.
   */
  #define math_min(a, b)      \
    ({                        \
      __typeof__(a) _a = (a); \
      __typeof__(b) _b = (b); \
      _a < _b ? _a : _b;      \
    })
#endif
#ifndef math_clamp
  /**
   * Returns the \p v clamped between \p a and \p b.
   * It is assumed that \p a <= \p b.
   */
  #define math_clamp(v, a, b)      \
    ({                             \
      __typeof__(a) _a = (a);      \
      __typeof__(b) _b = (b);      \
      __typeof__(v) _v = (v);      \
      math_max(math_min(v, b), a); \
    })
#endif

/**
 * Global constants.
 */
#ifndef MILLI2MICRO
  #define MILLI2MICRO 1000
#endif
#ifndef MILLI2SEC
  #define MILLI2SEC 1000
#endif
#ifndef MICRO2NANO
  #define MICRO2NANO 1000
#endif
#ifndef MICRO2SEC
  #define MICRO2SEC 1000000
#endif
#ifndef MSEC2NANO
  #define MSEC2NANO 1000000
#endif
#ifndef SEC2NANO
  #define SEC2NANO 1000000000
#endif
