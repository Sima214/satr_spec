#ifndef SPEC_FILEDESCRIPTOR_HPP
#define SPEC_FILEDESCRIPTOR_HPP
/**
 * @file
 * @brief Direct wrapper for synchronous I/O methods of the operating system(C++ wrapper).
 */

#include <Flags.hpp>
#include <Utils.hpp>

#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <string>
#include <string_view>
#include <utility>

#include <macros.h>
C_DECLS_START
#include <file_descriptor.h>
C_DECLS_END

namespace spec {

class FileDescriptor : public INonCopyable {
   public:
    enum class OpenFlagBits : int {
        Read = FILE_DESCRIPTOR_OPEN_READ_BIT,
        Write = FILE_DESCRIPTOR_OPEN_WRITE_BIT,
        Truncate = FILE_DESCRIPTOR_OPEN_TRUNCATE_BIT,
    };
    using OpenFlags = Flags<OpenFlagBits>;

    enum class SeekDirection {
        Current = FILE_DESCRIPTOR_SEEK_DIR_CURRENT,
        Start = FILE_DESCRIPTOR_SEEK_DIR_START,
        End = FILE_DESCRIPTOR_SEEK_DIR_END,
    };

    enum class FileStates {
        Eof = FILE_DESCRIPTOR_STATE_EOF,
        Good = FILE_DESCRIPTOR_STATE_GOOD,
        Error = FILE_DESCRIPTOR_STATE_ERROR,
        Invalid = FILE_DESCRIPTOR_STATE_INVALID,
    };

    enum class FileTypes {
        Invalid = FILE_DESCRIPTOR_TYPE_INVALID,
        Regular = FILE_DESCRIPTOR_TYPE_REGULAR,
        Directory = FILE_DESCRIPTOR_TYPE_DIRECTORY,
        Link = FILE_DESCRIPTOR_TYPE_LINK,
        Device = FILE_DESCRIPTOR_TYPE_DEVICE,
        Pipe = FILE_DESCRIPTOR_TYPE_PIPE,
        Unknown = FILE_DESCRIPTOR_TYPE_UNKNOWN,
    };

   protected:
    file_descriptor_t* _handle = nullptr;

   public:
    /* Open constructors */
    FileDescriptor(const char* path, OpenFlags flags) :
        _handle(file_descriptor_open(path,
                                     (FileDescriptorOpenFlagBits) static_cast<int>(flags))) {}
    explicit FileDescriptor(const std::string& path, OpenFlags flags) :
        FileDescriptor(path.c_str(), flags) {}
    explicit FileDescriptor(std::string_view path, OpenFlags flags) :
        FileDescriptor(path.data(), flags) {}
    explicit FileDescriptor(const std::filesystem::path& path, OpenFlags flags) :
        FileDescriptor(path.string(), flags) {}
    /* Adapt constructors */
    explicit FileDescriptor(file_descriptor_native_t handle) :
        _handle(file_descriptor_adapt(handle)) {}

    FileDescriptor() = default;
    FileDescriptor(FileDescriptor&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    FileDescriptor& operator=(FileDescriptor&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }
    ~FileDescriptor() {
        if (_handle != nullptr) {
            file_descriptor_free(_handle);
            _handle = nullptr;
        }
    }
    /**
     * Tests if the C++ wrapper contains an underlying object.
     *
     * Use the method \ref is_valid to test
     * whether the underlying object is valid.
     */
    operator bool() const {
        return _handle != nullptr;
    }
    operator file_descriptor_t*() const {
        return _handle;
    }

    bool is_valid() const {
        return file_descriptor_is_valid(_handle);
    }
    FileStates get_status() const {
        return static_cast<FileStates>(file_descriptor_get_status(_handle));
    }
    bool get_eof() const {
        return file_descriptor_get_eof(_handle);
    }
    file_descriptor_native_t get_native_handle() const {
        return file_descriptor_get_native_handle(_handle);
    }
    size_t get_size() const {
        return file_descriptor_get_size(_handle);
    }
    size_t get_pos() const {
        return file_descriptor_get_pos(_handle);
    }
    FileTypes get_type() const {
        return static_cast<FileTypes>(file_descriptor_get_type(_handle));
    }

    size_t read(uint8_t* data, size_t data_n) const {
        return file_descriptor_read(_handle, data, data_n);
    }
    size_t write(const uint8_t* data, size_t data_n) const {
        return file_descriptor_write(_handle, data, data_n);
    }
    int64_t seek(int64_t offset, SeekDirection direction) const {
        return file_descriptor_seek(_handle, offset, (FileDescriptorSeekDirection) direction);
    }
    bool flush() const {
        return file_descriptor_flush(_handle);
    }
    bool close() const {
        return file_descriptor_close(_handle);
    }
};

// TODO: shared_ptr lifecycle management for FileDescriptor.

}  // namespace spec

namespace spec {

template<> struct FlagTraits<spec::FileDescriptor::OpenFlagBits> {
    static constexpr bool isBitmask = true;
    static constexpr spec::FileDescriptor::OpenFlags allFlags =
         spec::FileDescriptor::OpenFlagBits::Read | spec::FileDescriptor::OpenFlagBits::Write |
         spec::FileDescriptor::OpenFlagBits::Truncate;
};

}  // namespace spec

#endif /*SPEC_FILEDESCRIPTOR_HPP*/
