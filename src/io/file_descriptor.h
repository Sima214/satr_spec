#ifndef SPEC_FILE_DESCRIPTOR
#define SPEC_FILE_DESCRIPTOR
/**
 * @file
 * @brief Direct wrapper for synchronous I/O methods of the operating system.
 */

#include <macros.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if IS_POSIX
    #define file_descriptor_native_t int
#elif IS_WINDOWS
    #include <windows.h>
    #define file_descriptor_native_t HANDLE
#else
    #error Unsupported operating system for native strings!
#endif

struct _file_descriptor_t;
typedef struct _file_descriptor_t file_descriptor_t;

typedef enum {
    FILE_DESCRIPTOR_OPEN_READ_BIT = MASK_CREATE(0),
    FILE_DESCRIPTOR_OPEN_WRITE_BIT = MASK_CREATE(1),
    FILE_DESCRIPTOR_OPEN_TRUNCATE_BIT = MASK_CREATE(2),
} FileDescriptorOpenFlagBits;

typedef enum {
    /** Seek relative to current position */
    FILE_DESCRIPTOR_SEEK_DIR_CURRENT,
    FILE_DESCRIPTOR_SEEK_DIR_START,
    FILE_DESCRIPTOR_SEEK_DIR_END,
} FileDescriptorSeekDirection;

typedef enum {
    FILE_DESCRIPTOR_STATE_EOF = -1,
    FILE_DESCRIPTOR_STATE_GOOD = 0,
    /** An unknown error has occured */
    FILE_DESCRIPTOR_STATE_ERROR,
    /** Invalid handle */
    FILE_DESCRIPTOR_STATE_INVALID,
} FileDescriptorFileStates;

typedef enum {
    FILE_DESCRIPTOR_TYPE_INVALID = -1,
    FILE_DESCRIPTOR_TYPE_REGULAR = 0,
    FILE_DESCRIPTOR_TYPE_DIRECTORY,
    /** Open file is a symbolic link. */
    FILE_DESCRIPTOR_TYPE_LINK,
    /** Character and block devices. */
    FILE_DESCRIPTOR_TYPE_DEVICE,
    /** Named pipes, sockets - anything fifo-like. */
    FILE_DESCRIPTOR_TYPE_PIPE,
    FILE_DESCRIPTOR_TYPE_UNKNOWN,
} FileDescriptorFileTypes;

/**
 * Open a file at the specified path with the given flags.
 *
 * @param path  The UTF-8 encoded path of the file to open.
 * @param flags A bitmask of flags from the FileDescriptorOpenFlagBits enum.
 *
 * @return A new file_descriptor_t object representing the opened file,
 *         or NULL on error.
 */
file_descriptor_t* file_descriptor_open(const char* path, FileDescriptorOpenFlagBits flags)
     MARK_NONNULL_ARGS(1) MARK_OBJ_ALLOC;

/**
 * Adapt an existing native file handle to a file_descriptor_t object.
 *
 * @param native_handle An already open file descriptor/handle.
 *
 * @return A new file_descriptor_t object representing the adapted handle,
 *         or NULL on error.
 */
file_descriptor_t* file_descriptor_adapt(file_descriptor_native_t native_handle) MARK_OBJ_ALLOC;

/**
 * Read data from the file descriptor into the provided buffer.
 *
 * @param fd      The file_descriptor_t object to read from.
 * @param data    The buffer to store the read data.
 * @param data_n  The maximum number of bytes to read.
 *
 * @return The number of bytes read.
 */
size_t file_descriptor_read(file_descriptor_t* fd, uint8_t* data, size_t data_n)
     MARK_NONNULL_ARGS(1, 2);

/**
 * Write data from the provided buffer to the file descriptor.
 *
 * @param fd      The file_descriptor_t object to write to.
 * @param data    The data buffer to write.
 * @param data_n  The number of bytes to write.
 *
 * @return The number of bytes written.
 */
size_t file_descriptor_write(file_descriptor_t* fd, const uint8_t* data, size_t data_n)
     MARK_NONNULL_ARGS(1, 2);

/**
 * Move the file descriptor's position within the file.
 *
 * @param fd        The file_descriptor_t object to perform seek on.
 * @param offset    The offset to move to.
 * @param direction The direction in which to seek.
 *
 * @return The resulting offset from the start of the file, or -1LL on error.
 */
int64_t file_descriptor_seek(file_descriptor_t* fd, int64_t offset,
                             FileDescriptorSeekDirection direction) MARK_NONNULL_ARGS(1);
// TODO: truncate

/**
 * Check if the file descriptor is valid/open.
 *
 * @param fd The file_descriptor_t object to check.
 *
 * @return true if the file descriptor is valid/open, false otherwise.
 */
bool file_descriptor_is_valid(const file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Get the status of the file descriptor.
 *
 * @param fd The file_descriptor_t object to query.
 *
 * @return The file status as a value from FileDescriptorFileStates enum.
 */
FileDescriptorFileStates file_descriptor_get_status(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Check if the file descriptor has reached the end of the file.
 *
 * @param fd The file_descriptor_t object to check.
 *
 * @return true if end of file is reached, false otherwise.
 */
bool file_descriptor_get_eof(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Get the native handle associated with the file descriptor.
 *
 * @param fd The file_descriptor_t object to query.
 *
 * @return The native file handle.
 */
file_descriptor_native_t file_descriptor_get_native_handle(const file_descriptor_t* fd)
     MARK_NONNULL_ARGS(1);

/**
 * Get the size of the file.
 *
 * @param fd The file_descriptor_t object to query.
 *
 * @return The size of the file in bytes.
 */
size_t file_descriptor_get_size(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Get the current offset within the file.
 *
 * @param fd The file_descriptor_t object to query.
 *
 * @return The current offset from the beginning of the file, or SIZE_MAX on error.
 */
size_t file_descriptor_get_pos(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Get the type of the file descriptor.
 *
 * @param fd The file_descriptor_t object to query.
 *
 * @return The type of the file descriptor as a value from FileDescriptorFileTypes enum.
 */
FileDescriptorFileTypes file_descriptor_get_type(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Flush any buffered data to the file.
 *
 * @param fd The file_descriptor_t object to flush.
 *
 * @return true if flushing is successful, false otherwise.
 */
bool file_descriptor_flush(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Close the file descriptor.
 *
 * @param fd The file_descriptor_t object to close.
 *
 * @return true if closing is successful, false otherwise.
 */
bool file_descriptor_close(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

/**
 * Close the file descriptor if open and free any allocated space.
 *
 * @param fd The file_descriptor_t object to close and free.
 */
void file_descriptor_free(file_descriptor_t* fd) MARK_NONNULL_ARGS(1);

#endif /*SPEC_FILE_DESCRIPTOR*/