#include "mapped_file.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <trace.h>

typedef struct {
    void* ptr;
    size_t len;
} _mapped_file_t;

mapped_file_t* mapped_file_create(file_descriptor_t* fd, MappedFileCreateFlags flags,
                                  MappedFileAdvice advice) {
    if (!file_descriptor_is_valid(fd)) {
        return NULL;
    }
    int mfd = file_descriptor_get_native_handle(fd);
    size_t len = file_descriptor_get_size(fd);
    if (len == 0) {
        TRACE("mapped_file_create: cannot map empty files!");
        return NULL;
    }

    int prot = PROT_NONE;
    if (flags & MAPPED_FILE_CREATE_READ_ACCESS_BIT) {
        prot |= PROT_READ;
    }
    if (flags & MAPPED_FILE_CREATE_WRITE_ACCESS_BIT) {
        prot |= PROT_WRITE;
    }
    if (flags & MAPPED_FILE_CREATE_EXEC_ACCESS_BIT) {
        prot |= PROT_EXEC;
    }

    int mflags = 0;
    if (flags & MAPPED_FILE_CREATE_SHARED_BIT) {
        mflags |= MAP_SHARED;
    }
    else {
        mflags |= MAP_PRIVATE;
    }

    int madv;
    switch (advice) {
        case MAPPED_FILE_ADVICE_NORMAL_ACCESS_PATTERN: {
            madv = MADV_NORMAL;
        } break;
        case MAPPED_FILE_ADVICE_SEQUENTIAL_ACCESS_PATTERN: {
            madv = MADV_SEQUENTIAL;
        } break;
        case MAPPED_FILE_ADVICE_RANDOM_ACCESS_PATTERN: {
            madv = MADV_RANDOM;
        } break;
        default: {
            FAILF("mapped_file_create: invalid value(%i) for MappedFileAdvice enum.", advice);
        }
    }

    errno = 0;
    void* ptr = mmap(NULL, len, prot, mflags, mfd, 0);
    if (ptr == MAP_FAILED) {
        TRACF("mapped_file_create: couldn't create mapping(%s)!", strerror(errno));
        return NULL;
    }

    errno = 0;
    int status = madvise(ptr, len, madv);
    if (status != 0) {
        /* Just issue a warning. */
        TRACF("mapped_file_create: failure to apply advice(%s).", strerror(errno));
    }

    _mapped_file_t* obj = malloc(sizeof(_mapped_file_t));
    if (obj == NULL) {
        FAIL("mapped_file_create: malloc failed!");
    }
    obj->ptr = ptr;
    obj->len = len;
    return (mapped_file_t*) obj;
}

bool mapped_file_is_valid(const mapped_file_t* mf) {
    const _mapped_file_t* obj = (const _mapped_file_t*) mf;
    return obj->ptr != NULL && obj->len != 0;
}
void* mapped_file_get_ptr(const mapped_file_t* mf) {
    const _mapped_file_t* obj = (const _mapped_file_t*) mf;
    return obj->ptr;
}
size_t mapped_file_get_size(const mapped_file_t* mf) {
    const _mapped_file_t* obj = (const _mapped_file_t*) mf;
    return obj->len;
}

bool mapped_file_sync(mapped_file_t* mf) {
    _mapped_file_t* obj = (_mapped_file_t*) mf;
    errno = 0;
    int status = msync(obj->ptr, obj->len, MS_ASYNC);
    if (status != 0) {
        TRACF("mapped_file_sync: msync error(%s)!", strerror(errno));
        return false;
    }
    return true;
}
bool mapped_file_unmap(mapped_file_t* mf) {
    _mapped_file_t* obj = (_mapped_file_t*) mf;
    errno = 0;
    int status = munmap(obj->ptr, obj->len);
    if (status != 0) {
        TRACF("mapped_file_unmap: munmap error(%s)!", strerror(errno));
        return false;
    }
    obj->ptr = NULL;
    obj->len = 0;
    return true;
}
void mapped_file_free(mapped_file_t* mf) {
    _mapped_file_t* obj = (_mapped_file_t*) mf;
    if (obj->ptr != NULL && obj->len != 0) {
        errno = 0;
        int status = munmap(obj->ptr, obj->len);
        if (status != 0) {
            // Cannot unmap - just issue a warning for the leak.
            TRACF("mapped_file_free: munmap error(%s).", strerror(errno));
        }
    }
    free(obj);
}
