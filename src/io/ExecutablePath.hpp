#ifndef SPEC_EXECUTABLEPATH_HPP
#define SPEC_EXECUTABLEPATH_HPP
/**
 * @file
 * @brief
 */

#include <macros.h>
C_DECLS_START
#include <executable_path.h>
C_DECLS_END

#include <memory>

namespace spec::exec_fp {

namespace detail {
class AutoFree {
   public:
    void operator()(const char* p) {
        retrieve_executable_filepath_free(p);
    }
};
}  // namespace detail

inline std::unique_ptr<const char, detail::AutoFree> retrieve() {
    const char* h = retrieve_executable_filepath();
    std::unique_ptr<const char, detail::AutoFree> r(h);
    return r;
}

}  // namespace spec::exec_fp

#endif /*SPEC_EXECUTABLEPATH_HPP*/