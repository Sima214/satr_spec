#include <macros.h>

#if IS_POSIX
    #include "mapped_file_unix.c"
#elif IS_WINDOWS
    #include "mapped_file_win32.c"
#else
    #error Unsupported operating system for I/O memory mapped files!
#endif
