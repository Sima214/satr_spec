#ifndef SPEC_EXECUTABLE_PATH
#define SPEC_EXECUTABLE_PATH
/**
 * @file
 * @brief Retrieve the filesystem path to the executable of this process.
 */

#include <stddef.h>

/**
 * Retrieves the path to the currently executing file.
 * 
 * \return A heap allocated null terminated string. Must be free()d when no longer needed.
 * Can return null on failure.
 */
const char* retrieve_executable_filepath();
void retrieve_executable_filepath_free(const char* path);

#endif /*SPEC_EXECUTABLE_PATH*/