#include "executable_path.h"

#include <natstr.h>
#include <stdlib.h>
#include <trace.h>
#include <windows.h>

/**
 * The following implementation depends on behavior after Windows XP.
 */

const char* retrieve_executable_filepath() {
    size_t buf_size = MAX_PATH;
    wchar_t* buf = malloc(buf_size * sizeof(wchar_t));

    // Bound time and memory resources.
    while (buf_size <= 1048576) {
        if (buf == NULL) {
            TRACE("retrieve_executable_filepath: allocation failure!");
            return NULL;
        }
        SetLastError(ERROR_SUCCESS);
        DWORD len = GetModuleFileNameW(NULL, buf, buf_size);
        DWORD err = GetLastError();

        if (err == ERROR_INSUFFICIENT_BUFFER) {
            // Possibly path is longer than currently allocated buffer.
            buf_size += buf_size / 2;
            buf = realloc(buf, buf_size * sizeof(wchar_t));
            continue;
        }
        else if (err != ERROR_SUCCESS) {
            // Report error.
            TRACF("retrieve_executable_filepath: GetModuleFileName error(%d)!", err);
            free(buf);
            return NULL;
        }
        else {
            // Successfully retrieved path - convert encoding.
            const char* u8buf = natstr_native_to_utf8(buf);
            free(buf);
            return u8buf;
        }
    }

    TRACE("retrieve_executable_filepath: reached max resource bound!");
    free(buf);
    return NULL;
}

void retrieve_executable_filepath_free(const char* path) {
    natstr_free(path);
}
