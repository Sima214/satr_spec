#ifndef SPEC_MAPPED_FILE
#define SPEC_MAPPED_FILE
/**
 * @file
 * @brief This file contains utility functions for creating
 * memory mappings from file descriptor objects.
 */

#include <file_descriptor.h>
#include <macros.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct _mapped_file_t;
typedef struct _mapped_file_t mapped_file_t;

typedef enum {
    MAPPED_FILE_CREATE_READ_ACCESS_BIT = MASK_CREATE(0),
    MAPPED_FILE_CREATE_WRITE_ACCESS_BIT = MASK_CREATE(1),
    MAPPED_FILE_CREATE_EXEC_ACCESS_BIT = MASK_CREATE(2),
    /** Mapping is shared with other processes. */
    MAPPED_FILE_CREATE_SHARED_BIT = MASK_CREATE(3),
} MappedFileCreateFlags;

typedef enum {
    MAPPED_FILE_ADVICE_NORMAL_ACCESS_PATTERN = 0,
    MAPPED_FILE_ADVICE_SEQUENTIAL_ACCESS_PATTERN = 1,
    MAPPED_FILE_ADVICE_RANDOM_ACCESS_PATTERN = 2
} MappedFileAdvice;

/**
 * @brief Creates a mapped file object from a file descriptor.
 *
 * @param fd A pointer to the file descriptor representing the file to be mapped.
 * @param flags Flags indicating the access permissions and other properties.
 * @param advice Advice for optimizing access pattern to the mapped file.
 * @return A pointer to the created mapped file object, or NULL on failure.
 */
mapped_file_t* mapped_file_create(file_descriptor_t* fd, MappedFileCreateFlags flags,
                                  MappedFileAdvice advice) MARK_NONNULL_ARGS(1) MARK_OBJ_ALLOC;
/**
 * @brief Checks if a mapped file object is valid.
 *
 * @param mf A pointer to the mapped file object.
 * @return true if the mapped file object is valid, false otherwise.
 */
bool mapped_file_is_valid(const mapped_file_t* mf) MARK_NONNULL_ARGS(1);
/**
 * @brief Retrieves the starting address of the mapping.
 *
 * @param mf A pointer to the mapped file object.
 * @return The pointer to the mapped file's data.
 */
void* mapped_file_get_ptr(const mapped_file_t* mf) MARK_NONNULL_ARGS(1);
/**
 * @brief Retrieves the size of the mapping.
 *
 * @param mf A pointer to the mapped file object.
 * @return The size of the mapped file in bytes.
 */
size_t mapped_file_get_size(const mapped_file_t* mf) MARK_NONNULL_ARGS(1);
/**
 * @brief Synchronizes/Flushes changes in the mapped file with the underlying storage.
 *
 * @param mf A pointer to the mapped file object.
 * @return true if synchronization was successful, false otherwise.
 */
bool mapped_file_sync(mapped_file_t* mf) MARK_NONNULL_ARGS(1);
/**
 * @brief Unmaps a mapped file from memory.
 *
 * @param mf A pointer to the mapped file object.
 * @return true if unmapping was successful, false otherwise.
 */
bool mapped_file_unmap(mapped_file_t* mf) MARK_NONNULL_ARGS(1);
/**
 * @brief Frees the resources associated with a mapped file object.
 *
 * @param mf A pointer to the mapped file object to be freed.
 */
void mapped_file_free(mapped_file_t* mf) MARK_NONNULL_ARGS(1);

#endif /*SPEC_MAPPED_FILE*/