#include "executable_path.h"

#include <errno.h>
#include <macros.h>
#include <stdlib.h>
#include <trace.h>
#include <unistd.h>

#if IS_LINUX
    #define SELF_EXEC_LINK "/proc/self/exe"
#else
    #error Unsupported unix operating system for retrieve_executable_filepath!
#endif

const char* retrieve_executable_filepath() {
    size_t buf_size = 800;
    char* buf = malloc(buf_size);

    // Bound time and memory resources.
    while (buf_size <= 1048576) {
        if (buf == NULL) {
            TRACE("retrieve_executable_filepath: allocation failure!");
            return NULL;
        }
        errno = 0;
        ssize_t status = readlink(SELF_EXEC_LINK, buf, buf_size);

        if (status < 0) {
            // Report error.
            TRACF("retrieve_executable_filepath: readlink error(%s)!", strerror(errno));
            free(buf);
            return NULL;
        }
        else if (((size_t) status) >= buf_size) {
            // Possibly path is longer than currently allocated buffer.
            buf_size += buf_size / 2;
            buf = realloc(buf, buf_size);
            continue;
        }
        else {
            // Successfully retrieved path - need to add null terminator.
            buf[status] = '\0';
            return buf;
        }
    }

    TRACE("retrieve_executable_filepath: reached max resource bound!");
    free(buf);
    return NULL;
}

void retrieve_executable_filepath_free(const char* path) {
    free((void*) path);
}
