#ifndef SPEC_MAPPEDFILE_HPP
#define SPEC_MAPPEDFILE_HPP
/**
 * @file
 * @brief This file contains utility functions for creating
 * memory mappings from file descriptor objects (C++ wrapper).
 */

#include <FileDescriptor.hpp>
#include <Flags.hpp>
#include <Utils.hpp>

#include <cstdint>

#include <macros.h>

C_DECLS_START
#include <mapped_file.h>
C_DECLS_END

namespace spec {

class MappedFile : public spec::INonCopyable {
   public:
    enum class CreateFlagBits : int {
        ReadAccess = MAPPED_FILE_CREATE_READ_ACCESS_BIT,
        WriteAccess = MAPPED_FILE_CREATE_WRITE_ACCESS_BIT,
        ExecAccess = MAPPED_FILE_CREATE_EXEC_ACCESS_BIT,
        Shared = MAPPED_FILE_CREATE_SHARED_BIT,
    };
    using CreateFlags = spec::Flags<CreateFlagBits>;

    enum class Advice {
        NormalAccessPattern = MAPPED_FILE_ADVICE_NORMAL_ACCESS_PATTERN,
        SequentialAccessPattern = MAPPED_FILE_ADVICE_SEQUENTIAL_ACCESS_PATTERN,
        RandomAccessPattern = MAPPED_FILE_ADVICE_RANDOM_ACCESS_PATTERN
    };

   protected:
    mapped_file_t* _handle = nullptr;

   public:
    MappedFile(const FileDescriptor& fd, CreateFlags flags,
               Advice advice = Advice::NormalAccessPattern) :
        _handle(mapped_file_create(static_cast<file_descriptor_t*>(fd),
                                   (MappedFileCreateFlags) static_cast<int>(flags),
                                   (MappedFileAdvice) advice)) {}

    MappedFile() = default;
    MappedFile(MappedFile&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    MappedFile& operator=(MappedFile&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }
    ~MappedFile() {
        if (_handle != nullptr) {
            mapped_file_free(_handle);
            _handle = nullptr;
        }
    }
    /**
     * Tests if the C++ wrapper contains an underlying object.
     *
     * Use the method \ref is_valid to test
     * whether the underlying object is valid.
     */
    operator bool() const {
        return _handle != nullptr;
    }
    operator mapped_file_t*() const {
        return _handle;
    }

    bool is_valid() const {
        return mapped_file_is_valid(_handle);
    }
    void* get_ptr() const {
        return mapped_file_get_ptr(_handle);
    }
    size_t get_size() const {
        return mapped_file_get_size(_handle);
    }
    bool sync() const {
        return mapped_file_sync(_handle);
    }
    bool unmap() const {
        return mapped_file_unmap(_handle);
    }

    void* operator*() const {
        return get_ptr();
    }
    uint8_t* begin() const {
        return reinterpret_cast<uint8_t*>(get_ptr());
    }
    uint8_t* end() const {
        return begin() + get_size();
    }
};

}  // namespace spec

namespace spec {

template<> struct FlagTraits<spec::MappedFile::CreateFlagBits> {
    static constexpr bool isBitmask = true;
    static constexpr spec::MappedFile::CreateFlags allFlags =
         spec::MappedFile::CreateFlagBits::ReadAccess |
         spec::MappedFile::CreateFlagBits::WriteAccess |
         spec::MappedFile::CreateFlagBits::ExecAccess |
         spec::MappedFile::CreateFlagBits::Shared;
};

}  // namespace spec

#endif /*SPEC_MAPPEDFILE_HPP*/
