#include <macros.h>

#if IS_POSIX
    #include "file_descriptor_unix.c"
#elif IS_WINDOWS
    #include "file_descriptor_win32.c"
#else
    #error Unsupported operating system for I/O file descriptors!
#endif
