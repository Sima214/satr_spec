#include <macros.h>

#if IS_POSIX
    #include "executable_path_unix.c"
#elif IS_WINDOWS
    #include "executable_path_win32.c"
#else
    #error Unsupported operating system for retrieve_executable_filepath!
#endif
