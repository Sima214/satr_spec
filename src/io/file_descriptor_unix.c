#include "file_descriptor.h"

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <trace.h>
#include <unistd.h>

typedef struct {
    int fd;
    int last_errno;
} _file_descriptor_t;

const mode_t DEFAULT_CREATE_MODE = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

file_descriptor_t* file_descriptor_open(const char* path, FileDescriptorOpenFlagBits flags) {
    int open_flags = 0;
    if (flags & FILE_DESCRIPTOR_OPEN_READ_BIT && flags & FILE_DESCRIPTOR_OPEN_WRITE_BIT) {
        open_flags |= O_RDWR | O_CREAT;
    }
    else {
        if (flags & FILE_DESCRIPTOR_OPEN_READ_BIT) {
            open_flags |= O_RDONLY;
        }
        if (flags & FILE_DESCRIPTOR_OPEN_WRITE_BIT) {
            open_flags |= O_WRONLY | O_CREAT;
        }
    }
    if (flags & FILE_DESCRIPTOR_OPEN_TRUNCATE_BIT && flags & FILE_DESCRIPTOR_OPEN_WRITE_BIT) {
        open_flags |= O_TRUNC;
    }

    errno = 0;
    int fd = -1;
    if (flags & FILE_DESCRIPTOR_OPEN_WRITE_BIT) {
        fd = open(path, open_flags, DEFAULT_CREATE_MODE);
    }
    else {
        fd = open(path, open_flags);
    }

    if (fd < 0) {
        // Failure to open.
        TRACF("file_descriptor_open: couldn't open `%s`(%s)!", path, strerror(errno));
        return NULL;
    }
    _file_descriptor_t* obj = malloc(sizeof(_file_descriptor_t));
    if (obj == NULL) {
        FAIL("file_descriptor_open: malloc failed!");
    }
    obj->fd = fd;
    obj->last_errno = 0;
    return (file_descriptor_t*) obj;
}

file_descriptor_t* file_descriptor_adapt(file_descriptor_native_t fd) {
    if (fd < 0) {
        return NULL;
    }
    else {
        _file_descriptor_t* obj = malloc(sizeof(_file_descriptor_t));
        obj->fd = fd;
        obj->last_errno = 0;
        return (file_descriptor_t*) obj;
    }
}

size_t file_descriptor_read(file_descriptor_t* fd, uint8_t* data, size_t data_n) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    ssize_t s = read(obj->fd, data, data_n);
    obj->last_errno = errno;
    if (s < 0) {
        return 0;
    }
    return s;
}

size_t file_descriptor_write(file_descriptor_t* fd, const uint8_t* data, size_t data_n) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    ssize_t s = write(obj->fd, data, data_n);
    obj->last_errno = errno;
    if (s < 0) {
        return 0;
    }
    return s;
}

int64_t file_descriptor_seek(file_descriptor_t* fd, int64_t offset,
                             FileDescriptorSeekDirection direction) {
    int whence = 0;
    switch (direction) {
        case FILE_DESCRIPTOR_SEEK_DIR_START: {
            whence = SEEK_SET;
        } break;
        case FILE_DESCRIPTOR_SEEK_DIR_CURRENT: {
            whence = SEEK_CUR;
        } break;
        case FILE_DESCRIPTOR_SEEK_DIR_END: {
            whence = SEEK_END;
        } break;
        default: {
            FAIL("Runtime error: invalid enum.");
        }
    }

    // const off_t ERR_STATUS = (off_t) -1L;
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    off_t new_offset = lseek(obj->fd, offset, whence);
    obj->last_errno = errno;
    return new_offset;
}

bool file_descriptor_is_valid(const file_descriptor_t* fd) {
    const _file_descriptor_t* obj = (const _file_descriptor_t*) fd;
    if (obj->fd < 0) {
        return false;
    }
    // Test if fd is valid:
    errno = 0;
    int flags = fcntl(obj->fd, F_GETFD);
    return flags != -1 || errno != EBADF;
}
FileDescriptorFileStates file_descriptor_get_status(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    if (obj->last_errno == 0) {
        if (obj->fd < 0) {
            return FILE_DESCRIPTOR_STATE_INVALID;
        }
        bool eof = file_descriptor_get_eof(fd);
        if (obj->last_errno != 0) {
            TRACF("file_descriptor_get_status: error while "
                  "getting eof status of a good file_descriptor_t!");
            obj->last_errno = 0;
        }
        if (eof) {
            return FILE_DESCRIPTOR_STATE_EOF;
        }
        else {
            return FILE_DESCRIPTOR_STATE_GOOD;
        }
    }
    else {
        return FILE_DESCRIPTOR_STATE_ERROR;
    }
}
bool file_descriptor_get_eof(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    char buffer;
    errno = 0;
    ssize_t c = read(obj->fd, &buffer, 1);
    obj->last_errno = errno;

    if (c == 0) {
        return true;
    }
    else if (c == -1) { /* Error: */
        return false;
    }
    else { /* c == 1 */
        // revert to original position
        errno = 0;
        if (lseek(obj->fd, -1, SEEK_CUR) == -1) {
            TRACF("file_descriptor_get_eof: error while reverting to original position!");
        }
        obj->last_errno = errno;
        return false;
    }
}
file_descriptor_native_t file_descriptor_get_native_handle(const file_descriptor_t* fd) {
    const _file_descriptor_t* obj = (const _file_descriptor_t*) fd;
    return obj->fd;
}
size_t file_descriptor_get_size(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    struct stat stats;
    errno = 0;
    int status = fstat(obj->fd, &stats);
    obj->last_errno = errno;
    if (status != 0) { /* Error: */
        return SIZE_MAX;
    }
    return stats.st_size;
}
size_t file_descriptor_get_pos(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    off_t o = lseek(obj->fd, 0, SEEK_CUR);
    obj->last_errno = errno;
    if (o >= 0) {
        return o;
    }
    else {
        return SIZE_MAX;
    }
}
FileDescriptorFileTypes file_descriptor_get_type(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    struct stat stats;
    errno = 0;
    int status = fstat(obj->fd, &stats);
    obj->last_errno = errno;
    if (status != 0) { /* Error: */
        return FILE_DESCRIPTOR_TYPE_INVALID;
    }
    if (S_ISREG(stats.st_mode)) {
        return FILE_DESCRIPTOR_TYPE_REGULAR;
    }
    else if (S_ISDIR(stats.st_mode)) {
        return FILE_DESCRIPTOR_TYPE_DIRECTORY;
    }
    else if (S_ISCHR(stats.st_mode) || S_ISBLK(stats.st_mode)) {
        return FILE_DESCRIPTOR_TYPE_DEVICE;
    }
    else if (S_ISFIFO(stats.st_mode) || S_ISSOCK(stats.st_mode)) {
        return FILE_DESCRIPTOR_TYPE_PIPE;
    }
    else if (S_ISLNK(stats.st_mode)) {
        return FILE_DESCRIPTOR_TYPE_LINK;
    }
    else {
        return FILE_DESCRIPTOR_TYPE_UNKNOWN;
    }
}

bool file_descriptor_flush(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    int status = fdatasync(obj->fd);
    obj->last_errno = errno;
    return status == 0;
}
bool file_descriptor_close(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    errno = 0;
    int status = close(obj->fd);
    obj->last_errno = errno;
    if (status == 0) {
        obj->fd = -1;
        return true;
    }
    else {
        return false;
    }
}
void file_descriptor_free(file_descriptor_t* fd) {
    _file_descriptor_t* obj = (_file_descriptor_t*) fd;
    if (obj->fd >= 0) {
        // Try only once and
        if (close(obj->fd) != 0) {
            // if it fails just report it.
            TRACE("file_descriptor_free: failure to close fd!");
        }
    }
    free(obj);
}
