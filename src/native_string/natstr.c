#include <macros.h>

#if IS_POSIX
    #include "natstr_unix.c"
#elif IS_WINDOWS
    #include "natstr_win32.c"
#else
    #error Unsupported operating system for native strings!
#endif
