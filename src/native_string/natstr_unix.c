#include "natstr.h"

#include <macros.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

size_t natstrns_utf8_to_native(void* dst, size_t dst_n, const char* src, size_t src_n) {
    if (src_n == 0) {
        src_n = SIZE_MAX;
    }
    size_t str_len = 0;  // Number of valid characters.
    while (str_len < src_n && src[str_len] != '\0') {
        str_len++;
    }

    if (dst_n == 0) {
        return str_len + 1;
    }

    if (str_len > (dst_n - 1)) {
        str_len = dst_n - 1;
    }
    memcpy(dst, src, str_len);
    ((char*) dst)[str_len] = '\0';
    return str_len + 1;
}
const void* natstrn_utf8_to_native(const char* src, MARK_UNUSED size_t srn_n) {
    return (const void*) src;
}
const void* natstr_utf8_to_native(const char* src) {
    return (const void*) src;
}

size_t natstrns_native_to_utf8(char* dst, size_t dst_n, const void* src, size_t src_n) {
    return natstrns_utf8_to_native(dst, dst_n, src, src_n);
}
const char* natstrn_native_to_utf8(const void* src, MARK_UNUSED size_t srn_n) {
    return (const char*) src;
}
const char* natstr_native_to_utf8(MARK_UNUSED const void* src) {
    return (const char*) src;
}

void natstr_free(MARK_UNUSED const void* str) {
    /* Nothing allocated, nothing to free. */
}
