#ifndef SPEC_NATSTR
#define SPEC_NATSTR
/**
 * @file
 * @brief Converts between utf8 and native string encodings.
 */

#include <stddef.h>

/**
 * \p dst: Output buffer. Can be null.
 * \p dst_n: Capacity of output buffer.
 * \p src: Input string, encoded in UTF-8.
 * \p src_n: Max number of characters to read from input or 0 for no limit.
 *
 * \returns number of bytes written, or that would have been
 * written if \p dst_n is 0. Includes any NULL characters.
 */
size_t natstrns_utf8_to_native(void* dst, size_t dst_n, const char* src, size_t src_n);

/**
 * \p src: Input string, encoded in UTF-8, optionally null-terminated.
 * \p src_n: Max number of characters to read from input.
 *
 * \returns dynamically allocated output.
 * Call \ref natstr_free to free memory.
 */
const void* natstrn_utf8_to_native(const char* src, size_t src_n);

/**
 * \p src: Input null-terminated string, encoded in UTF-8.
 *
 * \returns dynamically allocated output.
 * Call \ref natstr_free to free memory.
 */
const void* natstr_utf8_to_native(const char* src);

/**
 * \p dst: Output UTF-8 string. Can be null if \p dst_n is 0.
 * \p dst_n: Capacity of output buffer.
 * \p src: Input string, encoded in native API format.
 * \p src_n: Max number of bytes to read from input.
 *
 * \returns number of bytes written, or that would have been
 * written if \p dst_n is 0. Includes any NULL characters.
 */
size_t natstrns_native_to_utf8(char* dst, size_t dst_n, const void* src, size_t src_n);

/**
 * \p src: Input string, encoded in native API format, optionally null-terminated.
 * \p src_n: Max number of bytes to read from input.
 *
 * \returns dynamically allocated output.
 * Call \ref natstr_free to free memory.
 */
const char* natstrn_native_to_utf8(const void* src, size_t src_n);

/**
 * \p src: Input null-terminated string, encoded in UTF-8.
 *
 * \returns dynamically allocated output.
 * Call \ref natstr_free to free memory.
 */
const char* natstr_native_to_utf8(const void* src);

void natstr_free(const void* str);

#endif /*SPEC_NATSTR*/
