#ifndef SPEC_NATIVESTRING_HPP
#define SPEC_NATIVESTRING_HPP
/**
 * @file
 * @brief
 */

#include <macros.h>
C_DECLS_START
#include "natstr.h"
C_DECLS_END

#include <memory>
#include <string>

namespace spec::natstr {

namespace detail {
class AutoFree {
   public:
    void operator()(const void* p) {
        natstr_free(p);
    }
};
}  // namespace detail

inline std::unique_ptr<const void, detail::AutoFree> utf8_to_native(const char* str, size_t n) {
    const void* h = natstrn_utf8_to_native(str, n);
    std::unique_ptr<const void, detail::AutoFree> r(h);
    return r;
}

inline std::unique_ptr<const void, detail::AutoFree> utf8_to_native(const char* str) {
    const void* h = natstr_utf8_to_native(str);
    std::unique_ptr<const void, detail::AutoFree> r(h);
    return r;
}

inline std::unique_ptr<const void, detail::AutoFree> utf8_to_native(std::string& str) {
    const void* h = natstrn_utf8_to_native(str.data(), str.length());
    std::unique_ptr<const void, detail::AutoFree> r(h);
    return r;
}

inline std::unique_ptr<const char, detail::AutoFree> native_to_utf8(const void* str, size_t n) {
    const char* h = natstrn_native_to_utf8(str, n);
    std::unique_ptr<const char, detail::AutoFree> r(h);
    return r;
}

inline std::unique_ptr<const char, detail::AutoFree> native_to_utf8(const void* str) {
    const char* h = natstr_native_to_utf8(str);
    std::unique_ptr<const char, detail::AutoFree> r(h);
    return r;
}

inline std::unique_ptr<const char, detail::AutoFree> native_to_utf8(std::string& str) {
    const char* h = natstrn_native_to_utf8(str.data(), str.length());
    std::unique_ptr<const char, detail::AutoFree> r(h);
    return r;
}

}  // namespace spec::natstr

#endif /*SPEC_NATIVESTRING_HPP*/
