#include "natstr.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <trace.h>
#include <windows.h>

static inline void _natstr_error_check(int count) {
    if (count == 0) {
        uint32_t error = GetLastError();
        switch (error) {
            case ERROR_INSUFFICIENT_BUFFER: FAIL("ERROR_INSUFFICIENT_BUFFER");
            case ERROR_INVALID_FLAGS: FAIL("ERROR_INVALID_FLAGS");
            case ERROR_INVALID_PARAMETER: FAIL("ERROR_INVALID_PARAMETER");
            case ERROR_NO_UNICODE_TRANSLATION: FAIL("ERROR_NO_UNICODE_TRANSLATION");
            default: FAIL("UNKNOWN_ERROR");
        }
    }
}

size_t natstrns_utf8_to_native(void* dst, size_t dst_n, const char* src, size_t src_n) {
    wchar_t* wdst = (wchar_t*) dst;
    int c = MultiByteToWideChar(CP_UTF8, 0, src, src_n == 0 ? -1 : src_n, wdst,
                                dst_n / sizeof(wchar_t));
    _natstr_error_check(c);
    if (dst_n > 0) {
        // Post-process: Ensure null termination in output.
        if (wdst[c - 1] != L'\0') {
            if (((size_t) c) < dst_n / sizeof(wchar_t)) {
                c++;
            }
            wdst[c - 1] = L'\0';
        }
    }
    else if (src_n != 0) {
        // Add bytes if input is not null terminated.
        size_t src_len = 0;  // Do not risk it by skipping ahead.
        while (src_len < src_n && src[src_len] != '\0') {
            src_len++;
        }
        if (src[src_len] != '\0') {
            // If the last character in not the NULL character:
            c++;
        }
    }
    return c * 2;
}
const void* natstrn_utf8_to_native(const char* src, size_t src_n) {
    size_t n = natstrns_utf8_to_native(NULL, 0, src, src_n);
    void* a = malloc(n);
    if (natstrns_utf8_to_native(a, n, src, src_n) != n) {
        FAIL("natstrn_utf8_to_native: calculated and actual size do not match!");
    }
    return a;
}
const void* natstr_utf8_to_native(const char* src) {
    return natstrn_utf8_to_native(src, 0);
}

size_t natstrns_native_to_utf8(char* dst, size_t dst_n, const void* src, size_t src_n) {
    wchar_t* wsrc = (wchar_t*) src;
    int c = WideCharToMultiByte(CP_UTF8, 0, wsrc, src_n == 0 ? -1 : src_n / 2, dst, dst_n, NULL,
                                NULL);
    _natstr_error_check(c);
    if (dst_n > 0) {
        // Post-process: Ensure null termination in output.
        if (dst[c - 1] != '\0') {
            if (((size_t) c) < dst_n) {
                c++;
            }
            dst[c - 1] = '\0';
        }
    }
    else if (src_n != 0) {
        // Add bytes if input is not null terminated.
        size_t wsrc_n = src_n / 2;
        size_t wsrc_len = 0;  // Do not risk it by skipping ahead.
        while (wsrc_len < wsrc_n && wsrc[wsrc_len] != L'\0') {
            wsrc_len++;
        }
        if (wsrc[wsrc_len] != L'\0') {
            // If the last character in not the NULL character:
            c++;
        }
    }
    return c;
}
const char* natstrn_native_to_utf8(const void* src, size_t src_n) {
    size_t n = natstrns_native_to_utf8(NULL, 0, src, src_n);
    char* a = (char*) malloc(n);
    if (natstrns_native_to_utf8(a, n, src, src_n) != n) {
        FAIL("natstrn_native_to_utf8: calculated and actual size do not match!");
    }
    return a;
}
const char* natstr_native_to_utf8(const void* src) {
    return natstrn_native_to_utf8(src, 0);
}

void natstr_free(const void* str) {
    free((void*) str);
}
