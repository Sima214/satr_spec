#ifndef SPEC_ALGORITHMS
#define SPEC_ALGORITHMS
/**
 * @file
 * @brief Collections of functions similar to those found in <algorithm>.
 */

#include <stddef.h>
#include <stdint.h>

typedef int (*comparison_cb_t)(const void* a, const void* b);

/**
 * Perfoms binary search and returns the index
 * of the \p value or an insertion point
 * which maintains sorted order.
 *
 * \p container pointer to start of container to search.
 * \p container_size size of container in elements.
 * \p element_length size of each element in bytes.
 * \p found Gets set based on if an exact match is found (optional).
 * \returns the calculated insertion position.
 */
const void* bisect(const void* container, size_t container_size, const void* value,
                   uint8_t* found, size_t element_length, comparison_cb_t cmp_cb);

/**
 * Inserts \p value in the specified \p insert_pos,
 * displacing all subsequent elements.
 *
 * \p container pointer to start of container to search.
 * \p container_size size of container in elements. Does not include allocated space for the new element.
 * \p element_length size of each element in bytes.
 * \returns a non-zero code on error.
 */
int insert(void* container, size_t container_size, const void* value,
              size_t element_length, void* insert_pos);

#endif /*SPEC_ALGORITHMS*/
