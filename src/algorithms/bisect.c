#include "algorithms.h"

#include <stdint.h>

const void* bisect(const void* a, size_t n, const void* v, uint8_t* found, size_t l,
                   comparison_cb_t cb) {
    if (n == 0) {
        if (found != NULL) {
            *found = 0;
        }
        return a;
    }
    size_t start = 0;
    size_t end = n;
    while (start < end) {
        size_t middle = start + (end - start) / 2;
        const void* middle_address = ((uint8_t*) a) + middle * l;
        int cmp = cb(middle_address, v);
        if (cmp == 0) {
            // Hit
            if (found != NULL) {
                *found = 1;
            }
            return middle_address;
        }
        else if (cmp < 0) {
            // Right
            start = middle + 1;
        }
        else {
            // Left
            end = middle;
        }
    }
    if (found != NULL) {
        *found = 0;
    }
    return ((uint8_t*) a) + (start * l);
}
