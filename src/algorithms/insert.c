#include "algorithms.h"

#include <errno.h>
#include <string.h>

int insert(void* a, size_t n, const void* v, size_t l, void* p) {
    size_t pi = (((uintptr_t) p) - ((uintptr_t) a)) / l;
    if (pi > n) {
        return EINVAL;
    }
    memmove(((uint8_t*) p) + l, p, (n - pi) * l);
    memcpy(p, v, l);
    return 0;
}
