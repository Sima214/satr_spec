#ifndef SPEC_TUNABLES
#define SPEC_TUNABLES
/**
 * @file
 * @brief Low level configuration system based on environment variables,
 * to allow user configurability on low level tunable values.
 * Could also be viewed as a form of hidden settings.
 */

#include <stddef.h>
#include <stdint.h>

typedef enum {
    /** No error. */
    TUNABLES_OK,
    /* Environment variable not set. */
    TUNABLES_NOTSET,
    /* Unable to convert. */
    TUNABLES_NOCONV,
    /* Numerical value out of range. */
    TUNABLES_OUTRANGE,
} TunablesErrors;

TunablesErrors tunables_get_str(const char* key, const char** dest, const char* def);
TunablesErrors tunables_get_bool(const char* key, int* dest, int def);
TunablesErrors tunables_get_int8(const char* key, int8_t* dest, int8_t def);
TunablesErrors tunables_get_int16(const char* key, int16_t* dest, int16_t def);
TunablesErrors tunables_get_int32(const char* key, int32_t* dest, int32_t def);
TunablesErrors tunables_get_int64(const char* key, int64_t* dest, int64_t def);
TunablesErrors tunables_get_uint8(const char* key, uint8_t* dest, uint8_t def);
TunablesErrors tunables_get_uint16(const char* key, uint16_t* dest, uint16_t def);
TunablesErrors tunables_get_uint32(const char* key, uint32_t* dest, uint32_t def);
TunablesErrors tunables_get_uint64(const char* key, uint64_t* dest, uint64_t def);
TunablesErrors tunables_get_float(const char* key, float* dest, float def);
TunablesErrors tunables_get_double(const char* key, double* dest, double def);

#endif /*SPEC_TUNABLES*/
