#ifndef SPEC_ENVIRONMENT
#define SPEC_ENVIRONMENT
/**
 * @file
 * @brief getenv with consistent, thread-safe behavior.
 */
#include <stddef.h>

/**
 * Cached environment variable getter.
 * 
 * Returns a read-only pointer to the cached value
 * or null if no matching key is found.
 */
const char* getenv_cached(const char* key);

#endif /*SPEC_ENVIRONMENT*/
