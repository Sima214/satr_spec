#include <natstr.h>
#include <windows.h>

static char* _environment_character_store = NULL;

static void _getenv_cached_deinit() {
    free(_environment_character_store);
    _environment_character_store = NULL;
    free(_environment_cache);
    _environment_cache = NULL;
}

atexit_cb_t _getenv_cached_init() {
    size_t total_char_count = 0;
    size_t total_elem_count = 0;

    LPWCH env = GetEnvironmentStrings();
    LPWCH env_iter = env;
    while (*env_iter) {
        size_t l = natstrns_native_to_utf8(NULL, 0, env_iter, 0);
        env_iter += lstrlen(env_iter) + 1;

        total_char_count += l;
        total_elem_count += 1;
    }

    _environment_character_store = malloc(total_char_count);
    _environment_cache = malloc(total_elem_count * sizeof(_getenv_cache_element_t));

    char* character_store_next = _environment_character_store;
    env_iter = env;
    while (*env_iter) {
        size_t l = natstrns_native_to_utf8(NULL, 0, env_iter, 0);
        if (natstrns_native_to_utf8(character_store_next, l, env_iter, 0) != l) {
            FAIL("_getenv_cached_init::natstrn_native_to_utf8: calculated and actual size do "
                 "not match!");
        }
        env_iter += lstrlen(env_iter) + 1;

        char* key = character_store_next;
        char* sep = strchr(key, '=');
        *sep = '\0';
        char* value = sep + 1;
        character_store_next += l;

        _getenv_cache_element_t e = {key, value};
        _getenv_cache_element_t* pos = (_getenv_cache_element_t*) bisect(
             _environment_cache, _environment_cache_len, &e, NULL,
             sizeof(_getenv_cache_element_t), (comparison_cb_t) _getenv_cache_cmp);
        if (insert(_environment_cache, _environment_cache_len, &e,
                   sizeof(_getenv_cache_element_t), pos)) {
            FAIL("_getenv_cached_init: Failed to insert environment cache element.");
        }
        _environment_cache_len++;
    }

    FreeEnvironmentStrings(env);

    return _getenv_cached_deinit;
}
