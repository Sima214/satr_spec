#include "environment.h"

#include <algorithms.h>
#include <macros.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <trace.h>

typedef void (*atexit_cb_t)();

typedef struct {
    char* key;
    char* value;
} _getenv_cache_element_t;

static _getenv_cache_element_t* _environment_cache = NULL;
static size_t _environment_cache_len = 0;

static int _getenv_cache_cmp(_getenv_cache_element_t* a, _getenv_cache_element_t* b) {
    return strcmp(a->key, b->key);
}

static atexit_cb_t _getenv_cached_init();

const char* getenv_cached(const char* key) {
    if (_environment_cache == NULL) {
        one_time_static_init(_environment_cache, {
            // Initialize cache.
            atexit(_getenv_cached_init());
            // Optional debug messages.
            for (size_t c = 0; c < _environment_cache_len; c++) {
                TRACF("_getenv_cached_init::new_entry(key: `%s`, value: `%s`)",
                      _environment_cache[c].key, _environment_cache[c].value);
            }
        });
    }
    uint8_t elem_found = 0;
    _getenv_cache_element_t search_elem = {(char*) key, NULL};
    const _getenv_cache_element_t* elem =
         bisect(_environment_cache, _environment_cache_len, &search_elem, &elem_found,
                sizeof(_getenv_cache_element_t), (comparison_cb_t) _getenv_cache_cmp);
    if (elem_found) {
        return elem->value;
    }
    else {
        return NULL;
    }
}

#if IS_POSIX
    #if IS_LINUX
        #include "environment_linux.c"
    #elif IS_MACOS
        #include "environment_macos.c"
    #else
        #error Unsupported unix operating system for environment functions!
    #endif
#elif IS_WINDOWS
    #include "environment_win32.c"
#else
    #error Unsupported operating system for environment functions!
#endif
