#ifndef SPEC_TUNABLES_HPP
#define SPEC_TUNABLES_HPP
/**
 * @file
 * @brief Low level configuration system based on environment variables,
 * to allow user configurability on low level tunable values.
 * Could also be viewed as a form of hidden settings.
 */

#include <macros.h>
C_DECLS_START
#include <tunables.h>
C_DECLS_END

#include <limits>
#include <string>
#include <utility>

namespace spec::tunables {

enum class Errors {
    /** No error. */
    Ok,
    /* Environment variable not set. */
    NotSet,
    /* Unable to convert. */
    NoConv,
    /* Numerical value out of range. */
    OutRange
};

inline Errors _errors_convertion(TunablesErrors in) {
    switch (in) {
        case TUNABLES_OK: return Errors::Ok;
        case TUNABLES_NOTSET: return Errors::NotSet;
        case TUNABLES_NOCONV: return Errors::NoConv;
        case TUNABLES_OUTRANGE: return Errors::OutRange;
        default: return Errors::Ok;
    }
}

inline std::pair<Errors, const char*> get_str(const char* key, const char* def = "") {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_str(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    return {err, ret};
}
inline std::pair<Errors, std::string> get_str(const std::string& key,
                                              const std::string& def = "") {
    return get_str(key.c_str(), def.c_str());
}

inline std::pair<Errors, bool> get_bool(const char* key, bool def = false) {
    int ret;
    TunablesErrors cerr = tunables_get_bool(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    return {err, !!ret};
}
inline std::pair<Errors, bool> get_bool(const std::string& key,
                                              bool def = false) {
    return get_bool(key.c_str(), def);
}

inline std::pair<Errors, int8_t> get_int8(const char* key, int8_t def = 0,
                                          int8_t min = std::numeric_limits<int8_t>::min(),
                                          int8_t max = std::numeric_limits<int8_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_int8(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, int8_t> get_int8(const std::string& key, int8_t def = 0,
                                          int8_t min = std::numeric_limits<int8_t>::min(),
                                          int8_t max = std::numeric_limits<int8_t>::max()) {
    return get_int8(key.c_str(), def, min, max);
}

inline std::pair<Errors, int16_t> get_int16(const char* key, int16_t def = 0,
                                            int16_t min = std::numeric_limits<int16_t>::min(),
                                            int16_t max = std::numeric_limits<int16_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_int16(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, int16_t> get_int16(const std::string& key, int16_t def = 0,
                                            int16_t min = std::numeric_limits<int16_t>::min(),
                                            int16_t max = std::numeric_limits<int16_t>::max()) {
    return get_int16(key.c_str(), def, min, max);
}

inline std::pair<Errors, int32_t> get_int32(const char* key, int32_t def = 0,
                                            int32_t min = std::numeric_limits<int32_t>::min(),
                                            int32_t max = std::numeric_limits<int32_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_int32(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, int32_t> get_int32(const std::string& key, int32_t def = 0,
                                            int32_t min = std::numeric_limits<int32_t>::min(),
                                            int32_t max = std::numeric_limits<int32_t>::max()) {
    return get_int32(key.c_str(), def, min, max);
}

inline std::pair<Errors, int64_t> get_int64(const char* key, int64_t def = 0,
                                            int64_t min = std::numeric_limits<int64_t>::min(),
                                            int64_t max = std::numeric_limits<int64_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_int64(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, int64_t> get_int64(const std::string& key, int64_t def = 0,
                                            int64_t min = std::numeric_limits<int64_t>::min(),
                                            int64_t max = std::numeric_limits<int64_t>::max()) {
    return get_int64(key.c_str(), def, min, max);
}

inline std::pair<Errors, uint8_t> get_uint8(const char* key, uint8_t def = 0,
                                            uint8_t min = std::numeric_limits<uint8_t>::min(),
                                            uint8_t max = std::numeric_limits<uint8_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_uint8(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, uint8_t> get_uint8(const std::string& key, uint8_t def = 0,
                                            uint8_t min = std::numeric_limits<uint8_t>::min(),
                                            uint8_t max = std::numeric_limits<uint8_t>::max()) {
    return get_uint8(key.c_str(), def, min, max);
}

inline std::pair<Errors, uint16_t> get_uint16(
     const char* key, uint16_t def = 0, uint16_t min = std::numeric_limits<uint16_t>::min(),
     uint16_t max = std::numeric_limits<uint16_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_uint16(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, uint16_t> get_uint16(
     const std::string& key, uint16_t def = 0,
     uint16_t min = std::numeric_limits<uint16_t>::min(),
     uint16_t max = std::numeric_limits<uint16_t>::max()) {
    return get_uint16(key.c_str(), def, min, max);
}

inline std::pair<Errors, uint32_t> get_uint32(
     const char* key, uint32_t def = 0, uint32_t min = std::numeric_limits<uint32_t>::min(),
     uint32_t max = std::numeric_limits<uint32_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_uint32(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, uint32_t> get_uint32(
     const std::string& key, uint32_t def = 0,
     uint32_t min = std::numeric_limits<uint32_t>::min(),
     uint32_t max = std::numeric_limits<uint32_t>::max()) {
    return get_uint32(key.c_str(), def, min, max);
}

inline std::pair<Errors, uint64_t> get_uint64(
     const char* key, uint64_t def = 0, uint64_t min = std::numeric_limits<uint64_t>::min(),
     uint64_t max = std::numeric_limits<uint64_t>::max()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_uint64(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, uint64_t> get_uint64(
     const std::string& key, uint64_t def = 0,
     uint64_t min = std::numeric_limits<uint64_t>::min(),
     uint64_t max = std::numeric_limits<uint64_t>::max()) {
    return get_uint64(key.c_str(), def, min, max);
}

inline std::pair<Errors, float> get_float(const char* key, float def = 0,
                                          float min = -std::numeric_limits<float>::infinity(),
                                          float max = std::numeric_limits<float>::infinity()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_float(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, float> get_float(const std::string& key, float def = 0,
                                          float min = -std::numeric_limits<float>::infinity(),
                                          float max = std::numeric_limits<float>::infinity()) {
    return get_float(key.c_str(), def, min, max);
}

inline std::pair<Errors, double> get_double(
     const char* key, double def = 0, double min = -std::numeric_limits<double>::infinity(),
     double max = std::numeric_limits<double>::infinity()) {
    decltype(def) ret;
    TunablesErrors cerr = tunables_get_double(key, &ret, def);
    Errors err = _errors_convertion(cerr);
    if (err == Errors::Ok) {
        if (ret < min) {
            err = Errors::OutRange;
            ret = min;
        }
        else if (ret > max) {
            err = Errors::OutRange;
            ret = max;
        }
    }
    return {err, ret};
}
inline std::pair<Errors, double> get_double(
     const std::string& key, double def = 0,
     double min = -std::numeric_limits<double>::infinity(),
     double max = std::numeric_limits<double>::infinity()) {
    return get_double(key.c_str(), def, min, max);
}

}  // namespace spec::tunables

#endif /*SPEC_TUNABLES_HPP*/