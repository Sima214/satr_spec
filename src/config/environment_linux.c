#include <unistd.h>

static char* _environment_character_store = NULL;

static void _getenv_cached_deinit() {
    free(_environment_character_store);
    _environment_character_store = NULL;
    free(_environment_cache);
    _environment_cache = NULL;
}

atexit_cb_t _getenv_cached_init() {
    size_t total_char_count = 0;
    size_t total_elem_count = 0;

    char** environ_iter = environ;
    do {
        total_char_count += strlen(*environ_iter) + 1;
        total_elem_count += 1;
    } while (*(++environ_iter) != NULL);

    _environment_character_store = malloc(total_char_count);
    _environment_cache = malloc(total_elem_count * sizeof(_getenv_cache_element_t));

    char* character_store_next = _environment_character_store;
    for (size_t c = 0; c < total_elem_count; c++) {
        size_t l = strlen(environ[c]) + 1;
        memcpy(character_store_next, environ[c], l);
        char* key = character_store_next;
        char* sep = strchr(key, '=');
        *sep = '\0';
        char* value = sep + 1;
        character_store_next += l;

        _getenv_cache_element_t e = {key, value};
        _getenv_cache_element_t* pos = (_getenv_cache_element_t*) bisect(
             _environment_cache, _environment_cache_len, &e, NULL,
             sizeof(_getenv_cache_element_t), (comparison_cb_t) _getenv_cache_cmp);
        if (insert(_environment_cache, _environment_cache_len, &e,
                   sizeof(_getenv_cache_element_t), pos)) {
            FAIL("_getenv_cached_init: Failed to insert environment cache element.");
        }
        _environment_cache_len++;
    }

    return _getenv_cached_deinit;
}
