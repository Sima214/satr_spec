#include "tunables.h"

#include <ctype.h>
#include <environment.h>
#include <errno.h>
#include <macros.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

TunablesErrors tunables_get_str(const char* key, const char** dest, const char* def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    *dest = value;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_bool(const char* key, int* dest, int def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    // Possible values.
    const char* NEGATIVE[] = {"0", "n", "no", "false"};
    size_t NEGATIVE_COUNT = sizeof(NEGATIVE) / sizeof(char*);
    const char* POSITIVE[] = {"1", "y", "yes", "true"};
    size_t POSITIVE_COUNT = sizeof(POSITIVE) / sizeof(char*);
    const size_t MAX_CHAR_COUNT = sizeof("false");
    // Convert all to lowercase.
    size_t n = math_min(strlen(value), MAX_CHAR_COUNT);
    char value_lower[n + 1];
    value_lower[n] = '\0';
    for (size_t i = 0; i < n; i++) {
        value_lower[i] = tolower(value[i]);
    }
    // Check and match against possible values.
    for (size_t i = 0; i < NEGATIVE_COUNT; i++) {
        if (strequal(NEGATIVE[i], value_lower)) {
            *dest = 0;
            return TUNABLES_OK;
        }
    }
    for (size_t i = 0; i < POSITIVE_COUNT; i++) {
        if (strequal(POSITIVE[i], value_lower)) {
            *dest = 1;
            return TUNABLES_OK;
        }
    }
    // No match.
    *dest = def;
    return TUNABLES_NOCONV;
}

TunablesErrors tunables_get_int8(const char* key, int8_t* dest, int8_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    long cv = strtol(value, &conv_end, 0);
    if (errno == ERANGE || cv < INT8_MIN || cv > INT8_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_int16(const char* key, int16_t* dest, int16_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    long cv = strtol(value, &conv_end, 0);
    if (errno == ERANGE || cv < INT16_MIN || cv > INT16_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_int32(const char* key, int32_t* dest, int32_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    long long cv = strtoll(value, &conv_end, 0);
    if (errno == ERANGE || cv < INT32_MIN || cv > INT32_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_int64(const char* key, int64_t* dest, int64_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    long long cv = strtoll(value, &conv_end, 0);
    if (errno == ERANGE || cv < INT64_MIN || cv > INT64_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_uint8(const char* key, uint8_t* dest, uint8_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    unsigned long cv = strtoul(value, &conv_end, 0);
    if (errno == ERANGE || cv > UINT8_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_uint16(const char* key, uint16_t* dest, uint16_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    unsigned long cv = strtoul(value, &conv_end, 0);
    if (errno == ERANGE || cv > UINT16_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_uint32(const char* key, uint32_t* dest, uint32_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    unsigned long long cv = strtoull(value, &conv_end, 0);
    if (errno == ERANGE || cv > UINT32_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_uint64(const char* key, uint64_t* dest, uint64_t def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    unsigned long long cv = strtoull(value, &conv_end, 0);
    if (errno == ERANGE || cv > UINT64_MAX) {
        // Cannot represent value, return default.
        *dest = def;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_float(const char* key, float* dest, float def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    float cv = strtof(value, &conv_end);
    if (errno == ERANGE) {
        // Range error, but float can represent these values.
        *dest = cv;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}

TunablesErrors tunables_get_double(const char* key, double* dest, double def) {
    const char* value = getenv_cached(key);
    if (value == NULL) {
        *dest = def;
        return TUNABLES_NOTSET;
    }
    char* conv_end = NULL;
    // Pre-skip whitespace.
    while (isspace(*value)) {
        value++;
    }
    // Convertion.
    errno = 0;
    double cv = strtod(value, &conv_end);
    if (errno == ERANGE) {
        // Range error, but float can represent these values.
        *dest = cv;
        return TUNABLES_OUTRANGE;
    }
    if (conv_end == value) {
        // Did not convert anything.
        *dest = def;
        return TUNABLES_NOCONV;
    }
    *dest = cv;
    return TUNABLES_OK;
}
