#ifndef SPEC_STOPWATCH_HPP
#define SPEC_STOPWATCH_HPP
/**
 * @file
 * @brief High accuracy relative time measurements.
 */

#include <cstdint>

#include <macros.h>

C_DECLS_START
#include <stopwatch.h>
C_DECLS_END

namespace spec {

class StopWatch {
   private:
    ::StopWatch _state;

   public:
    StopWatch() {
        reset();
    }
    ~StopWatch() = default;

    void start() {
        clock_stopwatch_start(&_state);
    }

    void stop() {
        clock_stopwatch_stop(&_state);
    }

    void reset() {
        clock_stopwatch_reset(&_state);
    }

    int64_t get_count() {
        return _state.count_query;
    }
    double get_total_time() {
        return _state.delta_sum;
    }
    float get_last_time() {
        return _state.delta;
    }
    float get_avg_time() {
        return _state.avg;
    }
    float get_min_time() {
        return _state.min;
    }
    float get_max_time() {
        return _state.max;
    }
};

}  // namespace spec

#endif /*SPEC_STOPWATCH_HPP*/