#include "stopwatch.h"

#include <float.h>
#include <pthread.h>
#include <macros.h>

static void clock_stopwatch_update_statistics(StopWatch* sw, double new_delta) {
    sw->count_query++;
    sw->delta_sum += new_delta;
    sw->avg = sw->delta_sum / sw->count_query;
    sw->delta = new_delta;
    sw->max = math_max(sw->max, new_delta);
    sw->min = math_min(sw->min, new_delta);
}

void clock_stopwatch_reset(StopWatch* sw) {
    sw->count_query = 0;
    sw->delta_sum = 0;
    sw->max = FLT_MIN;
    sw->min = FLT_MAX;
}

#if IS_LINUX
    #include "stopwatch_linux.c"
#elif IS_MACOS
    #include "stopwatch_macos.c"
#elif IS_WINDOWS
    #include "stopwatch_win32.c"
#else
    #error Unsupported operating system for stopwatch functions!
#endif
