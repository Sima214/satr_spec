#ifndef SPEC_STOPWATCH
#define SPEC_STOPWATCH
/**
 * @file
 * @brief High accuracy relative time measurements.
 */

#include <stdint.h>

#if defined(__linux__)
  #define SPEC_STOPWATCH_INTERNAL_STORAGE 2
#elif defined(__APPLE__)
  #define SPEC_STOPWATCH_INTERNAL_STORAGE 2
#elif defined(_WIN32)
  #define SPEC_STOPWATCH_INTERNAL_STORAGE 1
#else
  #error "Unsupported platform!"
#endif

/**
 * Cross platform high accuracy clock.
 * All the captured information is in milliseconds unless otherwise stated.
 */
typedef struct {
  /** Internal counters. */
  int64_t __internal[SPEC_STOPWATCH_INTERNAL_STORAGE];
  /** How many samples have been taken since last reset. */
  int64_t count_query;
  /** Total time that has passed since last reset. */
  double delta_sum;
  /** Last measurement. */
  float delta;
  /** Mean delta since last reset. */
  float avg;
  /** Smallest delta since last reset. */
  float min;
  /** Biggest delta since last reset. */
  float max;
} StopWatch;

/**
 * Start measuring.
 */
void clock_stopwatch_start(StopWatch*);

/**
 * Stops measurement and calculates new delta and updates statistics.
 */
void clock_stopwatch_stop(StopWatch*);

/**
 * Resets clock counters and statistics.
 * This function must be called to initialize a StopWatch structure.
 */
void clock_stopwatch_reset(StopWatch*);

#endif /*SPEC_STOPWATCH*/