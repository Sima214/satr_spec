#include <mach/clock.h>
#include <mach/mach.h>
#include <macros.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

static clock_serv_t clock_stopwatch_kernel;

void clock_stopwatch_start(StopWatch* sw) {
    one_time_static_init(clock_stopwatch_kernel, {
        host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &clock_stopwatch_kernel);
    });
    mach_timespec_t tp;
    clock_get_time(clock_stopwatch_kernel, &tp);
    sw->internal[0] = tp.tv_nsec;
    sw->internal[1] = tp.tv_sec;
}

void clock_stopwatch_stop(StopWatch* sw) {
    mach_timespec_t tp;
    clock_get_time(clock_stopwatch_kernel, &tp);
    int64_t nano_delta =
         (tp.tv_nsec - sw->internal[0]) + SEC2NANO * (tp.tv_sec - sw->internal[1]);
    double delta = ((double) nano_delta) / MSEC2NANO;
    clock_stopwatch_update_statistics(sw, delta);
}
