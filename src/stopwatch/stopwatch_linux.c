#include <macros.h>
#include <time.h>
#include <unistd.h>

void clock_stopwatch_start(StopWatch* sw) {
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
    sw->__internal[0] = tp.tv_nsec;
    sw->__internal[1] = tp.tv_sec;
}

void clock_stopwatch_stop(StopWatch* sw) {
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
    int64_t nano_delta =
         (tp.tv_nsec - sw->__internal[0]) + SEC2NANO * (tp.tv_sec - sw->__internal[1]);
    double delta = ((double) nano_delta) / MSEC2NANO;
    clock_stopwatch_update_statistics(sw, delta);
}