#include <macros.h>
#include <ntdll.h>
#include <stdint.h>
#include <windows.h>

static int64_t clock_stopwatch_frequency;

void clock_stopwatch_start(StopWatch* sw) {
    one_time_static_init(clock_stopwatch_frequency, {
        // Get clock frequency.
        LARGE_INTEGER x;
        QueryPerformanceFrequency(&x);
        clock_stopwatch_frequency = x.QuadPart / 1000;
    });
    LARGE_INTEGER x;
    QueryPerformanceCounter(&x);
    sw->__internal[0] = x.QuadPart;
}

void clock_stopwatch_stop(StopWatch* sw) {
    LARGE_INTEGER x;
    QueryPerformanceCounter(&x);
    int64_t nano_delta = x.QuadPart;
    nano_delta = nano_delta - sw->__internal[0];
    double delta = ((double) nano_delta) / clock_stopwatch_frequency;
    clock_stopwatch_update_statistics(sw, delta);
}
