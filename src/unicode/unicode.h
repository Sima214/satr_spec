#ifndef SPEC_UNICODE
#define SPEC_UNICODE
/**
 * @file
 * @brief Convertion between Unicode codepoints.
 */

#include <stdint.h>

/**
 * Structure which represents a single UTF-8 character.
 */
typedef struct {
    /** Non-zero when this represents a valid UTF-8 character. */
    uint8_t valid;
    /** How many bytes is this character? */
    uint8_t size;
    /** Non-NULL terminated array of bytes. */
    char value[6];
} utf8_codepoint;

typedef uint32_t utf32_codepoint;

/**
 * Converts a single UTF-32 codepoint to a multibyte utf-8 codepoint.
 */
utf8_codepoint convert_utf32_utf8(utf32_codepoint);

#endif /*SPEC_UNICODE*/
