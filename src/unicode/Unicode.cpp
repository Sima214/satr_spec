#include "Unicode.hpp"

#include <trace/Trace.hpp>

#include <climits>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <cuchar>

extern "C" utf8_codepoint convert_utf32_utf8(utf32_codepoint u32) {
    // Are there any cases where we need the previous state when converting from UTF-32?
    std::mbstate_t mbstate{};

    char char8[MB_LEN_MAX]{};
    if (MB_CUR_MAX > 8) {
        spec::fatal("convert_utf32_utf8: MB_CUR_MAX=", MB_CUR_MAX,
                    ", but compile-time limit is 8!");
    }

    size_t c = std::c32rtomb(char8, u32, &mbstate);

    utf8_codepoint u8 = {0, 0, {}};
    if (c <= 6) {
        // Valid unicode!
        u8.valid = 1;
        u8.size = c;
        std::memcpy(u8.value, char8, c);
    }
    else {
        spec::trace("convert_utf32_utf8 error: c32rtomb(", u32, ") -> ", c);
    }

    return u8;
}

std::string spec::utf8_codepoint::to_string() const {
    if (valid) {
        return std::string(value, size);
    }
    return std::string(u8"�");
}
