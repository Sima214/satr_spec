#ifndef SPEC_UNICODE_HPP
#define SPEC_UNICODE_HPP
/**
 * @file
 * @brief Convert UTF32 codepoints to utf8 multibyte strings.
 */

#include <cstring>
#include <string>

#include <macros.h>
C_DECLS_START
#include <unicode.h>
C_DECLS_END

namespace spec {

/**
 * Import typedefs.
 */
using utf32_codepoint = ::utf32_codepoint;

struct utf8_codepoint : protected ::utf8_codepoint {
    utf8_codepoint() {
        valid = 0;
        size = 0;
    }

    /**
     * Construct from a parent class.
     */
    utf8_codepoint(::utf8_codepoint cs) {
        valid = cs.valid;
        size = cs.size;
        std::memcpy(value, cs.value, sizeof(value));
    }

    bool is_valid() const {
        return !!valid;
    }

    size_t get_size() const {
        return size;
    }

    std::string to_string() const;
    #if __cplusplus >= 202002L
        /** Support C++20's utf8 explicit string type. */
        std::u8string to_u8string() const {
            if (valid) {
                return std::u8string(reinterpret_cast<const char8_t*>(value), size);
            }
            return std::u8string(u8"�");
        }
    #endif

    static utf8_codepoint get(utf32_codepoint c) {
        return ::convert_utf32_utf8(c);
    }
};

inline utf8_codepoint convert_utf8(utf32_codepoint c) {
    return utf8_codepoint::get(c);
}

}  // namespace spec

#endif /*SPEC_UNICODE_HPP*/
