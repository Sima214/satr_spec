#!/usr/bin/env python

import argparse

generic_generator_pattern = """%s {
    static %s __resolved;
    if (!__resolved) {
        __resolved = %s();
    }
    return __resolved();
}
"""


def generic_generator(declaration: str, typedef: str, resolver: str):
    return generic_generator_pattern % (declaration, typedef, resolver)


def ifunc_generator(declaration: str, typedef: str, resolver: str):
    pat = "%s __attribute__((ifunc(\"%s\")));\n"
    return pat % (declaration, resolver)


target2generator = {
    'linux_x86': ifunc_generator,
    'linux_x86_64': ifunc_generator,
    'darwin_x86': generic_generator,
    'darwin_x86_64': generic_generator,
    'windows_x86': generic_generator,
    'windows_x86_64': generic_generator,
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Generate source code for linking with libraries at runtime.')
    parser.add_argument('output', metavar='OUT', type=str,
                        nargs=1, help='Output file.')
    parser.add_argument('--target', '-t', dest='target', type=str, required=True,
                        help='Target for which to generate source code. Typically in the form of <os>_<arch>.')
    parser.add_argument('--prototype', '-p', dest='prototype', type=str,
                        required=True, help='File to prototype of exported and resolver function.')
    args = parser.parse_args()

    exported_declaration = None
    exported_typedef = None
    resolver_declaration = None

    with open(args.prototype, 'r') as f:
        prototype = f.readlines()
        exported_declaration = prototype[0].strip()
        exported_typedef = prototype[1].strip()
        resolver_declaration = prototype[2].strip()

    with open(args.output[0], 'w') as f:
        generator = target2generator[args.target]
        f.write(generator(exported_declaration,
                          exported_typedef, resolver_declaration))
