typedef int (*get_code_t)();

static int get_code_imp() {
    return 0;
}

static get_code_t get_code_resolver() {
    return &get_code_imp;
}

#include <test_muldis.c>

int main() {
    return get_code();
}