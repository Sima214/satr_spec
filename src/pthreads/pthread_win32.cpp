#include <windows.h>

int pthread_getschedparam(pthread_t thrd, int* policy, struct sched_param* param) {
    std::thread* t = _pthread_get(thrd);
    if (t == nullptr) {
        return ESRCH;
    }
    HANDLE h = t->native_handle();
    int native_priority = GetThreadPriority(h);
    if (native_priority == THREAD_PRIORITY_ERROR_RETURN) {
        return EPERM;
    }
    param->sched_priority = native_priority;
    *policy = sched_getscheduler(0);
    return 0;
}

int pthread_setschedparam(pthread_t thrd, int policy, const struct sched_param* param) {
    std::thread* t = _pthread_get(thrd);
    if (t == nullptr) {
        return ESRCH;
    }
    HANDLE h = t->native_handle();
    int prio_min = sched_get_priority_min(SCHED_OTHER);
    int prio_max = sched_get_priority_max(SCHED_OTHER);
    int prio = param->sched_priority;
    if (prio < prio_min || prio > prio_max) {
        return EINVAL;
    }
    // Ignore policy, because it can only be set at process level.
    if (!SetThreadPriority(h, prio)) {
        // Assumption.
        return EPERM;
    }
    return 0;
}
