#include "pthread.h"

#include <cerrno>
#include <cstring>
#include <exception>
#include <iostream>
#include <limits>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <utility>
#include <vector>

struct _pthread_tpp {
    std::thread::id _id;
};

static int _pthread_concurrency = 0;

static std::recursive_mutex _pthread_once_mutex;

static std::recursive_mutex _pthread_map_mutex;
static std::unordered_map<std::thread::id, std::thread*> _pthread_map;
static std::recursive_mutex _pthread_return_value_map_mutex;
static std::unordered_map<std::thread::id, void*> _pthread_return_value_map;
static std::recursive_mutex _pthread_name_map_mutex;
static std::unordered_map<std::thread::id, char[16]> _pthread_name_map;

static size_t _pthread_tls_registry_next = 0;
static std::recursive_mutex _pthread_tls_registry_mutex;
static std::vector<pthread_key_destructor_t> _pthread_tls_registry;
static thread_local std::unordered_map<pthread_key_t, void*> _pthread_tls;

static_assert(sizeof(_pthread_tpp) <= sizeof(pthread_t),
              "C and C++ pthread_t definitions are incompatible!");

static void _pthread_register(_pthread_tpp* thrd, std::thread* t) {
    thrd->_id = t->get_id();
    std::lock_guard<std::recursive_mutex> lk(_pthread_map_mutex);
    _pthread_map[thrd->_id] = t;
}

static std::thread::id _pthread_extract_id(pthread_t thrd) {
    std::thread::id r;
    _pthread_tpp* tpp = (_pthread_tpp*) &thrd;
    r = tpp->_id;
    return r;
}

static bool _pthread_registered(pthread_t thrd) {
    std::thread::id id = _pthread_extract_id(thrd);
    std::lock_guard<std::recursive_mutex> lk(_pthread_map_mutex);
    return _pthread_map.count(id) == 1;
}

static std::thread* _pthread_get(pthread_t thrd) {
    std::thread::id id = _pthread_extract_id(thrd);
    if (_pthread_registered(thrd)) {
        std::lock_guard<std::recursive_mutex> lk(_pthread_map_mutex);
        return _pthread_map[id];
    }
    return nullptr;
}

static bool _pthread_detached(pthread_t thrd) {
    return _pthread_get(thrd) == nullptr;
}

static void _pthread_unregister(pthread_t thrd) {
    std::thread::id id = _pthread_extract_id(thrd);
    // Clean up thread.
    {
        std::lock_guard<std::recursive_mutex> lk(_pthread_map_mutex);
        if (_pthread_registered(thrd)) {
            std::thread* t = _pthread_get(thrd);
            auto i = _pthread_map.find(id);
            _pthread_map.erase(i);
            if (t != nullptr) {
                delete t;
            }
        }
    }
    // Clean up return values.
    {
        std::lock_guard<std::recursive_mutex> rvlk(_pthread_return_value_map_mutex);
        if (_pthread_return_value_map.count(id)) {
            auto i = _pthread_return_value_map.find(id);
            _pthread_return_value_map.erase(i);
        }
    }
}

class pthread_exit_exception : public std::exception {};

static void _pthread_main(pthread_func_t func, void* userarg) {
    pthread_t ptself = pthread_self();
    // Handle thread lifecycle.
    try {
        void* return_value = func(userarg);
        // Save return value for joining.
        if (!_pthread_detached(ptself)) {
            std::lock_guard<std::recursive_mutex> rvlk(_pthread_return_value_map_mutex);
            _pthread_return_value_map[std::this_thread::get_id()] = return_value;
        }
    }
    catch (const pthread_exit_exception& e) {
        // No-op, thrown from pthread_exit.
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    // TLS destructors.
    for (int c = 0; c < PTHREAD_DESTRUCTOR_ITERATIONS; c++) {
        std::lock_guard<std::recursive_mutex> lk(_pthread_tls_registry_mutex);
        for (int i = 0; i < _pthread_tls_registry.size(); i++) {
            pthread_key_destructor_t destructor = _pthread_tls_registry[i];
            if (destructor != (void*) -1 && destructor != nullptr) {
                auto v_i = _pthread_tls.find(i);
                if (v_i != _pthread_tls.end() && v_i->second != nullptr) {
                    void* v = v_i->second;
                    destructor(v);
                }
            }
        }
    }
    // Unregister detached threads.
    if (_pthread_detached(ptself)) {
        _pthread_unregister(ptself);
    }
}

int pthread_create(pthread_t* thrd, const pthread_attr_t* attr, pthread_func_t func,
                   void* arg) {
    // Construct empty pthread_t object.
    _pthread_tpp* tpp = (_pthread_tpp*) thrd;
    std::thread::id def;
    tpp->_id = def;
    // Allocate and start thread.
    std::thread* t = new std::thread(_pthread_main, func, arg);
    // Register thread.
    _pthread_register(tpp, t);
    // Apply attributes.
    if (attr != nullptr && attr->detach_state == PTHREAD_CREATE_DETACHED) {
        pthread_detach(*thrd);
    }
    if (attr != nullptr && attr->sched_inherit == PTHREAD_EXPLICIT_SCHED) {
        sched_param sprm = {attr->sched_priority};
        pthread_setschedparam(*thrd, sched_getscheduler(0), &sprm);
    }
    // TODO: stack size.
    return 0;
}

int pthread_detach(pthread_t thrd) {
    if (_pthread_registered(thrd)) {
        if (_pthread_detached(thrd)) {
            return EINVAL;
        }
        else {
            std::lock_guard<std::recursive_mutex> lk(_pthread_map_mutex);
            std::thread* t = _pthread_get(thrd);
            _pthread_map[t->get_id()] = nullptr;
            t->detach();
            return 0;
        }
    }
    else {
        return ESRCH;
    }
}

int pthread_join(pthread_t thrd, void** return_value) {
    if (_pthread_registered(thrd)) {
        if (_pthread_detached(thrd)) {
            return EINVAL;
        }
        else {
            std::thread* t = _pthread_get(thrd);
            t->join();
            std::thread::id id = _pthread_extract_id(thrd);
            // Retrieve return value.
            {
                std::lock_guard<std::recursive_mutex> rvlk(_pthread_return_value_map_mutex);
                if (return_value != nullptr && _pthread_return_value_map.count(id)) {
                    *return_value = _pthread_return_value_map[id];
                }
            }
            // Clean up.
            _pthread_unregister(thrd);
			return 0;
        }
    }
    else {
        return ESRCH;
    }
}

static void _pthread_exit() {
	throw pthread_exit_exception();
}

void pthread_exit(void* return_value) {
    pthread_t ptself = pthread_self();
    // Save return value for joining.
    if (!_pthread_detached(ptself)) {

        std::lock_guard<std::recursive_mutex> rvlk(_pthread_return_value_map_mutex);
        _pthread_return_value_map[std::this_thread::get_id()] = return_value;
    }
    _pthread_exit();
}

pthread_t pthread_self() {
    pthread_t thrd;
    _pthread_tpp* tpp = (_pthread_tpp*) &thrd;
    std::thread::id id = std::this_thread::get_id();
    tpp->_id = id;
    return thrd;
}

int pthread_equal(pthread_t a, pthread_t b) {
    std::thread::id a_id = _pthread_extract_id(a);
    std::thread::id b_id = _pthread_extract_id(b);
    return a_id == b_id;
}

int pthread_key_create(pthread_key_t* key, pthread_key_destructor_t destructor) {
    if (_pthread_tls_registry_next == INT_MAX) {
        return EAGAIN;
    }
    std::lock_guard<std::recursive_mutex> lk(_pthread_tls_registry_mutex);
    // Register new key.
    *key = _pthread_tls_registry_next;
    if (_pthread_tls_registry_next < _pthread_tls_registry.size()) {
        // Key is already allocated.
        _pthread_tls_registry[*key] = destructor;
        // Find next free slot.
        for (size_t next = _pthread_tls_registry_next + 1; next < _pthread_tls_registry.size();
             next++) {
            if (_pthread_tls_registry[next] == (void*) (-1)) {
                _pthread_tls_registry_next = next;
                return 0;
            }
        }
        // No next free slot found.
        _pthread_tls_registry_next = _pthread_tls_registry.size();
        return 0;
    }
    else {
        // Key is not allocated.
        _pthread_tls_registry.push_back(destructor);
        // Next free slot is end of registry.
        _pthread_tls_registry_next++;
        return 0;
    }
}

int pthread_key_delete(pthread_key_t key) {
    std::lock_guard<std::recursive_mutex> lk(_pthread_tls_registry_mutex);
    if (key < 0 || key >= _pthread_tls_registry.size()) {
        return EINVAL;
    }
    // Free slot.
    _pthread_tls_registry[key] = (pthread_key_destructor_t)(void*) -1;
    // Update next free slot.
    if (key < _pthread_tls_registry_next) {
        _pthread_tls_registry_next = key;
    }
    return 0;
}

void* pthread_getspecific(pthread_key_t key) {
    auto i = _pthread_tls.find(key);
    if (i == _pthread_tls.end()) {
        return nullptr;
    }
    else {
        return i->second;
    }
}

int pthread_setspecific(pthread_key_t key, const void* v) {
    _pthread_tls.insert_or_assign(key, (void*) v);
    return 0;
}

#ifdef _WIN32
    #include "pthread_win32.cpp"
#endif

int pthread_setname_np(pthread_t thrd, const char* name) {
    std::thread::id id = _pthread_extract_id(thrd);
    std::lock_guard<std::recursive_mutex> lk(_pthread_name_map_mutex);
    char* stored_name = _pthread_name_map[id];
    std::strncpy(stored_name, name, 16);
    stored_name[15] = '\0';
    return 0;
}

int pthread_getname_np(pthread_t thrd, char* name, size_t len) {
    std::thread::id id = _pthread_extract_id(thrd);
    std::lock_guard<std::recursive_mutex> lk(_pthread_name_map_mutex);
    if (!_pthread_name_map.count(id)) {
        return ESRCH;
    }
    char* stored_name = _pthread_name_map[id];
    std::strncpy(name, stored_name, len);
    name[len - 1] = '\0';
    return 0;
}

int pthread_getconcurrency() {
    return _pthread_concurrency;
}

int pthread_setconcurrency(int concurrency) {
    _pthread_concurrency = concurrency;
    return 0;
}

int pthread_once(pthread_once_t* flag, pthread_once_func_t routine) {
    std::lock_guard<std::recursive_mutex> lk(_pthread_once_mutex);
    if (!(*flag)) {
        routine();
        *flag = 1;
    }
	return 0;
}
