#ifndef SPEC_PTHREAD
#define SPEC_PTHREAD
/**
 * @file
 * @brief A compatibility layer between C++ threads and pthread.
 */

#include <sched.h>
#include <stddef.h>
#include <time.h>

#ifndef PTHREAD_EXPORT
	#define PTHREAD_EXPORT
#endif

/**
 * Unimplemented:
 *
 * PTHREAD_CANCEL_ASYNCHRONOUS
 * PTHREAD_CANCEL_ENABLE
 * PTHREAD_CANCEL_DEFERRED
 * PTHREAD_CANCEL_DISABLE
 * PTHREAD_CANCELED
 *
 * int pthread_cancel(pthread_t);
 * void pthread_cleanup_push(void*), void *);
 * void pthread_cleanup_pop(int);
 * int pthread_setcancelstate(int, int*);
 * int pthread_setcanceltype(int, int*);
 * void pthread_testcancel(void);
 */

#ifdef __cplusplus
extern "C" {
#endif

#define PTHREAD_CREATE_JOINABLE 1
#define PTHREAD_CREATE_DETACHED 2

#define PTHREAD_INHERIT_SCHED 3
#define PTHREAD_EXPLICIT_SCHED 4

#define PTHREAD_SCOPE_SYSTEM 5
#define PTHREAD_SCOPE_PROCESS 6

#define PTHREAD_MUTEX_ERRORCHECK 7
#define PTHREAD_MUTEX_RECURSIVE 8
#define PTHREAD_MUTEX_NORMAL 9
#define PTHREAD_MUTEX_DEFAULT PTHREAD_MUTEX_RECURSIVE

#define PTHREAD_PRIO_NONE 9
#define PTHREAD_PRIO_INHERIT 10
#define PTHREAD_PRIO_PROTECT 11

#define PTHREAD_PROCESS_PRIVATE 12
#define PTHREAD_PROCESS_SHARED 13

#define PTHREAD_STACK_MIN 65536

#define PTHREAD_DESTRUCTOR_ITERATIONS 3

typedef struct {
    int detach_state;
    int sched_inherit;
    int sched_priority;
    size_t stack_size;
} pthread_attr_t;

typedef struct {
    int type;
} pthread_mutexattr_t;

typedef int pthread_condattr_t;

typedef int pthread_rwlockattr_t;

typedef struct {
    int type;
    void* handle;
} pthread_mutex_t;

typedef struct {
    void* handle;
} pthread_cond_t;

typedef size_t pthread_t;

typedef struct {
    void* handle;
    pthread_t owner;
} pthread_rwlock_t;

typedef int pthread_once_t;

typedef int pthread_key_t;

typedef void* (*pthread_func_t)(void*);

typedef void (*pthread_key_destructor_t)(void*);

typedef void (*pthread_once_func_t)(void);

#define PTHREAD_MUTEX_INITIALIZER                                                              \
    { PTHREAD_MUTEX_DEFAULT, NULL }
#define PTHREAD_COND_INITIALIZER                                                               \
    { ((void*) (uintptr_t) -1) }
#define PTHREAD_ONCE_INIT 0
#define PTHREAD_RWLOCK_INITIALIZER                                                             \
    { NULL, (pthread_t) -1 }

/**
 * Thread attributes.
 */
PTHREAD_EXPORT int pthread_attr_init(pthread_attr_t* attr);

PTHREAD_EXPORT int pthread_attr_getdetachstate(const pthread_attr_t* attr, int* detach_state);
PTHREAD_EXPORT int pthread_attr_setdetachstate(pthread_attr_t* attr, int detach_state);

PTHREAD_EXPORT int pthread_attr_getguardsize(const pthread_attr_t* attr, size_t* guard);
PTHREAD_EXPORT int pthread_attr_setguardsize(pthread_attr_t* attr, size_t guard);

PTHREAD_EXPORT int pthread_attr_getinheritsched(const pthread_attr_t* attr, int* inherit);
PTHREAD_EXPORT int pthread_attr_setinheritsched(pthread_attr_t* attr, int inherit);

PTHREAD_EXPORT int pthread_attr_getschedparam(const pthread_attr_t* attr, struct sched_param* param);
PTHREAD_EXPORT int pthread_attr_setschedparam(pthread_attr_t* attr, const struct sched_param* param);

PTHREAD_EXPORT int pthread_attr_getschedpolicy(const pthread_attr_t* attr, int* policy);
PTHREAD_EXPORT int pthread_attr_setschedpolicy(pthread_attr_t* attr, int policy);

PTHREAD_EXPORT int pthread_attr_getscope(const pthread_attr_t* attr, int* scope);
PTHREAD_EXPORT int pthread_attr_setscope(pthread_attr_t* attr, int scope);

PTHREAD_EXPORT int pthread_attr_getstackaddr(const pthread_attr_t* attr, void** addr);
PTHREAD_EXPORT int pthread_attr_setstackaddr(pthread_attr_t* attr, void* addr);

PTHREAD_EXPORT int pthread_attr_getstacksize(const pthread_attr_t* attr, size_t* size);
PTHREAD_EXPORT int pthread_attr_setstacksize(pthread_attr_t* attr, size_t size);

PTHREAD_EXPORT int pthread_attr_destroy(pthread_attr_t* attr);

/**
 * Mutex attributes.
 */
PTHREAD_EXPORT int pthread_mutexattr_init(pthread_mutexattr_t* attr);

PTHREAD_EXPORT int pthread_mutexattr_gettype(const pthread_mutexattr_t* attr, int* type);
PTHREAD_EXPORT int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int type);

PTHREAD_EXPORT int pthread_mutexattr_getprotocol(const pthread_mutexattr_t* attr, int* protocol);
PTHREAD_EXPORT int pthread_mutexattr_setprotocol(pthread_mutexattr_t* attr, int protocol);

PTHREAD_EXPORT int pthread_mutexattr_getpshared(const pthread_mutexattr_t* attr, int* shared);
PTHREAD_EXPORT int pthread_mutexattr_setpshared(pthread_mutexattr_t* attr, int shared);

PTHREAD_EXPORT int pthread_mutexattr_getprioceiling(const pthread_mutexattr_t* attr, int* priority);
PTHREAD_EXPORT int pthread_mutexattr_setprioceiling(pthread_mutexattr_t* attr, int priority);

PTHREAD_EXPORT int pthread_mutexattr_destroy(pthread_mutexattr_t* attr);

/**
 * Condition variable attributes.
 */
PTHREAD_EXPORT int pthread_condattr_init(pthread_condattr_t* attr);

PTHREAD_EXPORT int pthread_condattr_getpshared(const pthread_condattr_t* attr, int* shared);
PTHREAD_EXPORT int pthread_condattr_setpshared(pthread_condattr_t* attr, int shared);

PTHREAD_EXPORT int pthread_condattr_destroy(pthread_condattr_t* attr);

/**
 * Read-Write Locks attributes.
 */
PTHREAD_EXPORT int pthread_rwlockattr_init(pthread_rwlockattr_t* attr);

PTHREAD_EXPORT int pthread_rwlockattr_getpshared(const pthread_rwlockattr_t* attr, int* shared);
PTHREAD_EXPORT int pthread_rwlockattr_setpshared(pthread_rwlockattr_t* attr, int shared);

PTHREAD_EXPORT int pthread_rwlockattr_destroy(pthread_rwlockattr_t* attr);

/**
 * Mutex.
 */
PTHREAD_EXPORT int pthread_mutex_init(pthread_mutex_t* mutex, const pthread_mutexattr_t* attr);
PTHREAD_EXPORT int pthread_mutex_destroy(pthread_mutex_t* mutex);

PTHREAD_EXPORT int pthread_mutex_getprioceiling(const pthread_mutex_t* mutex, int* prio);
PTHREAD_EXPORT int pthread_mutex_setprioceiling(pthread_mutex_t* mutex, int prio, int* old);

PTHREAD_EXPORT int pthread_mutex_lock(pthread_mutex_t* mutex);
PTHREAD_EXPORT int pthread_mutex_trylock(pthread_mutex_t* mutex);
PTHREAD_EXPORT int pthread_mutex_unlock(pthread_mutex_t* mutex);

/**
 * Condition variables.
 */
PTHREAD_EXPORT int pthread_cond_init(pthread_cond_t* cond, const pthread_condattr_t* attr);
PTHREAD_EXPORT int pthread_cond_destroy(pthread_cond_t* cond);

PTHREAD_EXPORT int pthread_cond_broadcast(pthread_cond_t* cond);
PTHREAD_EXPORT int pthread_cond_signal(pthread_cond_t* cond);

PTHREAD_EXPORT int pthread_cond_wait(pthread_cond_t* cond, pthread_mutex_t* mutex);
PTHREAD_EXPORT int pthread_cond_timedwait(pthread_cond_t* cond, pthread_mutex_t* mutex,
                           const struct timespec* tm);

/**
 * Read-Write/Shared Mutex.
 */
PTHREAD_EXPORT int pthread_rwlock_init(pthread_rwlock_t* mutex, const pthread_rwlockattr_t* attr);
PTHREAD_EXPORT int pthread_rwlock_destroy(pthread_rwlock_t* mutex);

PTHREAD_EXPORT int pthread_rwlock_rdlock(pthread_rwlock_t* mutex);
PTHREAD_EXPORT int pthread_rwlock_tryrdlock(pthread_rwlock_t* mutex);

PTHREAD_EXPORT int pthread_rwlock_wrlock(pthread_rwlock_t* mutex);
PTHREAD_EXPORT int pthread_rwlock_trywrlock(pthread_rwlock_t* mutex);

PTHREAD_EXPORT int pthread_rwlock_unlock(pthread_rwlock_t* mutex);

/**
 * Threads.
 */
PTHREAD_EXPORT int pthread_create(pthread_t* thrd, const pthread_attr_t* attr, pthread_func_t func, void* arg);

PTHREAD_EXPORT int pthread_detach(pthread_t thrd);
PTHREAD_EXPORT int pthread_join(pthread_t thrd, void** return_value);

PTHREAD_EXPORT void pthread_exit(void* return_value);
PTHREAD_EXPORT pthread_t pthread_self();
PTHREAD_EXPORT int pthread_equal(pthread_t a, pthread_t b);

PTHREAD_EXPORT int pthread_setname_np(pthread_t thrd, const char* name);
PTHREAD_EXPORT int pthread_getname_np(pthread_t thrd, char* name, size_t len);

PTHREAD_EXPORT int pthread_getschedparam(pthread_t thrd, int* policy, struct sched_param* param);
PTHREAD_EXPORT int pthread_setschedparam(pthread_t thrd, int policy, const struct sched_param* param);

/**
 * Thread local storage.
 */
PTHREAD_EXPORT int pthread_key_create(pthread_key_t* key, pthread_key_destructor_t destructor);
PTHREAD_EXPORT int pthread_key_delete(pthread_key_t key);

PTHREAD_EXPORT void* pthread_getspecific(pthread_key_t key);
PTHREAD_EXPORT int pthread_setspecific(pthread_key_t key, const void* v);

/**
 * Common.
 */
PTHREAD_EXPORT int pthread_getconcurrency();
PTHREAD_EXPORT int pthread_setconcurrency(int concurrency);

PTHREAD_EXPORT int pthread_once(pthread_once_t* flag, pthread_once_func_t routine);

#ifdef __cplusplus
}
#endif

#endif /*SPEC_PTHREAD*/
