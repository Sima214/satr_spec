#include "pthread.h"

#include <cerrno>
#include <cstddef>

int pthread_attr_init(pthread_attr_t* attr) {
    // Set pthread_attr_t's defaults.
    attr->detach_state = PTHREAD_CREATE_JOINABLE;
    attr->sched_inherit = PTHREAD_INHERIT_SCHED;
    attr->sched_priority = 0;
    attr->stack_size = 0;
    return 0;
}

int pthread_attr_getdetachstate(const pthread_attr_t* attr, int* detach_state) {
    *detach_state = attr->detach_state;
    return 0;
}

int pthread_attr_setdetachstate(pthread_attr_t* attr, int detach_state) {
    if (detach_state == PTHREAD_CREATE_JOINABLE || detach_state == PTHREAD_CREATE_DETACHED) {
        attr->detach_state = detach_state;
        return 0;
    }
    else {
        return EINVAL;
    }
}

int pthread_attr_getguardsize(const pthread_attr_t* attr, size_t* guard) {
    *guard = 0;
    return ENOTSUP;
}

int pthread_attr_setguardsize(pthread_attr_t* attr, size_t guard) {
    return ENOTSUP;
}

int pthread_attr_getinheritsched(const pthread_attr_t* attr, int* inherit) {
    *inherit = attr->sched_inherit;
    return 0;
}

int pthread_attr_setinheritsched(pthread_attr_t* attr, int inherit) {
    if (inherit == PTHREAD_INHERIT_SCHED || inherit == PTHREAD_EXPLICIT_SCHED) {
        attr->sched_inherit = inherit;
        return 0;
    }
    else {
        return EINVAL;
    }
}

int pthread_attr_getschedparam(const pthread_attr_t* attr, struct sched_param* param) {
    param->sched_priority = attr->sched_priority;
    return 0;
}

int pthread_attr_setschedparam(pthread_attr_t* attr, const struct sched_param* param) {
    int current_policy = sched_getscheduler(0);
    if (current_policy == -1) {
        return -1;
    }
    int min_valid_value = sched_get_priority_min(current_policy);
    int max_valid_value = sched_get_priority_max(current_policy);
    if (param->sched_priority >= min_valid_value && param->sched_priority <= max_valid_value) {
        attr->sched_priority = param->sched_priority;
        return 0;
    }
    else {
        return EINVAL;
    }
}

int pthread_attr_getschedpolicy(const pthread_attr_t* attr, int* policy) {
    *policy = SCHED_OTHER;
    return 0;
}

int pthread_attr_setschedpolicy(pthread_attr_t* attr, int policy) {
    if (policy == SCHED_OTHER) {
        return 0;
    }
    else {
        return ENOTSUP;
    }
}

int pthread_attr_getscope(const pthread_attr_t* attr, int* scope) {
    *scope = PTHREAD_SCOPE_SYSTEM;
    return 0;
}

int pthread_attr_setscope(pthread_attr_t* attr, int scope) {
    if (scope == PTHREAD_SCOPE_SYSTEM) {
        return 0;
    }
    else if (scope == PTHREAD_SCOPE_PROCESS) {
        return ENOTSUP;
    }
    else {
        return EINVAL;
    }
}

int pthread_attr_getstackaddr(const pthread_attr_t* attr, void** addr) {
    *addr = nullptr;
    return 0;
}

int pthread_attr_setstackaddr(pthread_attr_t* attr, void* addr) {
    return ENOTSUP;
}

int pthread_attr_getstacksize(const pthread_attr_t* attr, size_t* size) {
    *size = attr->stack_size;
    return 0;
}

int pthread_attr_setstacksize(pthread_attr_t* attr, size_t size) {
    if (size < PTHREAD_STACK_MIN) {
        return EINVAL;
    }
    else {
        attr->stack_size = size;
        return 0;
    }
}

int pthread_attr_destroy(pthread_attr_t* attr) {
    // No-op
	return 0;
}

int pthread_mutexattr_init(pthread_mutexattr_t* attr) {
    attr->type = PTHREAD_MUTEX_DEFAULT;
	return 0;
}

int pthread_mutexattr_gettype(const pthread_mutexattr_t* attr, int* type) {
    *type = attr->type;
    return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int type) {
    if (type == PTHREAD_MUTEX_ERRORCHECK || type == PTHREAD_MUTEX_RECURSIVE ||
        type == PTHREAD_MUTEX_NORMAL || type == PTHREAD_MUTEX_DEFAULT) {
        attr->type = type;
        return 0;
    }
    else {
        return EINVAL;
    }
}

int pthread_mutexattr_getprotocol(const pthread_mutexattr_t* attr, int* protocol) {
    *protocol = PTHREAD_PRIO_NONE;
    return 0;
}

int pthread_mutexattr_setprotocol(pthread_mutexattr_t* attr, int protocol) {
    if (protocol == PTHREAD_PRIO_NONE) {
        return 0;
    }
    else if (protocol == PTHREAD_PRIO_PROTECT || protocol == PTHREAD_PRIO_INHERIT) {
        return ENOTSUP;
    }
    else {
        return EINVAL;
    }
}

int pthread_mutexattr_getpshared(const pthread_mutexattr_t* attr, int* shared) {
    *shared = PTHREAD_PROCESS_PRIVATE;
    return 0;
}

int pthread_mutexattr_setpshared(pthread_mutexattr_t* attr, int shared) {
    if (shared == PTHREAD_PROCESS_PRIVATE) {
        return 0;
    }
    else if (shared == PTHREAD_PROCESS_SHARED) {
        return ENOTSUP;
    }
    else {
        return EINVAL;
    }
}

int pthread_mutexattr_getprioceiling(const pthread_mutexattr_t* attr, int* priority) {
    return ENOSYS;
}

int pthread_mutexattr_setprioceiling(pthread_mutexattr_t* attr, int priority) {
    return ENOSYS;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t* attr) {
    // No-op
	return 0;
}

int pthread_condattr_init(pthread_condattr_t* attr) {
    // No-op
	return 0;
}

int pthread_condattr_getpshared(const pthread_condattr_t* attr, int* shared) {
    *shared = PTHREAD_PROCESS_PRIVATE;
    return 0;
}

int pthread_condattr_setpshared(pthread_condattr_t* attr, int shared) {
    if (shared == PTHREAD_PROCESS_PRIVATE) {
        return 0;
    }
    else if (shared == PTHREAD_PROCESS_SHARED) {
        return ENOTSUP;
    }
    else {
        return EINVAL;
    }
}

int pthread_condattr_destroy(pthread_condattr_t* attr) {
    // No-op
	return 0;
}

int pthread_rwlockattr_init(pthread_rwlockattr_t* attr) {
    // No-op
	return 0;
}

int pthread_rwlockattr_getpshared(const pthread_rwlockattr_t* attr, int* shared) {
    *shared = PTHREAD_PROCESS_PRIVATE;
    return 0;
}

int pthread_rwlockattr_setpshared(pthread_rwlockattr_t* attr, int shared) {
    if (shared == PTHREAD_PROCESS_PRIVATE) {
        return 0;
    }
    else if (shared == PTHREAD_PROCESS_SHARED) {
        return ENOTSUP;
    }
    else {
        return EINVAL;
    }
}

int pthread_rwlockattr_destroy(pthread_rwlockattr_t* attr) {
    // No-op
	return 0;
}
