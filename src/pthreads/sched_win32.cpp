#include <cerrno>
#include <cstddef>
#include <thread>

#include <windows.h>

#include "sched.h"

static HANDLE get_proc_by_id(pid_t pid) {
    if (pid == 0) {
        return GetCurrentProcess();
    }
    // Remember to close it with CloseHandle()!
    HANDLE ph =
         OpenProcess(PROCESS_SET_INFORMATION | PROCESS_QUERY_INFORMATION, 0, (DWORD) pid);
    if (ph == NULL) {
        if (GetLastError() == ERROR_ACCESS_DENIED) {
            errno = EPERM;
        }
        else {
            errno = ESRCH;
        }
    }
    return ph;
}

static int native2api_policy(DWORD native_policy) {
    switch (native_policy) {
        case IDLE_PRIORITY_CLASS: return SCHED_IDLE;
        case BELOW_NORMAL_PRIORITY_CLASS: return SCHED_BELOW_NORMAL;
        case NORMAL_PRIORITY_CLASS: return SCHED_NORMAL;
        case ABOVE_NORMAL_PRIORITY_CLASS: return SCHED_ABOVE_NORMAL;
        case HIGH_PRIORITY_CLASS: return SCHED_HIGH;
        case REALTIME_PRIORITY_CLASS: return SCHED_REALTIME;
        default: return SCHED_NORMAL;
    }
}

static DWORD api2native_policy(int policy) {
    switch (policy) {
        case SCHED_IDLE: return IDLE_PRIORITY_CLASS;
        case SCHED_BELOW_NORMAL: return BELOW_NORMAL_PRIORITY_CLASS;
        case SCHED_NORMAL: return NORMAL_PRIORITY_CLASS;
        case SCHED_ABOVE_NORMAL: return ABOVE_NORMAL_PRIORITY_CLASS;
        case SCHED_HIGH: return HIGH_PRIORITY_CLASS;
        case SCHED_REALTIME: return REALTIME_PRIORITY_CLASS;
        default: return SCHED_INVALID;
    }
}

int sched_get_priority_max(int policy) {
    switch (policy) {
        case SCHED_IDLE:
        case SCHED_BELOW_NORMAL:
        case SCHED_NORMAL:
        case SCHED_ABOVE_NORMAL:
        case SCHED_HIGH:
        case SCHED_REALTIME: {
            return THREAD_PRIORITY_TIME_CRITICAL;
        }
        case SCHED_INVALID:
        default: {
            errno = EINVAL;
            return -1;
        }
    }
}

int sched_get_priority_min(int policy) {
    switch (policy) {
        case SCHED_IDLE:
        case SCHED_BELOW_NORMAL:
        case SCHED_NORMAL:
        case SCHED_ABOVE_NORMAL:
        case SCHED_HIGH:
        case SCHED_REALTIME: {
            return THREAD_PRIORITY_IDLE;
        }
        case SCHED_INVALID:
        default: {
            errno = EINVAL;
            return -1;
        }
    }
}

int sched_getparam(pid_t pid, sched_param* param) {
    errno = ENOSYS;
    return -1;
}

int sched_setparam(pid_t pid, const sched_param* param) {
    errno = ENOSYS;
    return -1;
}

int sched_rr_get_interval(pid_t pid, struct timespec* tm) {
    errno = ENOSYS;
    return -1;
}

int sched_getscheduler(pid_t pid) {
    HANDLE proc = get_proc_by_id(pid);
    if (proc == NULL) {
        return -1;
    }
    auto native_policy = GetPriorityClass(proc);
    CloseHandle(proc);
    return native2api_policy(native_policy);
}

int sched_setscheduler(pid_t pid, int policy, const struct sched_param* param) {
    // Argument check.
    DWORD native_policy = api2native_policy(policy);
    if (native_policy == (DWORD) SCHED_INVALID) {
        errno = EINVAL;
        return -1;
    }
    // Retreive native handle.
    HANDLE proc = get_proc_by_id(pid);
    if (proc == NULL) {
        return -1;
    }
    // Apply.
    bool applied = SetPriorityClass(proc, native_policy);
    auto old_native_policy = GetPriorityClass(proc);
    // Cleanup.
    CloseHandle(proc);
    if (!applied) {
        errno = EPERM;
        return -1;
    }
    return native2api_policy(old_native_policy);
}

int sched_yield() {
    std::this_thread::yield();
    return 0;
}
