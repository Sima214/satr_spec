#ifndef SPEC_SCHED
#define SPEC_SCHED
/**
 * @file
 * @brief A compatibility layer between C++ threads and pthread.
 */

#include <time.h>
#ifdef _WIN32
#include <windows.h>
#endif

#ifndef PTHREAD_EXPORT
	#define PTHREAD_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32

    typedef DWORD pid_t;

    struct sched_param {
        int sched_priority;
    };

    #define SCHED_INVALID -1
    #define SCHED_NORMAL 0
    #define SCHED_IDLE 1
    #define SCHED_BELOW_NORMAL 2
    #define SCHED_ABOVE_NORMAL 3
    #define SCHED_HIGH 4
    #define SCHED_REALTIME 5

    #define SCHED_FIFO SCHED_INVALID
    #define SCHED_RR SCHED_NORMAL
    #define SCHED_OTHER SCHED_NORMAL

#else

    #error `sched.h` not implemented for target operating system.

#endif

PTHREAD_EXPORT int sched_get_priority_max(int policy);
PTHREAD_EXPORT int sched_get_priority_min(int policy);

PTHREAD_EXPORT int sched_getparam(pid_t pid, struct sched_param* param);
PTHREAD_EXPORT int sched_setparam(pid_t pid, const struct sched_param* param);

PTHREAD_EXPORT int sched_getscheduler(pid_t pid);
PTHREAD_EXPORT int sched_setscheduler(pid_t pid, int policy, const struct sched_param* param);

PTHREAD_EXPORT int sched_rr_get_interval(pid_t pid, struct timespec* tm);

PTHREAD_EXPORT int sched_yield();

#ifdef __cplusplus
}
#endif

#endif /*SPEC_SCHED*/
