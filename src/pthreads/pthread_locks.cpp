#include "pthread.h"

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <shared_mutex>

constexpr std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>
timespec2timepoint(timespec ts) {
    auto abstime = std::chrono::seconds(ts.tv_sec) + std::chrono::nanoseconds(ts.tv_nsec);
    auto abstime_sys = std::chrono::duration_cast<std::chrono::system_clock::duration>(abstime);
    return std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>(
         abstime_sys);
}

static int _pthread_mutex_init(pthread_mutex_t* mutex) {
    // Assume handle is uninitialized.
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = new std::mutex;
            mutex->handle = (void*) mut;
            return 0;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = new std::recursive_mutex;
            mutex->handle = (void*) mut;
            return 0;
        }
        default: {
            return EINVAL;
        }
    }
}

static int _pthread_mutex_opt_init(pthread_mutex_t* mutex) {
    if (mutex->handle == nullptr) {
        return _pthread_mutex_init(mutex);
    }
    return 0;
}

static void _pthread_cond_opt_init(pthread_cond_t* cond) {
    if (((uintptr_t) cond->handle) == ((uintptr_t) -1)) {
        pthread_cond_init(cond, nullptr);
    }
}

int pthread_mutex_init(pthread_mutex_t* mutex, const pthread_mutexattr_t* attr) {
    int type = PTHREAD_MUTEX_DEFAULT;
    if (attr != nullptr) {
        if (attr->type == PTHREAD_MUTEX_ERRORCHECK || attr->type == PTHREAD_MUTEX_RECURSIVE ||
            attr->type == PTHREAD_MUTEX_NORMAL || attr->type == PTHREAD_MUTEX_DEFAULT) {
            type = attr->type;
        }
        else {
            return EINVAL;
        }
    }
    mutex->type = type;
    return _pthread_mutex_init(mutex);
}

int pthread_mutex_destroy(pthread_mutex_t* mutex) {
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            delete mut;
            break;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            delete mut;
            break;
        }
        default: {
            return EINVAL;
        }
    }
    mutex->type = -1;
    mutex->handle = nullptr;
    return 0;
}

int pthread_mutex_getprioceiling(const pthread_mutex_t* mutex, int* prio) {
    return ENOSYS;
}

int pthread_mutex_setprioceiling(pthread_mutex_t* mutex, int prio, int* old) {
    return ENOSYS;
}

int pthread_mutex_lock(pthread_mutex_t* mutex) {
    int init_error = _pthread_mutex_opt_init(mutex);
    if (init_error) {
        return init_error;
    }
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            mut->lock();
            return 0;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            mut->lock();
            return 0;
        }
        default: {
            return EINVAL;
        }
    }
}

int pthread_mutex_trylock(pthread_mutex_t* mutex) {
    int init_error = _pthread_mutex_opt_init(mutex);
    if (init_error) {
        return init_error;
    }
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            return mut->try_lock() ? 0 : EBUSY;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            return mut->try_lock() ? 0 : EBUSY;
        }
        default: {
            return EINVAL;
        }
    }
}

int pthread_mutex_unlock(pthread_mutex_t* mutex) {
    int init_error = _pthread_mutex_opt_init(mutex);
    if (init_error) {
        return init_error;
    }
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            mut->unlock();
            return 0;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            mut->unlock();
            return 0;
        }
        default: {
            return EINVAL;
        }
    }
}

int pthread_cond_init(pthread_cond_t* cond, const pthread_condattr_t* attr) {
    std::condition_variable_any* con = new std::condition_variable_any;
    cond->handle = (void*) con;
    return 0;
}

int pthread_cond_destroy(pthread_cond_t* cond) {
    if (cond->handle == nullptr) {
        return EINVAL;
    }
    std::condition_variable_any* con = (std::condition_variable_any*) cond->handle;
    delete con;
    return 0;
}

int pthread_cond_broadcast(pthread_cond_t* cond) {
    _pthread_cond_opt_init(cond);
    if (cond->handle == nullptr) {
        return EINVAL;
    }
    std::condition_variable_any* con = (std::condition_variable_any*) cond->handle;
    con->notify_all();
    return 0;
}
int pthread_cond_signal(pthread_cond_t* cond) {
    _pthread_cond_opt_init(cond);
    if (cond->handle == nullptr) {
        return EINVAL;
    }
    std::condition_variable_any* con = (std::condition_variable_any*) cond->handle;
    con->notify_one();
    return 0;
}

int pthread_cond_wait(pthread_cond_t* cond, pthread_mutex_t* mutex) {
    _pthread_cond_opt_init(cond);
    if (mutex->handle == nullptr || cond->handle == nullptr) {
        return EINVAL;
    }
    std::condition_variable_any* con = (std::condition_variable_any*) cond->handle;
    // Mutex is already locked. Adopt lock for condition variable.
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            std::unique_lock<std::mutex> lock(*mut, std::adopt_lock);
            con->wait(lock);
            // Do not unlock mutex just yet!
            lock.release();
            return 0;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            std::unique_lock<std::recursive_mutex> lock(*mut, std::adopt_lock);
            con->wait(lock);
            // Do not unlock mutex just yet!
            lock.release();
            return 0;
        }
        default: {
            return EINVAL;
        }
    }
}

int pthread_cond_timedwait(pthread_cond_t* cond, pthread_mutex_t* mutex,
                           const struct timespec* tm) {
    _pthread_cond_opt_init(cond);
    if (mutex->handle == nullptr || cond->handle == nullptr || tm == nullptr) {
        return EINVAL;
    }
    auto abstime = timespec2timepoint(*tm);
    std::condition_variable_any* con = (std::condition_variable_any*) cond->handle;
    // Mutex is already locked. Adopt lock for condition variable.
    switch (mutex->type) {
        case PTHREAD_MUTEX_ERRORCHECK:
        case PTHREAD_MUTEX_NORMAL: {
            std::mutex* mut = (std::mutex*) mutex->handle;
            std::unique_lock<std::mutex> lock(*mut, std::adopt_lock);
            auto timeout = con->wait_until(lock, abstime);
            // Do not unlock mutex just yet!
            lock.release();
            return timeout == std::cv_status::timeout ? ETIMEDOUT : 0;
        }
        case PTHREAD_MUTEX_RECURSIVE: {
            std::recursive_mutex* mut = (std::recursive_mutex*) mutex->handle;
            std::unique_lock<std::recursive_mutex> lock(*mut, std::adopt_lock);
            auto timeout = con->wait_until(lock, abstime);
            // Do not unlock mutex just yet!
            lock.release();
            return timeout == std::cv_status::timeout ? ETIMEDOUT : 0;
        }
        default: {
            return EINVAL;
        }
    }
}

static std::shared_mutex* _pthread_rwlock_opt_init(pthread_rwlock_t* mutex) {
    if (mutex->handle == nullptr) {
        pthread_rwlock_init(mutex, nullptr);
    }
    return (std::shared_mutex*) mutex->handle;
}

int pthread_rwlock_init(pthread_rwlock_t* mutex, const pthread_rwlockattr_t* attr) {
    std::shared_mutex* mut = new std::shared_mutex;
    mutex->handle = (void*) mut;
    mutex->owner = (pthread_t) -1;
    return 0;
}

int pthread_rwlock_destroy(pthread_rwlock_t* mutex) {
    if (mutex->owner != (pthread_t) -1) {
        return EBUSY;
    }
    std::shared_mutex* mut = (std::shared_mutex*) mutex->handle;
    delete mut;
	return 0;
}

int pthread_rwlock_rdlock(pthread_rwlock_t* mutex) {
    std::shared_mutex* mut = _pthread_rwlock_opt_init(mutex);
    mut->lock_shared();
    return 0;
}

int pthread_rwlock_tryrdlock(pthread_rwlock_t* mutex) {
    std::shared_mutex* mut = _pthread_rwlock_opt_init(mutex);
    return mut->try_lock_shared() ? 0 : EBUSY;
}

int pthread_rwlock_wrlock(pthread_rwlock_t* mutex) {
    std::shared_mutex* mut = _pthread_rwlock_opt_init(mutex);
    mut->lock();
    return 0;
}

int pthread_rwlock_trywrlock(pthread_rwlock_t* mutex) {
    std::shared_mutex* mut = _pthread_rwlock_opt_init(mutex);
    bool acquired = mut->try_lock();
    if (acquired) {
        mutex->owner = pthread_self();
        return 0;
    }
    else {
        return EBUSY;
    }
}

int pthread_rwlock_unlock(pthread_rwlock_t* mutex) {
    std::shared_mutex* mut = _pthread_rwlock_opt_init(mutex);
    if (pthread_equal(mutex->owner, pthread_self())) {
        // Exclusive lock. Note that only this function abd destroy read owner. 
        mutex->owner = (pthread_t) -1;
        mut->unlock();
    }
    else {
        // Shared lock.
        mut->unlock_shared();
    }
	return 0;
}
