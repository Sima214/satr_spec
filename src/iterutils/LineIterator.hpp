#ifndef SPEC_LINEITERATOR_HPP
#define SPEC_LINEITERATOR_HPP
/**
 * @file
 * @brief
 */

#include <trace/Trace.hpp>

#include <cstddef>
#include <istream>
#include <iterator>
#include <string>
#include <utility>

namespace spec {

class LineIterator {
   public:
    using iterator_category = std::input_iterator_tag;
    using value_type = std::pair<std::string, size_t>;
    using difference_type = size_t;
    using pointer = std::pair<std::string const*, size_t>;
    using reference = std::pair<const std::string&, size_t>;

   protected:
    std::istream* _stream;
    size_t _count;
    std::string _line;

   public:
    LineIterator(size_t size) : _stream(nullptr), _count(size) {}  // End mark.
    LineIterator(std::istream& stream) : _stream(&stream), _count(-1ULL) {
        _get_next_line();
    }

    LineIterator& operator++() {
        _get_next_line();
        return *this;
    }
    bool operator==(const LineIterator& other) const {
        if (other._stream != nullptr) {
            spec::fatal("Can only compare with end iterator!");
        }
        return !_has_value();
    }
    bool operator!=(const LineIterator& other) const {
        if (other._stream != nullptr) {
            spec::fatal("Can only compare with end iterator!");
        }
        return _has_value();
    }
    std::pair<const std::string&, size_t> operator*() const {
        if (!_has_value()) {
            spec::fatal("Tried to get value of empty LineIterator!");
        }
        return {_line, _count};
    }

   private:
    bool _has_value() const {
        return _stream != nullptr && _count != -1ULL;
    }
    bool _get_next_line();

    friend size_t operator-(const LineIterator&, const LineIterator&);
};

inline size_t operator-(const LineIterator& a, const LineIterator& b) {
    if (a._stream != nullptr) {
        spec::fatal("Difference's 1st operand must be end iterator!");
    }
    if (b._stream == nullptr) {
        // Case of two end iterators.
        return 0;
    }
    if (!b._has_value()) {
        spec::fatal("Difference's 2st operand doesn't have a value!");
    }
    return a._count - b._count;
}

class Lines {
   public:
    using iterator = LineIterator;

   protected:
    std::istream& _stream;
    size_t _count;

   public:
    Lines(std::istream& stream) : _stream(stream), _count(_count_lines(stream)) {}

    LineIterator begin() {
        return LineIterator(_stream);
    }
    LineIterator end() {
        return LineIterator(_count);
    }
    size_t size() {
        return _count;
    }

   private:
    static size_t _count_lines(std::istream& i);
};

}  // namespace spec

#endif /*SPEC_LINEITERATOR_HPP*/
