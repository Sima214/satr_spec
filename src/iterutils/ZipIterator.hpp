#ifndef SPEC_ZIPITERATOR_HPP
#define SPEC_ZIPITERATOR_HPP
/**
 * @file
 * @brief
 */

#include <trace/Trace.hpp>

#include <cstddef>
#include <iterator>
#include <utility>

namespace spec {

template<typename A, typename B> class ZipIterator {
   public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = std::pair<typename std::iterator_traits<A>::value_type,
                                 typename std::iterator_traits<B>::value_type>;
    using difference_type = size_t;
    using pointer = std::pair<typename std::iterator_traits<A>::pointer,
                              typename std::iterator_traits<B>::pointer>;
    using reference = std::pair<typename std::iterator_traits<A>::reference,
                                typename std::iterator_traits<B>::reference>;

   protected:
    A _iter_a;
    B _iter_b;

   public:
    ZipIterator(A a, B b) : _iter_a(a), _iter_b(b) {}

    ZipIterator& operator++() {
        ++_iter_a;
        ++_iter_b;
        return *this;
    }
    ZipIterator& operator--() {
        --_iter_a;
        --_iter_b;
        return *this;
    }

    ZipIterator operator++(int) {
        ZipIterator copy = *this;
        ++*this;
        return copy;
    }
    ZipIterator operator--(int) {
        ZipIterator copy = *this;
        --*this;
        return copy;
    }

    ZipIterator operator+(size_t v) const {
        ZipIterator copy = *this;
        copy += v;
        return copy;
    }
    ZipIterator operator-(size_t v) const {
        ZipIterator copy = *this;
        copy -= v;
        return copy;
    }

    ZipIterator& operator+=(size_t v) {
        _iter_a += v;
        _iter_b += v;
        return *this;
    }
    ZipIterator& operator-=(size_t v) {
        _iter_a -= v;
        _iter_b -= v;
        return *this;
    }

    bool operator==(const ZipIterator& other) const {
        bool a = _iter_a == other._iter_a;
        bool b = _iter_b == other._iter_b;
        return a && b;
    }
    bool operator!=(const ZipIterator& other) const {
        return !(*this == other);
    }
    reference operator*() const {
        typename std::iterator_traits<A>::reference a = *_iter_a;
        typename std::iterator_traits<B>::reference b = *_iter_b;
        return std::pair(a, b);
    }

    template<typename _A, typename _B>
    friend difference_type operator-(const ZipIterator<_A, _B>&, const ZipIterator<_A, _B>&);
};

template<typename A, typename B>
inline size_t operator-(const ZipIterator<A, B>& i, const ZipIterator<A, B>& j) {
    size_t a = i._iter_a - j._iter_a;
    size_t b = i._iter_b - j._iter_b;
    if (a == b) {
        return a;
    }
    else {
        spec::fatal("Zip iterators contains mismatched sizes!");
    }
}

template<typename A, typename B> class ZipContainerProxy {
   protected:
    A& _a;
    B& _b;

   public:
    using iterator = ZipIterator<decltype(_a.begin()), decltype(_b.begin())>;

   public:
    ZipContainerProxy(A& a, B& b) : _a(a), _b(b) {
        _validate();
    }

    iterator begin() {
        return ZipIterator(_a.begin(), _b.begin());
    }
    iterator end() {
        return ZipIterator(_a.end(), _b.end());
    }
    size_t size() {
        _validate();
        return _a.size();
    }

   protected:
    void _validate() {
        if (_a.size() != _b.size()) {
            spec::fatal("A and B containers have non-equal sizes!");
        }
    }
};

}  // namespace spec

#endif /*SPEC_ZIPITERATOR_HPP*/
