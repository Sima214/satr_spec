#include "LineIterator.hpp"

#include <trace/Trace.hpp>

#include <algorithm>

namespace spec {

bool LineIterator::_get_next_line() {
    if (_stream == nullptr) {
        spec::fatal("Cannot increment end iterator!");
    }
    if (_stream->eof()) {
        _count = -1ULL;
        _line.clear();
        return false;
    }
    if (!_stream->good()) {
        spec::fatal("Stream fail, but not eof!");
    }
    std::getline(*_stream, _line, '\n');
    _count++;
    return true;
}

size_t Lines::_count_lines(std::istream& i) {
    // Skip errored streams.
    if (!i.good()) {
        return 0;
    }
    // Save/reset state,
    auto old_state = i.rdstate();
    auto old_pos = i.tellg();
    i.clear();
    i.seekg(0);

    // Count newline characters.
    size_t c =
         std::count(std::istreambuf_iterator<char>(i), std::istreambuf_iterator<char>(), '\n');
    // Correct for first line.
    c++;
    // Clear eof.
    i.clear();

    // Restore state,
    i.seekg(old_pos);
    i.setstate(old_state);

    spec::trace("Lines::_count_lines: found #", c, " lines.");

    return c;
}

}  // namespace spec
