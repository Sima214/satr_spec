#ifndef SPEC_INDEXITERATOR_HPP
#define SPEC_INDEXITERATOR_HPP
/**
 * @file
 * @brief
 */

#include <cstddef>
#include <cstdint>
#include <iterator>

namespace spec {

template<typename C, typename V> class IndexIterator {
   public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = V;
    using difference_type = size_t;
    using pointer = V*;
    using reference = V&;

   protected:
    C& _handle;
    size_t _pos;

   public:
    IndexIterator(C& container, size_t position = 0) : _handle(container), _pos(position) {}

    reference operator*() const {
        return (*_handle)[_pos];
    }
    reference operator[](int) const {
        return (*_handle)[_pos];
    }

    IndexIterator& operator++() {
        ++_pos;
        return *this;
    }
    IndexIterator operator++(int) {
        IndexIterator copy = *this;
        ++_pos;
        return copy;
    }
    IndexIterator& operator--() {
        --_pos;
        return *this;
    }
    IndexIterator operator--(int) {
        IndexIterator copy = *this;
        --_pos;
        return copy;
    }

    IndexIterator operator+(size_t off) const {
        return IndexIterator(_handle, _pos + off);
    }
    IndexIterator operator+(intmax_t off) const {
        return IndexIterator(_handle, _pos + off);
    }
    IndexIterator operator-(size_t off) const {
        return IndexIterator(_handle, _pos - off);
    }
    IndexIterator operator-(intmax_t off) const {
        return IndexIterator(_handle, _pos - off);
    }

    IndexIterator& operator+=(size_t off) {
        _pos += off;
        return *this;
    }
    IndexIterator& operator+=(intmax_t off) {
        _pos += off;
        return *this;
    }
    IndexIterator& operator-=(size_t off) {
        _pos -= off;
        return *this;
    }
    IndexIterator& operator-=(intmax_t off) {
        _pos -= off;
        return *this;
    }

    bool operator==(const IndexIterator& o) const {
        return _pos == o._pos;
    }
    bool operator!=(const IndexIterator& o) const {
        return _pos != o._pos;
    }
    bool operator<=(const IndexIterator& o) const {
        return _pos <= o._pos;
    }
    bool operator>=(const IndexIterator& o) const {
        return _pos >= o._pos;
    }
    bool operator<(const IndexIterator& o) const {
        return _pos < o._pos;
    }
    bool operator>(const IndexIterator& o) const {
        return _pos > o._pos;
    }

    template<typename _C, typename _V>
    friend size_t operator-(const IndexIterator<_C, _V>& i, const IndexIterator<_C, _V>& j);
};

template<typename C, typename V>
inline size_t operator-(const IndexIterator<C, V>& i, const IndexIterator<C, V>& j) {
    return i._pos - j._pos;
}

template<typename C, typename V> class IndexerProxy {
   public:
    using iterator = IndexIterator<C, V>;

   protected:
    C& _handle;

   public:
    IndexerProxy(C container) : _handle(container) {}

    iterator begin() {
        return IndexIterator(_handle, 0);
    }
    iterator end() {
        return IndexIterator(_handle, _handle.size());
    }
    size_t size() {
        return _handle.size();
    }
};

}  // namespace spec

#endif /*SPEC_INDEXITERATOR_HPP*/
