#ifndef SPEC_CONCAT
#define SPEC_CONCAT
/**
 * @file
 * @brief C string concatenation
 */

#include <stddef.h>

/**
 * \p ... are the input strings.
 * \p count is the count of input strings.
 * 
 * Return value must be freed.
 */
char* concat(size_t count, ...);

#endif /*SPEC_CONCAT*/
