#include "concat.h"

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

char* concat(size_t count, ...) {
    // Allocate temporary and return memory.
    char* cache_str[count];
    size_t cache_len[count];
    char* final_str = NULL;
    size_t final_len = 0;
    // Prepare vargs.
    va_list vargs;
    va_start(vargs, count);
    // Get parameters.
    for (size_t i = 0; i < count; i++) {
        cache_str[i] = va_arg(vargs, char*);
        cache_len[i] = strlen(cache_str[i]);
        final_len += cache_len[i];
    }
    va_end(vargs);
    // Allocate destination string.
    final_str = malloc(final_len + 1);
    if (final_str == NULL) {
        return NULL;
    }
    // Copy old strings to new string.
    char* sp = final_str;
    for (size_t i = 0; i < count; i++) {
        char* cur_str = cache_str[i];
        size_t cur_len = cache_len[i];
        memcpy(sp, cur_str, cur_len);
        sp += cur_len;
    }
    *(sp) = '\0';
    return final_str;
}
