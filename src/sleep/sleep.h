#ifndef SPEC_SLEEP
#define SPEC_SLEEP
/**
 * @file
 * @brief A variety of sleep functions.
 */

#include <stdint.h>

/**
 * Pause execution for the milliseconds specified in the \p msecs.
 */
void clock_msleep(int64_t msecs);

/**
 * Creates a delay for the exact amount of time requested. Only for small delays.
 * \p usecs is in microseconds and it must be smaller than 1000000(1 second).
 */
void clock_nsleep(int64_t usecs);

/**
 * Combined version of satr_msleep and satr_nsleep.
 */
void clock_sleep(int32_t secs, int64_t usecs);

/**
 * Floating point version of satr_sleep.
 */
void clock_fsleep(double seconds);

#endif /*SPEC_SLEEP*/
