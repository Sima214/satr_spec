#ifndef SPEC_SLEEP_HPP
#define SPEC_SLEEP_HPP
/**
 * @file
 * @brief A variety of sleep functions.
 */

#include <macros.h>
C_DECLS_START
#include <sleep.h>
C_DECLS_END

namespace spec {

void sleep(double seconds) {
    clock_fsleep(seconds);
}

void sleep(int32_t secs, int64_t usecs) {
    clock_sleep(secs, usecs);
}

}  // namespace spec

#endif /*SPEC_SLEEP_HPP*/
