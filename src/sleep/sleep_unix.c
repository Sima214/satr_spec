#include <unistd.h>

void clock_msleep(int64_t msecs) {
    usleep(msecs * MILLI2MICRO);
}
