#include "sleep.h"

#include <macros.h>
#include <stdint.h>

void clock_fsleep(double seconds) {
    int32_t secs = (int32_t) seconds;
    int64_t usecs = ((seconds - secs) * MICRO2SEC);
    clock_sleep(math_max(secs, 0), math_max(usecs, 0));
}

void clock_sleep(int32_t secs, int64_t usecs) {
    int32_t leftover_secs = usecs / MICRO2SEC;
    usecs %= MICRO2SEC;
    int64_t msecs = (secs + leftover_secs) * MILLI2SEC;
    clock_msleep(msecs);
    clock_nsleep(usecs);
}

#if IS_POSIX
    #include "sleep_unix.c"
    #if IS_LINUX
        #include "sleep_linux.c"
    #elif IS_MACOS
        #include "sleep_macos.c"
    #else
        #error Unsupported unix operating system for sleep functions!
    #endif
#elif IS_WINDOWS
    #include "sleep_win32.c"
#else
    #error Unsupported operating system for sleep functions!
#endif
