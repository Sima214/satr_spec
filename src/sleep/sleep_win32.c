#include <macros.h>
#include <ntdll.h>
#include <trace.h>
#include <windows.h>

void clock_nsleep(int64_t usecs) {
    if (usecs <= 0 || usecs >= MICRO2SEC) {
        TRACF("%s input out of bounds!", __func__);
        return;
    }
    timeBeginPeriod(1);
    // Valid input, continue.
    LARGE_INTEGER delay;
    // Convert input to relative 100ns units.
    delay.QuadPart = -((usecs * MICRO2NANO) / 100);
    NtDelayExecution(FALSE, &delay);
    timeEndPeriod(1);
}

void clock_msleep(int64_t msecs) {
    // 35ms threshold for better accuracy above 30fps.
    if (msecs <= 35) {
        timeBeginPeriod(1);
        Sleep(msecs);
        timeEndPeriod(1);
    }
}
