#include <mach/clock.h>
#include <mach/mach.h>
#include <macros.h>
#include <trace.h>

void clock_nsleep(int64_t usecs) {
    if (usecs <= 0 || usecs >= MICRO2SEC) {
        TRACF("%s input out of bounds!", __func__);
        return;
    }
    // Valid input - continue.
    struct timespec towait = {0, usecs * MICRO2NANO};
    struct timespec remain = {0, 0};
    while (nanosleep(&towait, &remain) == -1 && errno == EINTR) {
        towait = remain;
    }
}
