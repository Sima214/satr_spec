#ifndef SPEC_MULTIVIEWOBJECT_HPP
#define SPEC_MULTIVIEWOBJECT_HPP
/**
 * @file
 * @brief Helper class to switch between objects of a pool,
 * similar to how DoubleBuffering/TripleBuffering works.
 * Note that semantics are different from what is used by framebuffers,
 * with the top buffer being directly visible to other code.
 */

#include <Utils.hpp>

#include <array>
#include <atomic>
#include <cstddef>
#include <cstdint>
#include <mutex>
#include <type_traits>
#include <utility>

namespace spec {

/**
 * @tparam strict_ordering Strong ordering uses mutexes instead of atomics internally.
 */
template<typename T, uint8_t N = 2, bool strict_ordering = false>
class MultiViewObject : public INonCopyable {
    static_assert(N >= 2, "Cannot have multiple views when having only one object.");
    static_assert(N <= 128, "Too many objects in MultiView.");

   protected:
    /**
     * Current index offset in cyclic buffer.
     */
    std::atomic<uint_fast8_t> state;
    /**
     * Pool of objects.
     */
    std::array<T, N> objects;

   public:
    MultiViewObject() {
        atomic_init(&state, (uint_fast8_t) 0);
    }

    template<size_t C> MultiViewObject(const T (&initial_objects)[C]) : MultiViewObject() {
        uint_fast8_t i;
        for (i = 0; i < std::min<size_t>(N, C); i++) {
            if constexpr (std::is_move_assignable<T>::value) {
                objects[i] = std::move(initial_objects[i]);
            }
            else {
                objects[i] = initial_objects[i];
            }
        }
    }

    ~MultiViewObject() = default;

    /**
     * Retrieve a certain view based on an relative \p offset.
     */
    T& get(const int relative_offset) {
        uint_fast8_t cur_state;
        // volatile is here to skip optimizations that move index calculation out of the loop.
        // FIXME: find a method which does not generate unneeded instructions.
        volatile int absolute_index;
        do {
            cur_state = state.load();
            // Get index of virtual cyclic buffer.
            auto relative_index = (relative_offset + cur_state) % N;
            absolute_index = (relative_index >= 0) ? relative_index : (relative_index + N);
        } while (state.load() != cur_state);
        return objects[absolute_index];
    }

    /**
     * Switch to next view.
     */
    void swap() {
        uint_fast8_t old_value;
        uint_fast8_t new_value;
        do {
            old_value = state.load();
            new_value = old_value + 1;
            if (new_value >= N) {
                new_value -= N;
            }
        } while (!state.compare_exchange_weak(old_value, new_value));
    }

    constexpr size_t size() const {
        return N;
    }

    /**
     * Alias for swap.
     */
    void increment() {
        swap();
    }
    /**
     * Alias for swap.
     */
    void next() {
        swap();
    }

    /**
     * Alias for front buffer.
     */
    T& get() {
        return get_front();
    }

    /**
     * The current buffer.
     */
    T& get_front() {
        return get(0);
    }
    /**
     * This is the previous front buffer.
     */
    T& get_back() {
        return get(-1);
    }

    /**
     * Alias for front buffer.
     */
    T& operator()() {
        return get();
    }

    /**
     * Syntactic sugar for accessing a relative view.
     */
    T& operator[](int index) {
        return get(index);
    }
};

template<typename T, uint8_t N> class MultiViewObject<T, N, true> : public INonCopyable {
    static_assert(N >= 2, "Cannot have multiple views when having only one object.");
    static_assert(N <= 128, "Too many objects in MultiView.");

   protected:
    /**
     * Current index offset in cyclic buffer.
     */
    uint_fast8_t state;
    /**
     * Pool of objects.
     */
    std::array<T, N> objects;
    /**
     * OS-provided synchronization primitive.
     */
    std::mutex lock;

   public:
    MultiViewObject() : state(0) {}

    template<size_t C> MultiViewObject(const T (&initial_objects)[C]) : MultiViewObject() {
        uint_fast8_t i;
        for (i = 0; i < std::min<size_t>(N, C); i++) {
            if constexpr (std::is_move_assignable<T>::value) {
                objects[i] = std::move(initial_objects[i]);
            }
            else {
                objects[i] = initial_objects[i];
            }
        }
    }

    ~MultiViewObject() = default;

    /**
     * Retrieve a certain view based on an relative \p offset.
     */
    T& get(const int relative_offset) {
        std::lock_guard<std::mutex> _lg(lock);
        auto relative_index = (relative_offset + state) % N;
        int absolute_index = (relative_index >= 0) ? relative_index : (relative_index + N);
        return objects[absolute_index];
    }

    /**
     * Switch to next view.
     */
    void swap() {
        std::lock_guard<std::mutex> _lg(lock);
        state++;
        if (state >= N) {
            state -= N;
        }
    }

    /**
     * Alias for swap.
     */
    void increment() {
        swap();
    }
    /**
     * Alias for swap.
     */
    void next() {
        swap();
    }

    /**
     * Alias for front buffer.
     */
    T& get() {
        return get_front();
    }

    /**
     * The current buffer.
     */
    T& get_front() {
        return get(0);
    }
    /**
     * This is the previous front buffer.
     */
    T& get_back() {
        return get(-1);
    }

    /**
     * Alias for front buffer.
     */
    T& operator()() {
        return get();
    }

    /**
     * Syntactic sugar for accessing a relative view.
     */
    T& operator[](int index) {
        return get(index);
    }
};

}  // namespace spec

#endif /*SPEC_MULTIVIEWOBJECT_HPP*/