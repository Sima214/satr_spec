#ifndef SPEC_SYNC_QUEUE
#define SPEC_SYNC_QUEUE
/**
 * @file
 * @brief Thread-safe dequeue containing variable size data.
 */

#include <macros.h>
#include <pthread.h>
#include <stddef.h>

/**
 * Implementation internal type.
 * Represents a node link in a
 * stack/queue data structure.
 */
typedef struct SyncQueueNode SyncQueueNode;

/**
 * Thread syncronized variable element type Dequeue.
 */
typedef struct {
    SyncQueueNode* head;
    SyncQueueNode* tail;
    size_t length;
    pthread_mutex_t lock;
    pthread_cond_t cond;
} SyncQueue;

typedef enum {
    /** No error */
    SYNC_QUEUE_OK,
    /**
     * Could not allocate memory
     * or destination buffer is too small.
     */
    SYNC_QUEUE_OUT_OF_MEMORY
} SyncQueueErrors;

/**
 * Initialize a new SyncQueue object.
 */
SyncQueueErrors syncqueue_init(SyncQueue* obj) MARK_NONNULL_ARGS(1);

/**
 * Returns current number of stored elements.
 */
size_t syncqueue_size(SyncQueue* obj) MARK_NONNULL_ARGS(1);

/**
 * Inserts a new element at the tail.
 */
SyncQueueErrors syncqueue_push_back(SyncQueue* obj, const void* data, size_t n)
     MARK_NONNULL_ARGS(1, 2);
/**
 * Inserts a new element at the head.
 */
SyncQueueErrors syncqueue_push_front(SyncQueue* obj, const void* data, size_t n)
     MARK_NONNULL_ARGS(1, 2);

/**
 * Removes the element from the tail.
 * If list is empty, then this function
 * blocks until a new element is available.
 *
 * The data is stored in buffer \p data.
 * The buffer's size is passed in \p n,
 * and the amount of data actually written
 * is stored in \p n.
 */
SyncQueueErrors syncqueue_pop_back(SyncQueue* obj, void* data, size_t* n)
     MARK_NONNULL_ARGS(1, 2, 3);
/**
 * Removes the element from the head.
 * If list is empty, then this function
 * blocks until a new element is available.
 *
 * The data is stored in buffer \p data.
 * The buffer's size is passed in \p n,
 * and the amount of data actually written
 * is stored in \p n.
 */
SyncQueueErrors syncqueue_pop_front(SyncQueue* obj, void* data, size_t* n)
     MARK_NONNULL_ARGS(1, 2, 3);

/**
 * Retrieves the element from the tail.
 * If list is empty, then this function
 * blocks until a new element is available.
 *
 * The data is stored in buffer \p data.
 * The buffer's size is passed in \p n,
 * and the amount of data actually written
 * is stored in \p n.
 */
SyncQueueErrors syncqueue_peek_back(SyncQueue* obj, void* data, size_t* n)
     MARK_NONNULL_ARGS(1, 2, 3);
/**
 * Retrieves the element from the head.
 * If list is empty, then this function
 * blocks until a new element is available.
 *
 * The data is stored in buffer \p data.
 * The buffer's size is passed in \p n,
 * and the amount of data actually written
 * is stored in \p n.
 */
SyncQueueErrors syncqueue_peek_front(SyncQueue* obj, void* data, size_t* n)
     MARK_NONNULL_ARGS(1, 2, 3);

/**
 * Clears all stored elements.
 */
SyncQueueErrors syncqueue_clear(SyncQueue* obj) MARK_NONNULL_ARGS(1);

/**
 * Uninitialize a previously initialized SyncQueue object.
 * Frees all internally allocated memory.
 * All stored elements are lost.
 */
void syncqueue_uninit(SyncQueue* obj) MARK_NONNULL_ARGS(1);

#endif /*SPEC_SYNC_QUEUE*/
