#ifndef SPEC_SYNCQUEUE_HPP
#define SPEC_SYNCQUEUE_HPP
/**
 * @file
 * @brief Thread-safe dequeue containing variable size data.
 * Does not respect C++ constructors/operators/destructors.
 */

#include <macros.h>
C_DECLS_START
#include <sync_queue.h>
C_DECLS_END

#include <Utils.hpp>

#include <cstddef>
#include <new>
#include <stdexcept>

namespace spec {

class SyncQueue : protected ::SyncQueue, public INonCopyable {
   public:
    SyncQueue() {
        SyncQueueErrors error = ::syncqueue_init(this);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::push_front.");
            } break;
        }
    }
    ~SyncQueue() {
        ::syncqueue_uninit(this);
    }

    template<typename T> void push_front(const T& o) {
        SyncQueueErrors error = syncqueue_push_front(this, &o, sizeof(T));
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::bad_alloc();
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue's constructor.");
            } break;
        }
    }
    template<typename T> void push_back(const T& o) {
        SyncQueueErrors error = syncqueue_push_back(this, &o, sizeof(T));
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::bad_alloc();
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::push_back.");
            } break;
        }
    }

    template<typename T, size_t N = sizeof(T)> size_t pop_front(T& o) {
        size_t n = N;
        SyncQueueErrors error = syncqueue_pop_front(this, &o, &n);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::length_error(
                     "Type does not have enough memory at SyncQueue::pop_front.");
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::pop_front.");
            } break;
        }
        return n;
    }
    template<typename T, size_t N = sizeof(T)> size_t pop_back(T& o) {
        size_t n = N;
        SyncQueueErrors error = syncqueue_pop_back(this, &o, &n);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::length_error(
                     "Type does not have enough memory at SyncQueue::pop_back.");
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::pop_back.");
            } break;
        }
        return n;
    }

    template<typename T, size_t N = sizeof(T)> size_t peek_front(T& o) {
        size_t n = N;
        SyncQueueErrors error = syncqueue_peek_front(this, &o, &n);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::length_error(
                     "Type does not have enough memory at SyncQueue::peek_front.");
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::peek_front.");
            } break;
        }
        return n;
    }
    template<typename T, size_t N = sizeof(T)> size_t peek_back(T& o) {
        size_t n = N;
        SyncQueueErrors error = syncqueue_peek_back(this, &o, &n);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            case SYNC_QUEUE_OUT_OF_MEMORY: {
                throw std::length_error(
                     "Type does not have enough memory at SyncQueue::peek_back.");
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::peek_back.");
            } break;
        }
        return n;
    }

    size_t size() {
        return ::syncqueue_size(this);
    }
    size_t length() {
        return ::syncqueue_size(this);
    }
    void clear() {
        SyncQueueErrors error = ::syncqueue_clear(this);
        switch (error) {
            case SYNC_QUEUE_OK: {
                /** NOP */
            } break;
            default: {
                throw std::runtime_error("Unknown runtime error at SyncQueue::clear.");
            } break;
        }
    }
};

}  // namespace spec

#endif /*SPEC_SYNCQUEUE_HPP*/