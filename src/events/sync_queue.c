#include "sync_queue.h"

#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <trace.h>

struct SyncQueueNode {
    /** Previous node. */
    SyncQueueNode* previous;
    /** Next node. */
    SyncQueueNode* next;
    /** Amount of stored bytes. */
    size_t length;
    /**
     * Stored data is after this point as a byte array:
     * https://en.wikipedia.org/wiki/Flexible_array_member.
     */
    uint8_t data[];
};

static pthread_mutexattr_t _syncqueue_init_lock_attr;
static pthread_condattr_t _syncqueue_init_cond_attr;

/**
 * syncqueue_init's static destructor.
 */
void _syncqueue_init_cleanup() {
    ERRNO_CALL_FAIL(pthread_mutexattr_destroy, &_syncqueue_init_lock_attr);
    ERRNO_CALL_FAIL(pthread_condattr_destroy, &_syncqueue_init_cond_attr);
}

/**
 * Allocate a new node.
 */
static SyncQueueNode* _syncqueue_alloc_node(const void* data, size_t n) {
    SyncQueueNode* node = malloc(sizeof(SyncQueueNode) + n);
    if (node) {
        node->previous = NULL;
        node->next = NULL;
        node->length = n;
        memcpy(node->data, data, n);
    }
    return node;
}

/**
 * If \p n is not large enough, then this function fails
 * with a zero value. Note that if \p n is 0,
 * then this range check is disabled.
 */
static int _syncqueue_node_range_check(SyncQueueNode* node, size_t* n) {
    size_t max_n = *n;
    if (max_n != 0 && max_n < node->length) {
        return 0;
    }
    return 1;
}

/**
 * Copy a node's data to user provided buffers.
 */
static void _syncqueue_node_copy_data(SyncQueueNode* node, void* data, size_t* n) {
    *n = node->length;
    memcpy(data, node->data, node->length);
}

/**
 * Free node memory.
 */
static void _syncqueue_free_node(SyncQueueNode* node) {
    free(node);
}

#define _syncqueue_wakeup(obj) ERRNO_CALL_FAIL(pthread_cond_signal, &obj->cond)
#define _syncqueue_wait(obj)                                                                   \
    while (obj->length == 0) {                                                                 \
        ERRNO_CALL_FAIL(pthread_cond_wait, &obj->cond, &obj->lock);                            \
    }

SyncQueueErrors syncqueue_init(SyncQueue* obj) {
    obj->head = NULL;
    obj->tail = NULL;
    obj->length = 0;
    // Initialize sync objects' settings.
    one_time_static_init(syncqueue_init_attrs, {
        ERRNO_CALL_FAIL(pthread_mutexattr_init, &_syncqueue_init_lock_attr);
        ERRNO_CALL_FAIL(pthread_condattr_init, &_syncqueue_init_cond_attr);
        if (atexit(_syncqueue_init_cleanup)) {
            FAIL("Could not register _syncqueue_init_cleanup!");
        }
    });
    // Initialize sync objects.
    ERRNO_CALL_FAIL(pthread_mutex_init, &obj->lock, &_syncqueue_init_lock_attr);
    ERRNO_CALL_FAIL(pthread_cond_init, &obj->cond, &_syncqueue_init_cond_attr);
    return SYNC_QUEUE_OK;
}

size_t syncqueue_size(SyncQueue* obj) {
    size_t r;
    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    // Exclusive access.
    r = obj->length;
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);
    return r;
}

SyncQueueErrors syncqueue_push_back(SyncQueue* obj, const void* data, size_t n) {
    SyncQueueNode* new_node = _syncqueue_alloc_node(data, n);
    if (new_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    size_t len = obj->length;
    if (len == 0) {
        // Nothing in the linked list.
        new_node->next = NULL;
        new_node->previous = NULL;
        obj->head = new_node;
        obj->tail = new_node;
    }
    else {
        // Normal case. Make node new tail.
        new_node->previous = NULL;
        new_node->next = obj->tail;
        obj->tail->previous = new_node;
        obj->tail = new_node;
    }
    obj->length = len + 1;
    _syncqueue_wakeup(obj);
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_push_front(SyncQueue* obj, const void* data, size_t n) {
    SyncQueueNode* new_node = _syncqueue_alloc_node(data, n);
    if (new_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    size_t len = obj->length;
    if (len == 0) {
        // Nothing in the linked list.
        new_node->next = NULL;
        new_node->previous = NULL;
        obj->head = new_node;
        obj->tail = new_node;
    }
    else {
        // Normal case. Make node new head.
        new_node->previous = obj->head;
        new_node->next = NULL;
        obj->head->next = new_node;
        obj->head = new_node;
    }
    obj->length = len + 1;
    _syncqueue_wakeup(obj);
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_pop_back(SyncQueue* obj, void* data, size_t* n) {
    SyncQueueNode* acquired_node = NULL;

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    // Wait until there is at least one element available.
    _syncqueue_wait(obj);
    // Try to acquire a node.
    if (_syncqueue_node_range_check(obj->tail, n)) {
        // Node acquired!
        acquired_node = obj->tail;
        // The length this data structure will have.
        size_t len = obj->length - 1;
        if (len == 0) {
            // Dequeue after this will be empty.
            obj->head = NULL;
            obj->tail = NULL;
        }
        else {
            // Remove node at tail and update links.
            obj->tail = acquired_node->next;
            obj->tail->previous = NULL;
        }
        // Update size.
        obj->length = len;
    }
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    // Report errors.
    if (acquired_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }
    // Copy acquired node data.
    _syncqueue_node_copy_data(acquired_node, data, n);
    // Free node.
    _syncqueue_free_node(acquired_node);
    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_pop_front(SyncQueue* obj, void* data, size_t* n) {
    SyncQueueNode* acquired_node = NULL;

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    // Wait until there is at least one element available.
    _syncqueue_wait(obj);
    // Try to acquire a node.
    if (_syncqueue_node_range_check(obj->head, n)) {
        // Node acquired!
        acquired_node = obj->head;
        // The length this data structure will have.
        size_t len = obj->length - 1;
        if (len == 0) {
            // Dequeue after this will be empty.
            obj->head = NULL;
            obj->tail = NULL;
        }
        else {
            // Remove node at head and update links.
            obj->head = acquired_node->previous;
            obj->head->next = NULL;
        }
        // Update size.
        obj->length = len;
    }
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    // Report errors.
    if (acquired_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }
    // Copy acquired node data.
    _syncqueue_node_copy_data(acquired_node, data, n);
    // Free node.
    _syncqueue_free_node(acquired_node);
    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_peek_back(SyncQueue* obj, void* data, size_t* n) {
    SyncQueueNode* acquired_node = NULL;

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    // Wait until there is at least one element available.
    _syncqueue_wait(obj);
    // Try to acquire a node.
    if (_syncqueue_node_range_check(obj->tail, n)) {
        // Node acquired!
        acquired_node = obj->tail;
        // Copy acquired node data, while holding the lock.
        _syncqueue_node_copy_data(acquired_node, data, n);
    }
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    // Report errors.
    if (acquired_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }
    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_peek_front(SyncQueue* obj, void* data, size_t* n) {
    SyncQueueNode* acquired_node = NULL;

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    // Wait until there is at least one element available.
    _syncqueue_wait(obj);
    // Try to acquire a node.
    if (_syncqueue_node_range_check(obj->head, n)) {
        // Node acquired!
        acquired_node = obj->head;
        // Copy acquired node data, while holding the lock.
        _syncqueue_node_copy_data(acquired_node, data, n);
    }
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    // Report errors.
    if (acquired_node == NULL) {
        return SYNC_QUEUE_OUT_OF_MEMORY;
    }
    return SYNC_QUEUE_OK;
}

SyncQueueErrors syncqueue_clear(SyncQueue* obj) {
    SyncQueueNode* node = NULL;

    ERRNO_CALL_FAIL(pthread_mutex_lock, &obj->lock);
    node = obj->head;
    obj->head = NULL;
    obj->tail = NULL;
    obj->length = 0;
    ERRNO_CALL_FAIL(pthread_mutex_unlock, &obj->lock);

    while (node != NULL) {
        SyncQueueNode* next_node = node->previous;
        _syncqueue_free_node(node);
        node = next_node;
    }

    return SYNC_QUEUE_OK;
}

void syncqueue_uninit(SyncQueue* obj) {
    // This clears all allocated nodes and their data.
    syncqueue_clear(obj);
    // Only need to destroy sync objects.
    ERRNO_CALL_FAIL(pthread_mutex_destroy, &obj->lock);
    ERRNO_CALL_FAIL(pthread_cond_destroy, &obj->cond);
}
