#ifndef SPEC_DISPATCHTABLE_HPP
#define SPEC_DISPATCHTABLE_HPP
/**
 * @file
 * @brief A class which can register callbacks,
 * returns a key, which can later be used to
 * unregister callbacks.
 *
 * The callbacks are stored in the
 * order they are registered.
 */

#include <meta/Utils.hpp>

#include <cstddef>
#include <functional>
#include <map>
#include <stdexcept>
#include <string>
#include <utility>

namespace spec {

/**
 * Warning: no internal synchronization.
 */
template<typename Y> class DispatchTable : public spec::INonCopyable {
   protected:
    size_t _last_uid = 0;
    std::map<size_t, std::function<Y>> mapping;

   public:
    DispatchTable() = default;
    DispatchTable(DispatchTable&&) = default;
    DispatchTable& operator=(DispatchTable&&) = default;

    /**
     * Registers a new callback.
     * Returns its UID, which
     * can be used to unregister
     * this callback.
     */
    size_t register_callback(std::function<Y>& cb) {
        // Assign a new UID.
        _last_uid++;
        // Register.
        mapping.insert(std::pair(_last_uid, cb));
        // Return used UID.
        return _last_uid;
    }

    /**
     * Unregisters a callback.
     * If \p uid does not refer to a known callback,
     * then a index_of_bounds exception is generated.
     */
    void unregister_callback(size_t uid) {
        auto pos = mapping.find(uid);
        if (pos == mapping.end()) {
            throw std::out_of_range(std::string("No callback function exists for UID: ") +
                                    std::to_string(uid));
        }
        mapping.erase(pos);
    }

    typename std::map<size_t, std::function<Y>>::const_iterator begin() {
        return mapping.cbegin();
    }

    typename std::map<size_t, std::function<Y>>::const_iterator end() {
        return mapping.cend();
    }

    /**
     * Calls all registered callbacks with the same
     * arguments and discards any return values.
     */
    template<typename... Args> void operator()(Args&&... args) {
        for (auto it : mapping) {
            it.second(args...);
        }
    }
};

}  // namespace spec

#endif /*SPEC_DISPATCHTABLE_HPP*/
