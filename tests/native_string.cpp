#include "results.h"

#include <NativeString.hpp>
#include <Trace.hpp>
#include <string>

int main() {
    std::string str("Hello world!");
    auto converted = spec::natstr::utf8_to_native(str);
    auto converted_original = spec::natstr::native_to_utf8(converted.get());
    spec::trace(converted_original.get());
    return TEST_OK;
}
