#include "results.h"

#include <logger.h>
#include <sleep.h>
#include <stddef.h>
#include <stopwatch.h>

int main() {
    logger_initialize(LOGGER_ALL, 1, LOGGER_ALL, NULL);
    StopWatch watch;
    clock_stopwatch_reset(&watch);
    for (int i = 0; i < 1000; i++) {
        clock_stopwatch_start(&watch);
        clock_fsleep(0.001);
        clock_stopwatch_stop(&watch);
    }
    logger_logi("avg: %.4f, min: %.4f, max: %.4f", watch.avg, watch.min, watch.max);
    return TEST_OK;
}
