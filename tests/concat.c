#include "results.h"

#include <concat.h>
#include <macros.h>
#include <stddef.h>
#include <stdlib.h>

int main() {
    static const char* TARGET = "Hello world!";
    char* generated = concat(4, "Hello", " ", "world", "!");
    if (generated == NULL) {
        return TEST_ERROR;
    }
    int equal = strequal(generated, TARGET);
    free(generated);
    return equal ? TEST_OK : TEST_FAIL;
}
