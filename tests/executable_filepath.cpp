#include "results.h"

#include <ExecutablePath.hpp>
#include <Trace.hpp>

#include <cstdlib>
#include <filesystem>

int main(int argc, char* argv[]) {
    if (argc != 2) {
        // No reference path provided.
        return TEST_ERROR;
    }
    std::filesystem::path ref(argv[1]);

    auto valp = spec::exec_fp::retrieve();
    if (!valp) {
        spec::trace("Unable to retrieve executable filepath.");
        return TEST_FAIL;
    }
    std::filesystem::path val(valp.get());

    bool pass = std::filesystem::equivalent(ref, val);
    if (pass) {
        return TEST_OK;
    }
    else {
        spec::trace("Path ref=`", ref, "` and val=`", val, "` are not equivalent!");
        return TEST_FAIL;
    }
}
