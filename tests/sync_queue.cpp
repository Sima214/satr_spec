#include "results.h"

#include <StopWatch.hpp>
#include <SyncQueue.hpp>
#include <Trace.hpp>

#include <functional>
#include <thread>

#define TESTSIZE 10000
#define BENCHSIZE 100000000
#define PRODUCER_LIMIT 100000

void consumer_main(spec::SyncQueue& queue, bool benchmark, bool latency) {
    if (latency) {
        spec::StopWatch* shared_clock = nullptr;
        while (true) {
            queue.pop_back(shared_clock);
            shared_clock->stop();
            if (shared_clock->get_count() == 100000) {
                spec::trace(shared_clock->get_min_time(), "\t", shared_clock->get_avg_time(),
                            "\t", shared_clock->get_max_time());
                shared_clock->reset();
            }
        }
    }
    else {
        for (size_t next = 0; next < (benchmark ? BENCHSIZE : TESTSIZE); next++) {
            size_t v = -1;
            queue.pop_back(v);
            if (v != next) {
                spec::fatal("Consumed at the wrong order!");
            }
        }
    }
}

void producer_main(spec::SyncQueue& queue, bool benchmark, bool latency) {
    if (latency) {
        spec::StopWatch shared_clock;
        while (true) {
            shared_clock.start();
            queue.push_front(&shared_clock);
            while (queue.size() > 0) {
                std::this_thread::yield();
            }
        }
    }
    else {
        for (size_t next = 0; next < (benchmark ? BENCHSIZE : TESTSIZE); next++) {
            // In 'production' applications this type of usage is discouraged.
            // while (queue.size() >= PRODUCER_LIMIT) {
            //     std::this_thread::yield();
            // }
            queue.push_front(next);
        }
    }
}

int main(int argc, char*[]) {
    spec::SyncQueue queue;
    bool benchmark = argc > 1;
    bool latency = argc > 2;
    std::thread consumer(consumer_main, std::ref(queue), benchmark, latency);
    std::thread producer(producer_main, std::ref(queue), benchmark, latency);
    consumer.join();
    producer.join();
    return TEST_OK;
}
