#include "results.h"

#include <stdio.h>
#include <sync_queue.h>

#define TEST_STR "Something"
#define ORDER_TEST_LENGTH 50
#define TEST_BUF_LEN 4096
static char TEST_BUF[TEST_BUF_LEN];

static int test_is_empty(SyncQueue* obj) {
    puts("Initialized queue should be empty.");
    return syncqueue_size(obj) == 0;
}

static int test_is_empty_after_add_remove_first(SyncQueue* obj) {
    puts("Should be empty after adding then removing first.");

    if (syncqueue_push_front(obj, TEST_STR, sizeof(TEST_STR))) {
        return 0;
    }
    int empty = syncqueue_size(obj) == 0;
    if (empty) {
        return 0;
    }
    size_t n = TEST_BUF_LEN;
    if (syncqueue_pop_front(obj, TEST_BUF, &n)) {
        return 0;
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

static int test_is_empty_after_add_remove_last(SyncQueue* obj) {
    puts("Should be empty after adding then removing last.");

    if (syncqueue_push_back(obj, TEST_STR, sizeof(TEST_STR))) {
        return 0;
    }
    int empty = syncqueue_size(obj) == 0;
    if (empty) {
        return 0;
    }
    size_t n = TEST_BUF_LEN;
    if (syncqueue_pop_back(obj, TEST_BUF, &n)) {
        return 0;
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

static int test_is_empty_after_add_first_remove_last(SyncQueue* obj) {
    puts("Should be empty after adding first then removing last.");

    if (syncqueue_push_front(obj, TEST_STR, sizeof(TEST_STR))) {
        return 0;
    }
    int empty = syncqueue_size(obj) == 0;
    if (empty) {
        return 0;
    }
    size_t n = TEST_BUF_LEN;
    if (syncqueue_pop_back(obj, TEST_BUF, &n)) {
        return 0;
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

static int test_is_empty_after_add_last_remove_first(SyncQueue* obj) {
    puts("Should be empty after adding last then removing first.");

    if (syncqueue_push_back(obj, TEST_STR, sizeof(TEST_STR))) {
        return 0;
    }
    int empty = syncqueue_size(obj) == 0;
    if (empty) {
        return 0;
    }
    size_t n = TEST_BUF_LEN;
    if (syncqueue_pop_front(obj, TEST_BUF, &n)) {
        return 0;
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int test_null(SyncQueue* obj) {
    puts("Testing NULL(zero length) elements.");
    if (syncqueue_push_back(obj, &TEST_BUF, 0)) {
        return 0;
    }
    int empty = syncqueue_size(obj) == 0;
    if (empty) {
        return 0;
    }
    size_t n = TEST_BUF_LEN;
    if (syncqueue_pop_front(obj, TEST_BUF, &n)) {
        return 0;
    }
    if (n != 0) {
        return 0;
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int test_queue_order(SyncQueue* obj) {
    int empty;
    puts("Testing queue functionality.");
    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        if (syncqueue_push_front(obj, &i, sizeof(i))) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(i + 1)) {
            return 0;
        }
    }

    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        int v;
        size_t n = sizeof(v);
        if (syncqueue_pop_back(obj, &v, &n)) {
            return 0;
        }
        if (n != sizeof(v) || v != i) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(ORDER_TEST_LENGTH - (i + 1))) {
            return 0;
        }
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int test_stack_order(SyncQueue* obj) {
    int empty;
    puts("Testing stack functionality.");
    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        if (syncqueue_push_front(obj, &i, sizeof(i))) {
            puts("Error pushing!");
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(i + 1)) {
            puts("Invalid size after pushing!");
            return 0;
        }
    }

    for (int i = ORDER_TEST_LENGTH - 1; i >= 0; i--) {
        int v;
        size_t n = sizeof(v);
        if (syncqueue_pop_front(obj, &v, &n)) {
            puts("Error popping!");
            return 0;
        }
        if (n != sizeof(v) || v != i) {
            printf("Expected: %i:%zu. Got: %i:%zu.\n", i, sizeof(i), v, n);
            return 0;
        }
        if (syncqueue_size(obj) != (size_t) i) {
            puts("Invalid size after popping!");
            return 0;
        }
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int test_queue_order_rev(SyncQueue* obj) {
    int empty;
    puts("Testing queue functionality(reversed).");
    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        if (syncqueue_push_back(obj, &i, sizeof(i))) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(i + 1)) {
            return 0;
        }
    }

    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        int v;
        size_t n = sizeof(v);
        if (syncqueue_pop_front(obj, &v, &n)) {
            return 0;
        }
        if (n != sizeof(v) || v != i) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(ORDER_TEST_LENGTH - (i + 1))) {
            return 0;
        }
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int test_stack_order_rev(SyncQueue* obj) {
    int empty;
    puts("Testing stack functionality(rev).");
    for (int i = 0; i < ORDER_TEST_LENGTH; i++) {
        if (syncqueue_push_back(obj, &i, sizeof(i))) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t)(i + 1)) {
            return 0;
        }
    }

    for (int i = ORDER_TEST_LENGTH - 1; i >= 0; i--) {
        int v;
        size_t n = sizeof(v);
        if (syncqueue_pop_back(obj, &v, &n)) {
            return 0;
        }
        if (n != sizeof(v) || v != i) {
            return 0;
        }
        if (syncqueue_size(obj) != (size_t) i) {
            return 0;
        }
    }

    empty = syncqueue_size(obj) == 0;
    return empty;
}

int main() {
    SyncQueue obj;
    if (syncqueue_init(&obj)) {
        return TEST_ERROR;
    }
    if (!test_is_empty(&obj)) {
        return TEST_FAIL;
    }
    if (!test_is_empty_after_add_remove_first(&obj)) {
        return TEST_FAIL;
    }
    if (!test_is_empty_after_add_remove_last(&obj)) {
        return TEST_FAIL;
    }
    if (!test_is_empty_after_add_first_remove_last(&obj)) {
        return TEST_FAIL;
    }
    if (!test_is_empty_after_add_last_remove_first(&obj)) {
        return TEST_FAIL;
    }
    if (!test_null(&obj)) {
        return TEST_FAIL;
    }
    if (!test_queue_order(&obj)) {
        return TEST_FAIL;
    }
    if (!test_stack_order(&obj)) {
        return TEST_FAIL;
    }
    if (!test_queue_order_rev(&obj)) {
        return TEST_FAIL;
    }
    if (!test_stack_order_rev(&obj)) {
        return TEST_FAIL;
    }
    syncqueue_uninit(&obj);
    return TEST_OK;
}
