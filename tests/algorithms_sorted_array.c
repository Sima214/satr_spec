#include "results.h"

#include <algorithms.h>
#include <macros.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <trace.h>

const int8_t INPUTS[] = {49, 54, -61, -68, 48, -79, -90, -5, 99, 7, 85, 80};

typedef struct MARK_PACKED {
    int8_t v;
    uint8_t _padd[2];
} special_t;

int special_cmp_callback(special_t* a, special_t* b) {
    return a->v - b->v;
}

int main() {
    special_t* a = NULL;
    for (size_t i = 0; i < sizeof(INPUTS); i++) {
        TRACE("####################################");
        a = realloc(a, (i + 1) * sizeof(special_t));
        special_t v = {INPUTS[i], {0, 0}};
        special_t* dst = (special_t*) bisect(a, i, &v, NULL, sizeof(special_t),
                                             (comparison_cb_t) special_cmp_callback);
        TRACF("%p -> %p <- %p", a, &a[i + 1], dst);
        if (insert(a, i, &v, sizeof(special_t), dst)) {
            return TEST_FAIL;
        }
        for (size_t j = 0; j <= i; j++) {
            TRACF("#%zu.%zu: %hhd", i, j, a[j].v);
        }
    }
    free(a);
    return TEST_OK;
}
