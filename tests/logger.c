#include "results.h"

#include <logger.h>
#include <stddef.h>

int main() {
    if (logger_initialize(LOGGER_ALL, 1, LOGGER_ALL, NULL)) {
        return TEST_FAIL;
    }
    logger_logv("%s%d%s", "Test", 0, "Verbose");
    logger_logd("%s%d%s", "Test", 1, "Debug");
    logger_logi("%s%d%s", "Test", 2, "Info");
    logger_logw("%s%d%s", "Test", 3, "Warn");
    logger_loge("%s%d%s", "Test", 4, "Error");
    logger_logf_rec("%s%d%s", "Test", 5, "Fatal");
    if (logger_uninitialize()) {
        return TEST_FAIL;
    }
    return TEST_OK;
}
