#include "results.h"

#include <environment.h>
#include <string.h>
#include <trace.h>

int main() {
    const char* env_test = getenv_cached("SATR_SPEC_CONFIG0");
    int env_ok = env_test != NULL && strcmp(env_test, "test") == 0;
    if (!env_ok) {
        return TEST_FAIL;
    }
    return TEST_OK;
}
