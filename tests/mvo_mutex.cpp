#include "results.h"

#include <MultiViewObject.hpp>
#include <Trace.hpp>

#define N 13

bool check_forward(spec::MultiViewObject<int, N, true>& mvo, int s) {
    for (int i = 1; i <= N * 3; i++) {
        auto e = (s + i) % N;
        auto a = mvo[i];
        spec::trace("mvo[", i, "] = ", a, "(e:", e, ")");
        if (e != a) {
            return false;
        }
    }
    return true;
}

bool check_backward(spec::MultiViewObject<int, N, true>& mvo, int s) {
    for (int i = 1; i <= N * 3; i++) {
        auto e = s - i;
        while (e < 0) {
            e += N;
        }
        auto a = mvo[-i];
        spec::trace("mvo[", i, "] = ", a, "(e:", e, ")");
        if (e != a) {
            return false;
        }
    }
    return true;
}

bool check_state(spec::MultiViewObject<int, N, true>& mvo, int s) {
    spec::trace("mvo[", 0, "] = ", mvo[0], "(e:", s, ")");
    return mvo[0] == s && check_forward(mvo, s) && check_backward(mvo, s);
}

int main(int, char*[]) {
    spec::MultiViewObject<int, N, true> mvo(
         {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    for (int s = 0; s <= N; s++) {
        auto e_s = s % N;
        if (!check_state(mvo, e_s)) {
            return TEST_FAIL;
        }
        mvo.next();
    }
    return TEST_OK;
}
