#include "results.h"

#include <DispatchTable.hpp>

#include <functional>

int callback() {
    return TEST_OK;
}

int main() {
    spec::DispatchTable<int()> dp;
    std::function<int()> cb(&callback);
    auto uid = dp.register_callback(cb);
    auto rv = 0;
    for (auto& it : dp) {
        rv += it.second();
    }
    dp.unregister_callback(uid);
    return rv;
}
