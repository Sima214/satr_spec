#include "results.h"

#include <Logger.hpp>

int main() {
    spec::Logger::logv("Test", 0, "Verbose");
    spec::Logger::logd("Test", 1, "Debug");
    spec::Logger::logi("Test", 2, "Info");
    spec::Logger::logw("Test", 3, "Warn");
    spec::Logger::loge("Test", 4, "Error");
    spec::Logger::logf_rec("Test", 5, "Fatal");
    return TEST_OK;
}
