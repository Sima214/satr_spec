#include "results.h"

#include <locale.h>
#include <stdio.h>
#include <unicode.h>

int main() {
    // Ensure correct locale.
    setlocale(LC_ALL, "en_US.utf8");

    const utf8_codepoint ALPHA_TONED = {1, 2, "ά"};
    utf8_codepoint alpha_toned = convert_utf32_utf8(940);

    if (alpha_toned.valid) {
        if (alpha_toned.size == ALPHA_TONED.size) {
            if (alpha_toned.value[0] == ALPHA_TONED.value[0] &&
                alpha_toned.value[1] == ALPHA_TONED.value[1]) {
                return TEST_OK;
            }
        }
    }
    return TEST_FAIL;
}
