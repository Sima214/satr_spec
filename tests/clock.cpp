#include "results.h"

#include <Logger.hpp>
#include <Sleep.hpp>
#include <StopWatch.hpp>

int main() {
    spec::StopWatch watch;
    for (int i = 0; i < 1000; i++) {
        watch.start();
        spec::sleep(0.001);
        watch.stop();
    }
    logger.logi("avg: ", watch.get_avg_time(), ", min: ", watch.get_min_time(),
                ", max: ", watch.get_max_time());
    return TEST_OK;
}
