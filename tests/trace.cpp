#include "results.h"

#include <Trace.hpp>

int main() {
    spec::trace("Hello world!");
    spec::trace("Hello", ' ', "world", '!');
    return TEST_OK;
}
