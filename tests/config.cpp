#include "config/Tunables.hpp"
#include "results.h"

#include <Trace.hpp>
#include <Tunables.hpp>

#include <string>

int main() {
    auto r0 = spec::tunables::get_str("SATR_SPEC_CONFIG0");
    if (r0.first != spec::tunables::Errors::Ok || r0.second != std::string("test")) {
        spec::trace("Subtest 0 failed.");
        return TEST_FAIL;
    }
    auto r0alt = spec::tunables::get_int32("SATR_SPEC_CONFIG0");
    if (r0alt.first != spec::tunables::Errors::NoConv) {
        spec::trace("Subtest 0 alt failed.");
        return TEST_FAIL;
    }
    auto r1 = spec::tunables::get_int16("SATR_SPEC_CONFIG1", 3);
    if (r1.first != spec::tunables::Errors::OutRange || r1.second != 3) {
        spec::trace("Subtest 1 failed.");
        return TEST_FAIL;
    }
    auto r2 = spec::tunables::get_int64("SATR_SPEC_CONFIG2");
    if (r2.first != spec::tunables::Errors::Ok || r2.second != -143) {
        spec::trace("Subtest 2 failed.");
        return TEST_FAIL;
    }
    auto r3 = spec::tunables::get_double("SATR_SPEC_CONFIG3");
    if (r3.first != spec::tunables::Errors::Ok || r3.second != 0.34) {
        spec::trace("Subtest 3 failed.");
        return TEST_FAIL;
    }
    return TEST_OK;
}
