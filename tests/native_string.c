#include "results.h"

#include <natstr.h>
#include <trace.h>

int main() {
    const char INPUT[] = "~δοκιμή~";
    const void* converted = natstrn_utf8_to_native(INPUT, sizeof(INPUT) - 1);
    const char* converted_original = natstr_native_to_utf8(converted);
    TRACE(converted_original);
    natstr_free(converted_original);
    natstr_free(converted);
    return TEST_OK;
}
